package com.imfreemobile.redeemi.android.redeemiandroid.viewmodel;

import android.app.Activity;
import android.app.Application;
import android.databinding.ObservableBoolean;
import android.databinding.ObservableField;

import com.imfreemobile.redeemi.android.redeemiandroid.BR;
import com.imfreemobile.redeemi.android.redeemiandroid.model.entity.WithdrawTransaction;
import com.imfreemobile.redeemi.android.redeemiandroid.model.networkmodel.EnvelopedWithdrawalHistory;
import com.imfreemobile.redeemi.android.redeemiandroid.utils.AuthUtils;
import com.imfreemobile.redeemi.android.redeemiandroid.utils.networkutils.NoConnectivityException;
import com.imfreemobile.redeemi.android.redeemiandroid.view.BottomNavActivity;
import com.imfreemobile.redeemi.android.redeemiandroid.view.fragment.withdraw.WithdrawHistoryFragment;
import com.imfreemobile.redeemi.android.redeemiandroid.view.fragment.withdraw.WithdrawIncomeFragment;

import java.util.List;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;
import timber.log.Timber;

/**
 * Created by jvillarey on 20/11/2017.
 */

public class WithdrawHistoryViewModel extends BaseViewModel {

    private static final String TAG = WithdrawHistoryViewModel.class.getSimpleName();

    private BottomNavActivity mActivity;
    private WithdrawIncomeFragment withdrawIncomeFragment;
    private WithdrawHistoryFragment withdrawHistoryFragment;
    private ObservableField<List<WithdrawTransaction>> withdrawTransactions = new ObservableField<>();
    private Call<EnvelopedWithdrawalHistory> networkCallWithdrawalHistory;
    private ObservableBoolean loading = new ObservableBoolean();
    public int page = 1;

    public void setLoading(boolean loading) {
        this.loading.set(loading);
        notifyPropertyChanged(BR._all);
        withdrawHistoryFragment.getMBinding().swipeRefreshLayout.setRefreshing(loading);
    }

    public boolean getLoading() {
        return this.loading.get();
    }

    public void setWithdrawTransactions(List<WithdrawTransaction> withdrawTransactions) {
        this.withdrawTransactions.set(withdrawTransactions);
        withdrawHistoryFragment.getAdapter().setItems(withdrawTransactions);
        withdrawHistoryFragment.getMBinding().recycler.getAdapter().notifyDataSetChanged();
        notifyPropertyChanged(BR._all);
    }

    public List<WithdrawTransaction> getWithdrawTransactions() {
        return this.withdrawTransactions.get();
    }

    public WithdrawHistoryViewModel(Application application, Activity activity, WithdrawIncomeFragment fragment, WithdrawHistoryFragment withdrawHistoryFragment) {
        super(application, activity);
        this.mActivity = (BottomNavActivity) activity;
        this.withdrawIncomeFragment = fragment;
        this.withdrawHistoryFragment = withdrawHistoryFragment;
        page = 1;

        fetchWithdrawalHistory(page);
    }

    public void fetchWithdrawalHistory(final int page) {
        setLoading(true);
        networkCallWithdrawalHistory = apiService.getWithdrawalHistory(AuthUtils.INSTANCE.getAccessHeaderFormattedToken(appDatabase.getTokenDao().getToken().getAccessToken()), page);
        networkCallWithdrawalHistory.enqueue(new Callback<EnvelopedWithdrawalHistory>() {
            @Override
            public void onResponse(Call<EnvelopedWithdrawalHistory> call, Response<EnvelopedWithdrawalHistory> response) {
                setLoading(false);
                if (response.isSuccessful()) {
                    if (page == 1) {
                        setWithdrawTransactions(response.body().getData());
                        withdrawHistoryFragment.getScrollListener().setmLoading(false);
                    } else {
                        List<WithdrawTransaction> withdrawTransactions = getWithdrawTransactions();
                        withdrawTransactions.addAll(response.body().getData());
                        Timber.d(TAG, "transactions: $withdrawTransactions");
                        setWithdrawTransactions(withdrawTransactions);
                    }
                } else {
                    // handleError
                }
            }

            @Override
            public void onFailure(Call<EnvelopedWithdrawalHistory> call, Throwable t) {
                if (t instanceof NoConnectivityException) {
                    // show no internet
                } else {
                    // handle error
                }
            }
        });
    }
}
