package com.imfreemobile.redeemi.android.redeemiandroid.view.fragment.transaction

import android.databinding.DataBindingUtil
import android.os.Bundle
import android.support.v4.app.Fragment
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup

import com.imfreemobile.redeemi.android.redeemiandroid.R
import com.imfreemobile.redeemi.android.redeemiandroid.databinding.FragmentIncomeBinding
import com.imfreemobile.redeemi.android.redeemiandroid.model.entity.IncomeBreakdown
import com.imfreemobile.redeemi.android.redeemiandroid.view.BottomNavActivity
import com.imfreemobile.redeemi.android.redeemiandroid.view.fragment.BaseFragment
import com.imfreemobile.redeemi.android.redeemiandroid.viewmodel.IncomeViewModel
import com.marcinorlowski.fonty.Fonty

/**
 * Created by jvillarey on 06/11/2017.
 */

class IncomeFragment : BaseFragment() {

    internal var mBinding: FragmentIncomeBinding? = null
    internal var mViewModel: IncomeViewModel? = null

    override fun onCreateView(inflater: LayoutInflater?, container: ViewGroup?, savedInstanceState: Bundle?): View? {
        super.onCreateView(inflater, container, savedInstanceState)
        mBinding = DataBindingUtil.inflate(inflater, R.layout.fragment_income, container, false)
        mViewModel = IncomeViewModel(activity.application, this.activity as BottomNavActivity, IncomeBreakdown(), parentFragment as TransactionFragment?)
        mBinding?.viewModel = mViewModel
        Fonty.setFonts(mBinding?.root as ViewGroup)
        return mBinding?.root
    }

    override fun onPause() {
        super.onPause()
        mViewModel?.cancelNetCall()
    }

    companion object {

        private val TAG = IncomeFragment::class.java.simpleName
    }
}
