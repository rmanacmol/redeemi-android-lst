package com.imfreemobile.redeemi.android.redeemiandroid.viewmodel;

import android.content.Context;

import com.imfreemobile.redeemi.android.redeemiandroid.model.entity.Transaction;

/**
 * Created by rmanacmol on 08/11/2017.
 */

public class ItemTransactionHistoryViewModel extends BaseViewModel {

    private Transaction transaction;
    private Context mContext;

    public ItemTransactionHistoryViewModel(Context context, Transaction transaction) {
        this.mContext = context;
        this.transaction = transaction;
    }

    public Transaction getTransaction() {
        return transaction;
    }

    public void setTransaction(Transaction transaction) {
        this.transaction = transaction;
    }

}
