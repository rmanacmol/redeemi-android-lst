package com.imfreemobile.redeemi.android.redeemiandroid.model.db

import android.arch.persistence.room.Database
import android.arch.persistence.room.RoomDatabase

import com.imfreemobile.redeemi.android.redeemiandroid.model.dao.TokenDao
import com.imfreemobile.redeemi.android.redeemiandroid.model.dao.UserDao
import com.imfreemobile.redeemi.android.redeemiandroid.model.tables.Token
import com.imfreemobile.redeemi.android.redeemiandroid.model.tables.User

/**
 * Created by renzmanacmol on 24/10/2017.
 */


@Database(entities = arrayOf(User::class, Token::class), version = 9, exportSchema = false)
abstract class AppDatabase : RoomDatabase() {

    abstract val userDao: UserDao

    abstract val tokenDao: TokenDao

}
