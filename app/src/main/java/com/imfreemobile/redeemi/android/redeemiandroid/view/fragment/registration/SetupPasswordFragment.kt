package com.imfreemobile.redeemi.android.redeemiandroid.view.fragment.registration

import android.content.Context
import android.databinding.DataBindingUtil
import android.os.Bundle
import android.support.v4.app.Fragment
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import com.imfreemobile.redeemi.android.redeemiandroid.R
import com.imfreemobile.redeemi.android.redeemiandroid.databinding.FragmentSetupPasswordBinding
import com.imfreemobile.redeemi.android.redeemiandroid.utils.RegistrationValidationutils
import com.imfreemobile.redeemi.android.redeemiandroid.view.ForgotPasswordActivity
import com.imfreemobile.redeemi.android.redeemiandroid.view.RegistrationFlowActivity
import com.imfreemobile.redeemi.android.redeemiandroid.view.fragment.BaseFragment
import com.imfreemobile.redeemi.android.redeemiandroid.viewmodel.SetupPasswordViewModel
import com.marcinorlowski.fonty.Fonty

/**
 * A simple [Fragment] subclass.
 * Activities that contain this fragment must implement the
 * [SetupPasswordFragment.OnSetupPasswordListener] interface
 * to handle interaction events.
 * Use the [SetupPasswordFragment.newInstance] factory method to
 * create an instance of this fragment.
 */
class SetupPasswordFragment : BaseFragment() {

    var mListener: OnSetupPasswordListener? = null

    var mBinding: FragmentSetupPasswordBinding? = null
    var mViewModel: SetupPasswordViewModel? = null
    var mActivity: RegistrationFlowActivity? = null

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        mViewModel = SetupPasswordViewModel(activity.application, mListener, this@SetupPasswordFragment)
    }

    override fun onCreateView(inflater: LayoutInflater?, container: ViewGroup?,
                              savedInstanceState: Bundle?): View? {
        super.onCreateView(inflater, container, savedInstanceState)
        mBinding = DataBindingUtil.inflate(inflater, R.layout.fragment_setup_password, container, false)
        mBinding?.viewModel = mViewModel
        Fonty.setFonts(mBinding?.root as ViewGroup)
        return mBinding?.root
    }

    override fun onViewCreated(view: View?, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        mBinding?.passwordStrengthIndicator?.visibility = View.INVISIBLE

        val indicatorButtons = arrayOf<View?>(mBinding?.indicatorOne, mBinding?.indicatorTwo, mBinding?.indicatorThree)
        mBinding?.etPassword?.addTextChangedListener(RegistrationValidationutils.PasswordStrengthTextWatcher(object : RegistrationValidationutils.PasswordStrengthTextWatcherCallback {
            override fun onWeakReached() {
                togglePasswordStrengthIndicatorUI(indicatorButtons, 1)
            }

            override fun onGoodReached() {
                togglePasswordStrengthIndicatorUI(indicatorButtons, 2)
            }

            override fun onStrongReached() {
                togglePasswordStrengthIndicatorUI(indicatorButtons, 3)
            }

            override fun onInvalidPassword() {
                togglePasswordStrengthIndicatorUI(indicatorButtons, 0)
            }
        }))

        if (activity is RegistrationFlowActivity) {
            mBinding?.tvEnterPassword?.setText(R.string.enter_password_text)
        } else if (activity is ForgotPasswordActivity) {
            mBinding?.tvEnterPassword?.setText(R.string.enter_new_password)
        }
    }

    var passwordStrength: Int = 0
    private fun togglePasswordStrengthIndicatorUI(indicatorButtons: Array<View?>, strength: Int) {
        passwordStrength = strength
        (0 until indicatorButtons.size).forEachIndexed { _, i -> indicatorButtons[i]?.isEnabled = (i + 1 <= strength) }
        mBinding?.indicatorText?.setText(resources.getStringArray(R.array.password_strength_text)[strength]);
        mBinding?.passwordStrengthIndicator?.visibility = if (strength == 0) View.INVISIBLE else View.VISIBLE
        mViewModel?.nextEnabled = strength > 0
    }

    override fun onAttach(context: Context?) {
        super.onAttach(context)
        if (context is OnSetupPasswordListener) {
            mListener = context
        } else {
            throw RuntimeException(context!!.toString() + " must implement OnSetupPasswordListener")
        }
    }

    override fun onDetach() {
        super.onDetach()
        mListener = null
    }

    interface OnSetupPasswordListener {
        fun onPasswordSet(password: String)
    }

    companion object {

        fun newInstance(): SetupPasswordFragment {
            val fragment = SetupPasswordFragment()
            val args = Bundle()
            fragment.arguments = args
            return fragment
        }
    }
}
