package com.imfreemobile.redeemi.android.redeemiandroid.viewmodel;

import android.app.Application;
import android.content.Context;
import android.databinding.ObservableBoolean;
import android.support.design.widget.Snackbar;
import android.text.method.HideReturnsTransformationMethod;
import android.text.method.PasswordTransformationMethod;
import android.view.View;
import android.view.inputmethod.InputMethodManager;

import com.imfreemobile.redeemi.android.redeemiandroid.BR;
import com.imfreemobile.redeemi.android.redeemiandroid.view.RegistrationFlowActivity;
import com.imfreemobile.redeemi.android.redeemiandroid.view.fragment.registration.SetupPasswordFragment;

/**
 * Created by jvillarey on 14/11/2017.
 */

public class SetupPasswordViewModel extends BaseViewModel {

    private static final String TAG = SetupPasswordViewModel.class.getSimpleName();

    private SetupPasswordFragment.OnSetupPasswordListener mListener;
    private SetupPasswordFragment mFragment;
    private ObservableBoolean passwordToggle = new ObservableBoolean();
    private ObservableBoolean nextEnabled = new ObservableBoolean();
    private ObservableBoolean loading = new ObservableBoolean();

    private Snackbar snackbar;

    public void setNextEnabled(boolean nextEnabled) {
        this.nextEnabled.set(nextEnabled);
        notifyPropertyChanged(BR._all);
    }

    public boolean getNextEnabled() {
        return this.nextEnabled.get();
    }

    public void setPasswordToggle(boolean passwordToggle) {
        this.passwordToggle.set(passwordToggle);
        notifyPropertyChanged(BR._all);
    }

    public boolean getPasswordToggle() {
        return this.passwordToggle.get();
    }

    public void setLoading(boolean loading) {
        this.loading.set(loading);
        notifyPropertyChanged(BR._all);
    }

    public boolean getLoading() {
        return this.loading.get();
    }

    public SetupPasswordViewModel(Application application, SetupPasswordFragment.OnSetupPasswordListener onSetupPasswordListener, SetupPasswordFragment fragment) {
        super(application, fragment.getActivity());
        this.mListener = onSetupPasswordListener;
        this.mFragment = fragment;
    }

    public void hideKeyboard() {
        InputMethodManager imm = (InputMethodManager) mFragment.getActivity().getSystemService(Context.INPUT_METHOD_SERVICE);
        imm.hideSoftInputFromWindow(mFragment.getMBinding().getRoot().getWindowToken(), 0);
    }

    public void onNextButtonClicked(View view) {
        hideKeyboard();
        mListener.onPasswordSet(mFragment.getMBinding().etPassword.getText().toString());
    }

    public void onPasswordToggleClicked(View view) {

        setPasswordToggle(!getPasswordToggle());

        int selectionStart = mFragment.getMBinding().etPassword.getSelectionStart();
        int selectionEnd = mFragment.getMBinding().etPassword.getSelectionEnd();
        if (mFragment.getMBinding().btnPasswordToggle.isChecked()) {
            mFragment.getMBinding().etPassword.setTransformationMethod(HideReturnsTransformationMethod.getInstance());
        } else {
            mFragment.getMBinding().etPassword.setTransformationMethod(PasswordTransformationMethod.getInstance());
        }
        if (selectionStart == selectionEnd) {
            mFragment.getMBinding().etPassword.setSelection(selectionStart);
        } else {
            mFragment.getMBinding().etPassword.setSelection(selectionStart, selectionEnd);
        }
    }

}
