package com.imfreemobile.redeemi.android.redeemiandroid.viewmodel;

import android.app.Activity;
import android.app.Application;
import android.content.Intent;
import android.databinding.BindingAdapter;
import android.databinding.ObservableField;
import android.text.TextUtils;
import android.util.Log;
import android.view.View;
import android.widget.ImageView;

import com.imfreemobile.redeemi.android.redeemiandroid.BR;
import com.imfreemobile.redeemi.android.redeemiandroid.R;
import com.imfreemobile.redeemi.android.redeemiandroid.model.entity.Transaction;
import com.imfreemobile.redeemi.android.redeemiandroid.model.entity.Voucher;
import com.imfreemobile.redeemi.android.redeemiandroid.model.networkmodel.EnvelopedTransaction;
import com.imfreemobile.redeemi.android.redeemiandroid.model.networkmodel.EnvelopedVoucher;
import com.imfreemobile.redeemi.android.redeemiandroid.model.networkmodel.Error;
import com.imfreemobile.redeemi.android.redeemiandroid.utils.AuthUtils;
import com.imfreemobile.redeemi.android.redeemiandroid.utils.networkutils.NoConnectivityException;
import com.imfreemobile.redeemi.android.redeemiandroid.view.ConfirmRedemptionActivity;
import com.imfreemobile.redeemi.android.redeemiandroid.view.PromoStatusActivity;
import com.imfreemobile.redeemi.android.redeemiandroid.view.SuccessRedemptionActivity;
import com.squareup.picasso.Picasso;

import java.io.IOException;
import java.io.UnsupportedEncodingException;
import java.net.URLEncoder;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

/**
 * Created by renzmanacmol on 24/10/2017.
 */

public class ConfirmRedemptionViewModel extends BaseViewModel {

    private static final String TAG = ConfirmRedemptionViewModel.class.getSimpleName();

    private ConfirmRedemptionActivity mActivity;
    public ObservableField<Voucher> voucher = new ObservableField<>();
    private Call<EnvelopedVoucher> networkCallVoucherInfo;
    private Call<EnvelopedTransaction> networkCallRedeemVoucher;

    public ConfirmRedemptionViewModel(Voucher voucher) {
        this.voucher.set(voucher);
    }

    public ConfirmRedemptionActivity getmActivity() {
        return mActivity;
    }

    public void setVoucher(Voucher voucher) {
        this.voucher.set(voucher);
        notifyPropertyChanged(BR._all);
    }

    public Voucher getVoucher() {
        return this.voucher.get();
    }

    public ConfirmRedemptionViewModel(Application application, ConfirmRedemptionActivity activity, Voucher voucher) {
        super(application, activity);
        this.mActivity = activity;
        this.voucher.set(voucher);
    }

    public void fetchVoucherInfo(String voucherId) {
        mActivity.showLoadingDialog();
        try {
            apiService.getVoucherDetails(AuthUtils.INSTANCE.getAccessHeaderFormattedToken(appDatabase.getTokenDao().getToken().getAccessToken()), URLEncoder.encode(voucherId, "UTF-8")).enqueue(new Callback<EnvelopedVoucher>() {
                @Override
                public void onResponse(Call<EnvelopedVoucher> call, Response<EnvelopedVoucher> response) {
                    mActivity.hideLoadingDialog();
                    if (response.isSuccessful()) {
                        setVoucher(response.body().getData());
                    } else {
                        // handle error
                        try {
                            handleError(response.errorBody().string());
                        } catch (IOException e) {
                            e.printStackTrace();
                            returnToScanWithInvalidQRError();
                        }
                    }
                }

                @Override
                public void onFailure(Call<EnvelopedVoucher> call, Throwable t) {
                    mActivity.hideLoadingDialog();
                    if (t instanceof NoConnectivityException) {
                        returnToScanWithNoConnectionError();
                    } else {
                        handleError(t.getMessage());
                    }
                }
            });
        } catch (UnsupportedEncodingException e) {
            e.printStackTrace();
        }
    }

    public void handleError(String string) {
        try {
            Error error = gson.fromJson(string, EnvelopedVoucher.class).getError();
            Intent i = new Intent(mActivity, PromoStatusActivity.class);
            i.putExtra("error_code", error.getErrorCode());
            mActivity.startActivity(i);
            mActivity.finish();
        } catch (Exception e) {
            returnToScanWithInvalidQRError();
        }
    }

    public void returnToScanWithInvalidQRError() {
        mActivity.setResult(Activity.RESULT_OK);
        mActivity.finish();
    }

    public void returnToScanWithNoConnectionError() {
        Intent i = new Intent();
        i.putExtra("type", "no connection");
        mActivity.setResult(Activity.RESULT_OK, i);
        mActivity.finish();
    }

    @BindingAdapter({"bind:brandImageUrl"})
    public static void loadImage(ImageView imageView, String imageUrl) {
        if (imageUrl != null && !TextUtils.isEmpty(imageUrl)) {
            Picasso.with(imageView.getContext())
                    .load(imageUrl)
                    .error(R.drawable.icon_brand)
                    .placeholder(R.drawable.icon_brand)
                    .into(imageView);
        } else {
            Picasso.with(imageView.getContext()).load(R.drawable.icon_brand).into(imageView);
        }
    }

    public void onConfirmClicked(View view) {
        mActivity.showLoadingDialog();
        networkCallRedeemVoucher = apiService.redeemVoucher(AuthUtils.INSTANCE.getAccessHeaderFormattedToken(appDatabase.getTokenDao().getToken().getAccessToken()), voucher.get().getCode(), "{\"\":\"\"}");
        networkCallRedeemVoucher.enqueue(new Callback<EnvelopedTransaction>() {
            @Override
            public void onResponse(Call<EnvelopedTransaction> call, Response<EnvelopedTransaction> response) {
                mActivity.hideLoadingDialog();
                if (response.isSuccessful()) {
                    launchSuccessfulRedemption(response.body().getData());
                } else {
                    try {
                        handleError(response.errorBody().string());
                    } catch (IOException e) {
                        e.printStackTrace();
                        returnToScanWithInvalidQRError();
                    }
                }
            }

            @Override
            public void onFailure(Call<EnvelopedTransaction> call, Throwable t) {
                mActivity.hideLoadingDialog();
                if (t instanceof NoConnectivityException) {
                    returnToScanWithNoConnectionError();
                } else {
                    handleError(t.getMessage());
                }
            }
        });
    }

    public void launchSuccessfulRedemption(Transaction transaction) {
        Intent i = new Intent(mActivity, SuccessRedemptionActivity.class);
        i.putExtra("transaction", transaction);
        mActivity.startActivity(i);
        mActivity.finish();
    }

    public void onCancelClicked(View view) {
        mActivity.setResult(Activity.RESULT_CANCELED);
        mActivity.finish();
    }

    public void cancelNetCall() {
        if (networkCallVoucherInfo != null) {
            networkCallVoucherInfo.cancel();
            networkCallVoucherInfo = null;
        }
        if (networkCallRedeemVoucher != null) {
            networkCallRedeemVoucher.cancel();
            networkCallRedeemVoucher = null;
        }
    }

}
