package com.imfreemobile.redeemi.android.redeemiandroid.view;

import android.Manifest;
import android.annotation.TargetApi;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.location.LocationManager;
import android.net.Uri;
import android.os.Build;
import android.os.Bundle;
import android.provider.Settings;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.v4.app.ActivityCompat;
import android.support.v4.content.ContextCompat;
import android.support.v7.app.AppCompatActivity;
import android.util.Log;
import android.view.View;

import com.afollestad.materialdialogs.DialogAction;
import com.afollestad.materialdialogs.MaterialDialog;
import com.google.android.gms.auth.api.phone.SmsRetrieverClient;
import com.imfreemobile.redeemi.android.redeemiandroid.ApiService;
import com.imfreemobile.redeemi.android.redeemiandroid.BuildConfig;
import com.imfreemobile.redeemi.android.redeemiandroid.R;
import com.imfreemobile.redeemi.android.redeemiandroid.RedeemiApplication;
import com.imfreemobile.redeemi.android.redeemiandroid.model.db.AppDatabase;

import javax.inject.Inject;

import timber.log.Timber;

/**
 * Created by jvillarey on 09/11/2017.
 */

public abstract class BaseLocationActivity extends AppCompatActivity {

    private static final String TAG = BaseLocationActivity.class.getSimpleName();
    @Inject
    LocationManager locationManager;
    @Inject
    ApiService apiService;
    @Inject
    AppDatabase appDatabase;
    @Inject
    SmsRetrieverClient client;
    private long timeStart = 0;
    private final static int minutesToRequirePin = BuildConfig.MINS_TO_REQUIRE_PIN;
    private MaterialDialog permissionSettingsDialog = null;
    private MaterialDialog locationSettingsDialog = null;

    protected abstract boolean isPinRequiredInActivity();

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        Timber.d(TAG, "density: $getResources.getDisplayMetrics.density");

        ((RedeemiApplication) getApplication()).getApplicationComponent().inject(this);

        if (BuildConfig.AUTOFILL_DISABLED && Build.VERSION.SDK_INT >= Build.VERSION_CODES.O) {
            disableAutoFill();
        }
    }

    @TargetApi(Build.VERSION_CODES.O)
    private void disableAutoFill() {
        getWindow().getDecorView().setImportantForAutofill(View.IMPORTANT_FOR_AUTOFILL_NO_EXCLUDE_DESCENDANTS);
    }

    @Override
    protected void onStart() {
        super.onStart();
        if (locationManager.isProviderEnabled(LocationManager.GPS_PROVIDER) || locationManager.isProviderEnabled(LocationManager.NETWORK_PROVIDER)) {
            if (ContextCompat.checkSelfPermission(this, Manifest.permission.ACCESS_COARSE_LOCATION) != PackageManager.PERMISSION_GRANTED || ContextCompat.checkSelfPermission(this, Manifest.permission.ACCESS_FINE_LOCATION) != PackageManager.PERMISSION_GRANTED) {
                ActivityCompat.requestPermissions(this, new String[]{Manifest.permission.ACCESS_FINE_LOCATION}, 10);
            } else {
                onResumeComplete();
            }
        } else {
            showLaunchLocationSettingsDialog();
        }
    }

    private void showLaunchLocationSettingsDialog() {
        if (locationSettingsDialog == null) {
            locationSettingsDialog = new MaterialDialog.Builder(this).iconRes(R.drawable.icon_location)
                    .limitIconToDefaultSize()
                    .title(R.string.location_dialog_content)
                    .cancelable(false)
                    .canceledOnTouchOutside(false)
                    .positiveText(R.string.location_dialog_settings)
                    .onPositive(new MaterialDialog.SingleButtonCallback() {
                        @Override
                        public void onClick(@NonNull MaterialDialog dialog, @NonNull DialogAction which) {
                            startActivity(new Intent(Settings.ACTION_LOCATION_SOURCE_SETTINGS));
                        }
                    }).build();
        }
        locationSettingsDialog.show();
    }

    private void showLaunchSettingsDialog() {
        if (permissionSettingsDialog == null) {
            permissionSettingsDialog = new MaterialDialog.Builder(this)
                    .title(R.string.location_dialog_content)
                    .onPositive(new MaterialDialog.SingleButtonCallback() {
                        @Override
                        public void onClick(@NonNull MaterialDialog dialog, @NonNull DialogAction which) {
                            Intent i = new Intent(Settings.ACTION_APPLICATION_DETAILS_SETTINGS);
                            i.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
                            Uri uri = Uri.fromParts("package", getPackageName(), null);
                            i.setData(uri);
                            startActivity(i);
                            dialog.dismiss();
                        }
                    }).iconRes(R.drawable.icon_location).canceledOnTouchOutside(false).cancelable(false).positiveText(R.string.home_dialog_settings).build();
        }
        permissionSettingsDialog.show();
    }

    @Override
    public void onRequestPermissionsResult(int requestCode, @NonNull String[] permissions, @NonNull int[] grantResults) {
        if (grantResults.length > 0) {
            if (grantResults[0] == PackageManager.PERMISSION_GRANTED) {
                onResumeComplete();
            } else if (grantResults[0] == PackageManager.PERMISSION_DENIED) {
                showLaunchSettingsDialog();
            }
        }
    }

    @Override
    protected void onResume() {
        super.onResume();
        if (isPinRequiredInActivity() && BuildConfig.PIN_REQUIRED) {
            if (timeStart != 0) {
                long elapsedInMillis = System.currentTimeMillis() - timeStart;
                int seconds = (int) (elapsedInMillis / 1000) % 60;
                int minutes = (int) ((elapsedInMillis / (1000 * 60)) % 60);
                int hours = (int) ((elapsedInMillis / (1000 * 60 * 60)) % 24);
                Timber.d(TAG, "time elapsed: $hours $minutes $seconds");
                if (minutes >= minutesToRequirePin) {
                    ActivityCompat.finishAffinity(this);
                    startActivity(new Intent(this, PinRequireActivity.class));
                }
            }

        }
        onResumeComplete();
    }

    @Override
    protected void onPause() {
        super.onPause();
        if (permissionSettingsDialog != null) {
            permissionSettingsDialog.dismiss();
        }
        if (locationSettingsDialog != null) {
            locationSettingsDialog.dismiss();
        }
        timeStart = 0;
        if (isPinRequiredInActivity()) {
            timeStart = System.currentTimeMillis();
        }
    }

    protected abstract void onResumeComplete();
}
