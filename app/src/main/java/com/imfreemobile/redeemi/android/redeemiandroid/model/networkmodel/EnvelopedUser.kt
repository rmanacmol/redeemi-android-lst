package com.imfreemobile.redeemi.android.redeemiandroid.model.networkmodel

import com.google.gson.annotations.Expose
import com.google.gson.annotations.SerializedName
import com.imfreemobile.redeemi.android.redeemiandroid.model.tables.User

/**
 * Created by Joseph on 26/10/2017.
 */

class EnvelopedUser {

    @Expose
    @SerializedName(DATA)
    val data: User? = null

    companion object {
        const val DATA = "data"
    }
}
