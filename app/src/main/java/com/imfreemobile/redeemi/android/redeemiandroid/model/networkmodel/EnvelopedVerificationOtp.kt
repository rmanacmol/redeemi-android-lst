package com.imfreemobile.redeemi.android.redeemiandroid.model.networkmodel

import com.google.gson.annotations.Expose
import com.google.gson.annotations.SerializedName
import com.imfreemobile.redeemi.android.redeemiandroid.model.entity.VerificationOtp

/**
 * Created by jvillarey on 13/11/2017.
 */

class EnvelopedVerificationOtp {

    @Expose
    @SerializedName(ERROR)
    val error: Error? = null

    @Expose
    @SerializedName(DATA)
    val data: VerificationOtp? = null

    companion object {
        const val DATA = "data"
        const val ERROR = "error"
    }
}
