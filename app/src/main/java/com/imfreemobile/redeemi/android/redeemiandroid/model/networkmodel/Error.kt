package com.imfreemobile.redeemi.android.redeemiandroid.model.networkmodel

import com.google.gson.annotations.Expose
import com.google.gson.annotations.SerializedName

/**
 * Created by Joseph on 26/10/2017.
 */

class Error {

    @Expose
    @SerializedName(RETRY_AT)
    val retryAt: Long? = null

    @Expose
    @SerializedName(ERROR_CODE)
    val errorCode: Int? = null

    @Expose
    @SerializedName(ERROR_DESC)
    val errorDesc: String? = null

    @Expose
    @SerializedName(STATUS_CD)
    val statusCd: Int? = null

    companion object {
        const val ERROR_CODE = "error_cd"
        const val ERROR_DESC = "error_desc"
        const val STATUS_CD = "status_cd"
        const val RETRY_AT = "retry_at"
    }
}
