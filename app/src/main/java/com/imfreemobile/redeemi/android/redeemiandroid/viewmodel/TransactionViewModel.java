package com.imfreemobile.redeemi.android.redeemiandroid.viewmodel;

import android.app.Activity;
import android.app.Application;
import android.graphics.Color;
import android.support.design.widget.Snackbar;
import android.view.View;

import com.imfreemobile.redeemi.android.redeemiandroid.R;
import com.imfreemobile.redeemi.android.redeemiandroid.model.tables.User;
import com.imfreemobile.redeemi.android.redeemiandroid.view.BottomNavActivity;
import com.imfreemobile.redeemi.android.redeemiandroid.view.adapter.TabsPageAdapter;
import com.imfreemobile.redeemi.android.redeemiandroid.view.fragment.transaction.IncomeFragment;
import com.imfreemobile.redeemi.android.redeemiandroid.view.fragment.transaction.TransactionActivitiesFragment;
import com.imfreemobile.redeemi.android.redeemiandroid.view.fragment.transaction.TransactionFragment;

/**
 * Created by rmanacmol on 06/11/2017.
 */

public class TransactionViewModel extends BaseViewModel {

    private static final String TAG = TransactionViewModel.class.getSimpleName();

    private BottomNavActivity mActivity;
    private TransactionFragment transactionFragment;
    private Snackbar snackbar;

    public TransactionViewModel(Application application, Activity activity, TransactionFragment fragment) {
        super(application, activity);
        this.mActivity = (BottomNavActivity) activity;
        this.transactionFragment = fragment;
    }

    public void setupPager() {
        TabsPageAdapter transactionPageAdapter = new TabsPageAdapter(transactionFragment.getChildFragmentManager());
        if (appDatabase.getUserDao().getUser().getUserType().equals(User.Companion.getTYPE_OWNER())) {
            transactionPageAdapter.addFragment(new IncomeFragment(), mActivity.getString(R.string.transaction_income));
        }
        transactionPageAdapter.addFragment(new TransactionActivitiesFragment(), mActivity.getString(R.string.transaction_activities));
        transactionFragment.getmBinding().viewPager.setAdapter(transactionPageAdapter);
        transactionFragment.getmBinding().transactionTab.setupWithViewPager(transactionFragment.getmBinding().viewPager);
    }

    public void showError(View view, String errorString) {
        if(snackbar==null) {
            snackbar = Snackbar.make(view,errorString,Snackbar.LENGTH_INDEFINITE);
        } else {
            snackbar.setText(errorString);
        }
        snackbar.setAction(R.string.okay, new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                snackbar.dismiss();
            }
        });
        snackbar.setActionTextColor(Color.WHITE);
        snackbar.show();
    }
}