package com.imfreemobile.redeemi.android.redeemiandroid.model.entity

import com.google.gson.annotations.Expose
import com.google.gson.annotations.SerializedName

/**
 * Created by jvillarey on 15/11/2017.
 */

class Status(@field:Expose
             @field:SerializedName(STATUS)
             val status: String?) {

    companion object {
        const val STATUS = "status"
    }
}
