package com.imfreemobile.redeemi.android.redeemiandroid.utils;

import android.text.Editable;
import android.text.TextWatcher;

import com.imfreemobile.redeemi.android.redeemiandroid.BuildConfig;

import java.util.regex.Pattern;

/**
 * Created by jvillarey on 13/11/2017.
 */

public class RegistrationValidationutils {

    public interface PasswordStrengthTextWatcherCallback {
        void onWeakReached();

        void onGoodReached();

        void onStrongReached();

        void onInvalidPassword();
    }

    public interface DigitsTextWatcherCallback {
        void onInput();

        void onMaxCharsReached();

        void onMaxCharsNotReached();
    }

    public static class VerificationCodeTextWatcher implements TextWatcher {
        private DigitsTextWatcherCallback callback;

        private VerificationCodeTextWatcher() {
        }

        public VerificationCodeTextWatcher(DigitsTextWatcherCallback callback) {
            this.callback = callback;
        }

        int maxChars = 4;

        @Override
        public void beforeTextChanged(CharSequence charSequence, int i, int i1, int i2) {
        }

        @Override
        public void onTextChanged(CharSequence charSequence, int i, int i1, int i2) {
        }

        @Override
        public void afterTextChanged(Editable s) {
            if (s.length() == maxChars) {
                callback.onMaxCharsReached();
            } else {
                callback.onMaxCharsNotReached();
            }
            callback.onInput();
        }
    }


    public static class MobileNumberTextWatcher implements TextWatcher {
        private DigitsTextWatcherCallback callback;

        private MobileNumberTextWatcher() {
        }

        public MobileNumberTextWatcher(DigitsTextWatcherCallback callback) {
            this.callback = callback;
        }

        char space = ' ';
        int maxChars = 13;

        @Override
        public void beforeTextChanged(CharSequence charSequence, int start, int count, int after) {

        }

        @Override
        public void onTextChanged(CharSequence charSequence, int start, int count, int after) {

        }

        @Override
        public void afterTextChanged(Editable s) {
            if (s.length() > 0 && s.charAt(s.length() - 1) == space) {
                s.delete(s.length() - 1, s.length());
            }
            if (s.length() > 0 && Character.isDigit(s.charAt(s.length() - 1)) && (s.length() == 5 || s.length() == 9)) {
                s.insert(s.length() - 1, String.valueOf(space));
            }

            if (s.length() == maxChars) {
                callback.onMaxCharsReached();
            } else {
                callback.onMaxCharsNotReached();
            }
            callback.onInput();
        }
    }

    public static class PasswordStrengthTextWatcher implements TextWatcher {
        private PasswordStrengthTextWatcherCallback callback;

        private PasswordStrengthTextWatcher() {
        }

        public PasswordStrengthTextWatcher(PasswordStrengthTextWatcherCallback callback) {
            this.callback = callback;
        }

        @Override
        public void beforeTextChanged(CharSequence charSequence, int i, int i1, int i2) {

        }

        @Override
        public void onTextChanged(CharSequence charSequence, int i, int i1, int i2) {

        }

        @Override
        public void afterTextChanged(Editable s) {
            switch (getPasswordStrengthRating(s.toString())) {
                case 0:
                    callback.onInvalidPassword();
                    break;
                case 1:
                    callback.onWeakReached();
                    break;
                case 2:
                    callback.onGoodReached();
                    break;
                case 3:
                    callback.onStrongReached();
                    break;
            }
        }
    }

    private static int getPasswordStrengthRating(String password) {
        if (password.length() >= BuildConfig.PASSWORD_MIN_LENGTH && Pattern.compile("\\d+").matcher(password).find() && Pattern.compile("[^A-za-z0-9]").matcher(password).find()) {
            if (password.length() >= BuildConfig.PASSWORD_MIN_LENGTH && password.length() < BuildConfig.PASSWORD_GOOD_LENGTH) {
                return 1;
            } else if (password.length() > BuildConfig.PASSWORD_MIN_LENGTH && password.length() < BuildConfig.PASSWORD_STRONG_LENGTH) {
                return 2;
            } else if (password.length() >= BuildConfig.PASSWORD_STRONG_LENGTH) {
                return 3;
            }
        }
        return 0;
    }

}
