package com.imfreemobile.redeemi.android.redeemiandroid.model.entity

import com.google.gson.annotations.Expose
import com.google.gson.annotations.SerializedName

/**
 * Created by jvillarey on 14/11/2017.
 */

class RegisterRequest(@field:Expose
                      @field:SerializedName(PIN)
                      val pin: String?, @field:Expose
                      @field:SerializedName(PASSWORD)
                      val password: String) {

    companion object {
        const val PIN = "pin"
        const val PASSWORD = "password"
    }
}
