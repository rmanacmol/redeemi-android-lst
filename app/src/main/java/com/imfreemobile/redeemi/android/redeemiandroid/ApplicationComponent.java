package com.imfreemobile.redeemi.android.redeemiandroid;

import com.imfreemobile.redeemi.android.redeemiandroid.view.BaseLocationActivity;
import com.imfreemobile.redeemi.android.redeemiandroid.viewmodel.BaseViewModel;

import javax.inject.Singleton;

import dagger.Component;

/**
 * Created by renzmanacmol on 23/10/2017.
 */

@Singleton
@Component(modules = {ApplicationModule.class})
public interface ApplicationComponent {

    void inject(BaseViewModel model);
    void inject(BaseLocationActivity activity);
}