package com.imfreemobile.redeemi.android.redeemiandroid.viewmodel;

import android.app.Application;
import android.app.DatePickerDialog;
import android.databinding.ObservableBoolean;
import android.databinding.ObservableField;
import android.os.Build;
import android.view.View;
import android.widget.DatePicker;

import com.imfreemobile.redeemi.android.redeemiandroid.BR;
import com.imfreemobile.redeemi.android.redeemiandroid.R;
import com.imfreemobile.redeemi.android.redeemiandroid.model.entity.IncomeBreakdown;
import com.imfreemobile.redeemi.android.redeemiandroid.model.networkmodel.EnvelopedIncomeBreakdown;
import com.imfreemobile.redeemi.android.redeemiandroid.model.networkmodel.Error;
import com.imfreemobile.redeemi.android.redeemiandroid.utils.AuthUtils;
import com.imfreemobile.redeemi.android.redeemiandroid.utils.DateUtils;
import com.imfreemobile.redeemi.android.redeemiandroid.utils.networkutils.NoConnectivityException;
import com.imfreemobile.redeemi.android.redeemiandroid.view.BottomNavActivity;
import com.imfreemobile.redeemi.android.redeemiandroid.view.fragment.transaction.TransactionFragment;

import java.io.IOException;
import java.util.Calendar;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

/**
 * Created by jvillarey on 07/11/2017.
 */

public class IncomeViewModel extends BaseViewModel {

    private static final String TAG = IncomeViewModel.class.getSimpleName();
    public ObservableField<IncomeBreakdown> incomeBreakdown = new ObservableField<>();
    public ObservableBoolean loading = new ObservableBoolean();
    public ObservableField<Calendar> fromDate = new ObservableField<>();
    public ObservableField<Calendar> toDate = new ObservableField<>();
    private BottomNavActivity mActivity;
    private TransactionFragment mFragment;
    private Call<EnvelopedIncomeBreakdown> networkCallGetIncomeBreakdown;

    public IncomeViewModel(IncomeBreakdown incomeBreakdown) {
        this.incomeBreakdown.set(incomeBreakdown);
    }

    public IncomeViewModel(Application application, BottomNavActivity bottomNavActivity, IncomeBreakdown incomeBreakdown, TransactionFragment fragment) {
        super(application, bottomNavActivity);
        this.mActivity = bottomNavActivity;
        this.incomeBreakdown.set(incomeBreakdown);
        this.mFragment = fragment;

        Calendar calendar = Calendar.getInstance();
        calendar.set(Calendar.DAY_OF_MONTH, 1);

        if (calendar.getTimeInMillis() < DateUtils.getUnixTimestampFromDateString(appDatabase.getUserDao().getUser().getCreatedAt())) {
            this.fromDate.set(DateUtils.getCalendarFromDateString(appDatabase.getUserDao().getUser().getCreatedAt()));
        } else {
            this.fromDate.set(calendar);
        }

        this.toDate.set(Calendar.getInstance());

        callIncomeBreakdownApi();
    }

    public BottomNavActivity getmActivity() {
        return mActivity;
    }

    public boolean getLoading() {
        return loading.get();
    }

    public void setLoading(boolean loading) {
        this.loading.set(loading);
        notifyPropertyChanged(BR._all);
    }

    public IncomeBreakdown getIncomeBreakdown() {
        return this.incomeBreakdown.get();
    }

    public void setIncomeBreakdown(IncomeBreakdown incomeBreakdown) {
        this.incomeBreakdown.set(incomeBreakdown);
        notifyPropertyChanged(BR._all);
    }

    public Calendar getFromDate() {
        return this.fromDate.get();
    }

    public void setFromDate(Calendar calendar) {
        this.fromDate.set(calendar);
        notifyPropertyChanged(BR._all);
        callIncomeBreakdownApi();
    }

    public Calendar getToDate() {
        return this.toDate.get();
    }

    public void setToDate(Calendar calendar) {
        this.toDate.set(calendar);
        notifyPropertyChanged(BR._all);
        callIncomeBreakdownApi();
    }

    public void callIncomeBreakdownApi() {
        fetchIncomeBreakdown(DateUtils.getApiCallFormattedStringFromCalendar(this.getFromDate()), DateUtils.getApiCallFormattedStringFromCalendar(this.getToDate()));
    }

    public void cancelNetCall() {
        if (networkCallGetIncomeBreakdown != null) {
            networkCallGetIncomeBreakdown.cancel();
            networkCallGetIncomeBreakdown = null;
        }
    }

    public void fetchIncomeBreakdown(String from, String to) {
        setLoading(true);
        networkCallGetIncomeBreakdown = apiService.getTransactionsSummary(AuthUtils.INSTANCE.getAccessHeaderFormattedToken(appDatabase.getTokenDao().getToken().getAccessToken()), from, to);
        networkCallGetIncomeBreakdown.enqueue(new Callback<EnvelopedIncomeBreakdown>() {
            @Override
            public void onResponse(Call<EnvelopedIncomeBreakdown> call, Response<EnvelopedIncomeBreakdown> response) {
                setLoading(false);
                if (response.isSuccessful()) {
                    setIncomeBreakdown(response.body().getData());
                } else {
                    // handle error
                    try {
                        handleError(response.errorBody().string());
                    } catch (IOException | NullPointerException e) {
                        e.printStackTrace();
                        showParseError();
                    }
                }
            }

            @Override
            public void onFailure(Call<EnvelopedIncomeBreakdown> call, Throwable t) {
                setLoading(false);
                if (t instanceof NoConnectivityException) {
                    showNoInternetError();
                } else {
                    showParseError();
                }
            }
        });
    }

    public void handleError(String string) {
        try {
            Error error = gson.fromJson(string, EnvelopedIncomeBreakdown.class).getError();
            showParseError();
        } catch (Exception e) {
            showParseError();
        }
    }

    public void showNoInternetError() {
        mFragment.showError(mActivity.getString(R.string.error_connection));
    }

    public void showParseError() {
        mFragment.showError(mActivity.getString(R.string.income_error_string));
    }

    public void onFromClicked(View view) {

        final Calendar c = fromDate.get();
        int year = c.get(Calendar.YEAR);
        int month = c.get(Calendar.MONTH);
        int day = c.get(Calendar.DAY_OF_MONTH);

        DatePickerDialog datePickerDialog = new DatePickerDialog(getmActivity(), new DatePickerDialog.OnDateSetListener() {
            @Override
            public void onDateSet(DatePicker datePicker, int year, int month, int day) {
                setFromDate(DateUtils.getCalendarFromYearMonthDay(year, month, day));
            }
        }, year, month, day);

        datePickerDialog.getDatePicker().setMinDate(DateUtils.getUnixTimestampFromDateString(appDatabase.getUserDao().getUser().getCreatedAt()));
        if (Build.VERSION.SDK_INT == Build.VERSION_CODES.LOLLIPOP_MR1 || Build.VERSION.SDK_INT == Build.VERSION_CODES.LOLLIPOP) {
            datePickerDialog.getDatePicker().setMaxDate(System.currentTimeMillis() + (24 * 60 * 60 * 1000));
        } else {
            datePickerDialog.getDatePicker().setMaxDate(System.currentTimeMillis());
        }

        datePickerDialog.show();
    }

    public void onToClicked(View view) {

        final Calendar min = fromDate.get();
        int year = min.get(Calendar.YEAR);
        int month = min.get(Calendar.MONTH);
        int day = min.get(Calendar.DAY_OF_MONTH);

        final Calendar to = toDate.get();

        DatePickerDialog datePickerDialog = new DatePickerDialog(getmActivity(), new DatePickerDialog.OnDateSetListener() {
            @Override
            public void onDateSet(DatePicker datePicker, int year, int month, int day) {
                setToDate(DateUtils.getCalendarFromYearMonthDay(year, month, day));
            }
        }, to.get(Calendar.YEAR), to.get(Calendar.MONTH), to.get(Calendar.DAY_OF_MONTH));

        datePickerDialog.getDatePicker().setMinDate(min.getTimeInMillis());
        if (Build.VERSION.SDK_INT == Build.VERSION_CODES.LOLLIPOP_MR1) {
            datePickerDialog.getDatePicker().setMaxDate(System.currentTimeMillis() + (24 * 60 * 60 * 1000));
        } else {
            datePickerDialog.getDatePicker().setMaxDate(System.currentTimeMillis());
        }

        datePickerDialog.show();
    }
}