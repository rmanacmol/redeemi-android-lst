package com.imfreemobile.redeemi.android.redeemiandroid.view

import android.databinding.DataBindingUtil
import android.os.Bundle
import android.view.ViewGroup
import com.imfreemobile.redeemi.android.redeemiandroid.R
import com.imfreemobile.redeemi.android.redeemiandroid.databinding.ActivityVerifyBinding
import com.imfreemobile.redeemi.android.redeemiandroid.utils.RegistrationValidationutils
import com.imfreemobile.redeemi.android.redeemiandroid.viewmodel.VerifyViewModel
import com.marcinorlowski.fonty.Fonty

/**
 * Created by jvillarey on 10/11/2017.
 */

class VerifyActivity : BaseLocationActivity() {

    override fun isPinRequiredInActivity(): Boolean {
        return false
    }

    companion object {
        val TAG = VerifyActivity::class.java.simpleName
    }

    var mBinding: ActivityVerifyBinding? = null
    var mViewModel: VerifyViewModel? = null

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        mBinding = DataBindingUtil.setContentView(this@VerifyActivity, R.layout.activity_verify)
        mViewModel = VerifyViewModel(application, this@VerifyActivity)
        mBinding?.viewModel = mViewModel

        mBinding?.etPhone?.addTextChangedListener(RegistrationValidationutils.MobileNumberTextWatcher(object : RegistrationValidationutils.DigitsTextWatcherCallback {
            override fun onInput() {
                mViewModel?.hideError()
            }

            override fun onMaxCharsReached() {
                mViewModel?.nextEnabled = true
            }

            override fun onMaxCharsNotReached() {
                mViewModel?.nextEnabled = false
            }

        }))

        Fonty.setFonts(mBinding?.getRoot() as ViewGroup)
    }

    fun getMobileNumber(): String {
        return mBinding?.etPhone?.text.toString()
    }

    override fun onResumeComplete() {

    }

    fun showSnackbar(text: String) {
        mViewModel?.showSnackbar(mBinding?.root, text)
    }
}
