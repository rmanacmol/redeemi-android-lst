package com.imfreemobile.redeemi.android.redeemiandroid.model.dao

import android.arch.persistence.room.Dao
import android.arch.persistence.room.Delete
import android.arch.persistence.room.Insert
import android.arch.persistence.room.Query
import android.arch.persistence.room.Update

import com.imfreemobile.redeemi.android.redeemiandroid.model.tables.User

import android.arch.persistence.room.OnConflictStrategy.REPLACE

/**
 * Created by renzmanacmol on 24/10/2017.
 */

@Dao
interface UserDao {

    @get:Query("SELECT * FROM user")
    val allUser: List<User>

    @get:Query("SELECT * FROM user LIMIT 1")
    val user: User

    @Insert(onConflict = REPLACE)
    fun insert(user: User?)

    @Delete
    fun delete(users: User?)

    @Query("DELETE FROM user")
    fun deleteAll()

    @Update(onConflict = REPLACE)
    fun updateEvent(user: User?)
}