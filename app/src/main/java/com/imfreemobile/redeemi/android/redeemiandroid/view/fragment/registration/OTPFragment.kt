package com.imfreemobile.redeemi.android.redeemiandroid.view.fragment.registration

import android.content.BroadcastReceiver
import android.content.Context
import android.content.Intent
import android.content.IntentFilter
import android.databinding.DataBindingUtil
import android.os.Bundle
import android.support.v4.app.Fragment
import android.support.v4.content.LocalBroadcastManager
import android.util.Log
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import com.imfreemobile.redeemi.android.redeemiandroid.R
import com.imfreemobile.redeemi.android.redeemiandroid.databinding.FragmentOtpBinding
import com.imfreemobile.redeemi.android.redeemiandroid.utils.RegistrationValidationutils
import com.imfreemobile.redeemi.android.redeemiandroid.view.fragment.BaseFragment
import com.imfreemobile.redeemi.android.redeemiandroid.viewmodel.OTPFragmentViewModel
import com.marcinorlowski.fonty.Fonty
import timber.log.Timber
import java.lang.IllegalArgumentException

/**
 * A simple [Fragment] subclass.
 * Activities that contain this fragment must implement the
 * [OTPFragment.OnOtpFragmentNextButtonClickListener] interface
 * to handle interaction events.
 * Use the [OTPFragment.newInstance] factory method to
 * create an instance of this fragment.
 */
class OTPFragment : BaseFragment() {

    private var verificationUuid: String? = null
    private var phoneNumber: String? = null

    var mBinding: FragmentOtpBinding? = null
    var mViewModel: OTPFragmentViewModel? = null
    var mListener: OTPFragment.OnOtpSuccessListener? = null
    private val codeReceiver = object : BroadcastReceiver() {
        override fun onReceive(context: Context, intent: Intent) {
            val code = intent.getStringExtra("code")
            mViewModel?.setCodeAndValidate(code)
            Timber.d(TAG, "code: $code")
        }
    }

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        if (arguments != null) {
            verificationUuid = arguments.getString(ARG_VERIFICATION_UUID)
            phoneNumber = arguments.getString(ARG_PHONE_NUMBER)
        }
        mViewModel = OTPFragmentViewModel(activity.application, mListener, this@OTPFragment, phoneNumber, verificationUuid)
    }

    override fun onResume() {
        Log.d(TAG, "otp onresume");
        LocalBroadcastManager.getInstance(activity).registerReceiver(codeReceiver, IntentFilter("sms-code-received"))
        super.onResume()
    }

    override fun onPause() {
        Log.d(TAG, "otp onpause");
        LocalBroadcastManager.getInstance(activity).unregisterReceiver(codeReceiver)
        super.onPause()
    }

    override fun onCreateView(inflater: LayoutInflater?, container: ViewGroup?,
                              savedInstanceState: Bundle?): View? {
        super.onCreateView(inflater, container, savedInstanceState)
        mBinding = DataBindingUtil.inflate(inflater, R.layout.fragment_otp, container, false)
        mBinding?.viewModel = mViewModel
        Fonty.setFonts(mBinding?.root as ViewGroup)
        return mBinding?.root
    }

    override fun onViewCreated(view: View?, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)

        mBinding?.etOtp?.addTextChangedListener(RegistrationValidationutils.VerificationCodeTextWatcher(object : RegistrationValidationutils.DigitsTextWatcherCallback {
            override fun onInput() {
                mViewModel?.hideError()
            }

            override fun onMaxCharsReached() {
                mViewModel?.hideKeyboard()
                mViewModel?.nextEnabled = true
            }

            override fun onMaxCharsNotReached() {
                mViewModel?.nextEnabled = false
            }

        }))
    }

    fun startTimer() {
        mViewModel?.startTimer()
    }

    override fun onAttach(context: Context?) {
        super.onAttach(context)
        if (context is OTPFragment.OnOtpSuccessListener) {
            mListener = context
        } else {
            throw IllegalArgumentException("${context.toString()} must implement ${OnOtpSuccessListener::class.java.simpleName}")
        }
    }

    override fun onDetach() {
        super.onDetach()
        mListener == null
        mViewModel?.stopTimer()
    }

    companion object {
        private val TAG = OTPFragment::class.java.simpleName
        private val ARG_VERIFICATION_UUID = "verification_uuid"
        private val ARG_PHONE_NUMBER = "phone_number"

        fun newInstance(): OTPFragment {
            val fragment = OTPFragment()
            return fragment
        }

        fun newInstance(verificationUuid: String?, phoneNumber: String?): OTPFragment {
            val fragment = OTPFragment()
            val args = Bundle()
            args.putString(ARG_VERIFICATION_UUID, verificationUuid)
            args.putString(ARG_PHONE_NUMBER, phoneNumber)
            fragment.arguments = args
            return fragment
        }
    }

    public fun setVerificationAndMobileNumber(verificationUuid: String?, phoneNumber: String?) {
        this.verificationUuid = verificationUuid
        this.phoneNumber = phoneNumber
        mViewModel?.number = phoneNumber
        mViewModel?.setVerificationUuid(verificationUuid)
    }

    public fun showSnackbar(text: String) {
        mViewModel?.showSnackbar(mBinding?.root, text)
    }

    interface OnOtpSuccessListener {
        fun onOtpSuccess(accessToken: String);
    }
}
