package com.imfreemobile.redeemi.android.redeemiandroid.viewmodel;

import android.app.Application;
import android.content.Intent;
import android.support.design.widget.Snackbar;
import android.util.Log;
import android.view.View;

import com.imfreemobile.redeemi.android.redeemiandroid.R;
import com.imfreemobile.redeemi.android.redeemiandroid.model.entity.Voucher;
import com.imfreemobile.redeemi.android.redeemiandroid.model.networkmodel.EnvelopedVoucher;
import com.imfreemobile.redeemi.android.redeemiandroid.model.networkmodel.Error;
import com.imfreemobile.redeemi.android.redeemiandroid.utils.AuthUtils;
import com.imfreemobile.redeemi.android.redeemiandroid.utils.networkutils.NoConnectivityException;
import com.imfreemobile.redeemi.android.redeemiandroid.view.ConfirmRedemptionActivity;
import com.imfreemobile.redeemi.android.redeemiandroid.view.PromoStatusActivity;
import com.imfreemobile.redeemi.android.redeemiandroid.view.ScanActivity;

import java.io.IOException;
import java.io.UnsupportedEncodingException;
import java.net.URLEncoder;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

/**
 * Created by renzmanacmol on 24/10/2017.
 */

public class ScanViewModel extends BaseViewModel {

    private static final String TAG = ScanViewModel.class.getSimpleName();

    private ScanActivity mActivity;
    public static final int REQUEST_CODE_QR_DETECTED = 234;
    private Call<EnvelopedVoucher> networkCallGetVoucherDetails;

    public ScanActivity getmActivity() {
        return mActivity;
    }

    public ScanViewModel() {

    }

    public ScanViewModel(Application application, ScanActivity scanActivity) {
        super(application, scanActivity);
        this.mActivity = scanActivity;
    }


    public void onBarcodeDetected(Voucher voucher) {
        Intent i = new Intent(mActivity, ConfirmRedemptionActivity.class);
        i.putExtra("voucher", voucher);
        mActivity.startActivityForResult(i, REQUEST_CODE_QR_DETECTED);
    }

    public void onBackPressed() {
        getmActivity().onBackPressed();
    }

    public void onNoInternetConnection(View view) {
        Snackbar.make(view, mActivity.getString(R.string.error_connection), Snackbar.LENGTH_LONG).show();
        mActivity.initBarcodeScanning();
    }

    public void onQRCodeInvalid(View view) {
        Snackbar.make(view, R.string.scan_invalid_qr, Snackbar.LENGTH_LONG).show();
        mActivity.initBarcodeScanning();
    }

    public void handleError(String string) {
        try {
            Error error = gson.fromJson(string, EnvelopedVoucher.class).getError();
            if (error.getErrorCode() == 1000) {
                mActivity.showQRInvalid();
            } else {
                Intent i = new Intent(mActivity, PromoStatusActivity.class);
                i.putExtra("error_code", error.getErrorCode());
                mActivity.startActivity(i);
                mActivity.finish();
            }
        } catch (Exception e) {
//            Log.d(TAG, "error string: " + string);
            mActivity.showQRInvalid();
        }
    }

    public void cancelNetCall() {
        if (networkCallGetVoucherDetails != null) {
            networkCallGetVoucherDetails.cancel();
            networkCallGetVoucherDetails = null;
        }
    }

    public void fetchVoucherInfo(final String voucherId) {
        mActivity.showLoadingDialog();
        try {
            networkCallGetVoucherDetails = apiService.getVoucherDetails(AuthUtils.INSTANCE.getAccessHeaderFormattedToken(appDatabase.getTokenDao().getToken().getAccessToken()), URLEncoder.encode(voucherId, "UTF-8"));
            networkCallGetVoucherDetails.enqueue(new Callback<EnvelopedVoucher>() {
                @Override
                public void onResponse(Call<EnvelopedVoucher> call, Response<EnvelopedVoucher> response) {
                    mActivity.hideLoadingDialog();
                    if (response.isSuccessful()) {
                        onBarcodeDetected(response.body().getData());
                    } else {
                        // handle error
                        try {
                            handleError(response.errorBody().string());
                        } catch (IOException | NullPointerException e) {
                            e.printStackTrace();
                            mActivity.showQRInvalid();
                        }
                    }
                }

                @Override
                public void onFailure(Call<EnvelopedVoucher> call, Throwable t) {
                    mActivity.hideLoadingDialog();
                    if (t instanceof NoConnectivityException) {
                        mActivity.showNoInternetConnection();
                    } else {
                        handleError(t.getMessage());
                    }
                }
            });
        } catch (UnsupportedEncodingException e) {
            e.printStackTrace();
        }
    }

}
