package com.imfreemobile.redeemi.android.redeemiandroid.model.entity

import com.google.gson.annotations.Expose
import com.google.gson.annotations.SerializedName

/**
 * Created by jvillarey on 13/11/2017.
 */

class VerificationOtpRequest(@Expose
                             @SerializedName(VERIFICATION_CODE)
                             private val verificationCode: String,
                             @Expose
                             @SerializedName(VERIFICATION_UUID)
                             private val verificationUuid: String) {

    companion object {
        const val VERIFICATION_CODE = "verification_code"
        const val VERIFICATION_UUID = "verification_uuid"
    }
}