package com.imfreemobile.redeemi.android.redeemiandroid

import com.imfreemobile.redeemi.android.redeemiandroid.model.entity.LoginRequest
import com.imfreemobile.redeemi.android.redeemiandroid.model.entity.Password
import com.imfreemobile.redeemi.android.redeemiandroid.model.entity.RegisterRequest
import com.imfreemobile.redeemi.android.redeemiandroid.model.entity.VerificationOtpRequest
import com.imfreemobile.redeemi.android.redeemiandroid.model.entity.VerificationRequest
import com.imfreemobile.redeemi.android.redeemiandroid.model.entity.WithdrawRequest
import com.imfreemobile.redeemi.android.redeemiandroid.model.networkmodel.EnvelopedIncomeBreakdown
import com.imfreemobile.redeemi.android.redeemiandroid.model.networkmodel.EnvelopedStatus
import com.imfreemobile.redeemi.android.redeemiandroid.model.networkmodel.EnvelopedTransaction
import com.imfreemobile.redeemi.android.redeemiandroid.model.networkmodel.EnvelopedTransactionHistory
import com.imfreemobile.redeemi.android.redeemiandroid.model.networkmodel.EnvelopedUser
import com.imfreemobile.redeemi.android.redeemiandroid.model.networkmodel.EnvelopedUserLogin
import com.imfreemobile.redeemi.android.redeemiandroid.model.networkmodel.EnvelopedVerification
import com.imfreemobile.redeemi.android.redeemiandroid.model.networkmodel.EnvelopedVerificationOtp
import com.imfreemobile.redeemi.android.redeemiandroid.model.networkmodel.EnvelopedVoucher
import com.imfreemobile.redeemi.android.redeemiandroid.model.networkmodel.EnvelopedWithdrawTransaction
import com.imfreemobile.redeemi.android.redeemiandroid.model.networkmodel.EnvelopedWithdrawalHistory

import retrofit2.Call
import retrofit2.http.Body
import retrofit2.http.GET
import retrofit2.http.Header
import retrofit2.http.POST
import retrofit2.http.Path
import retrofit2.http.Query

/**
 * Created by renzmanacmol on 23/10/2017.
 */

interface ApiService {

    @GET(STORE_DETAILS)
    fun getStoreDetails(@Header("Authorization") tokenHeader: String): Call<EnvelopedUser>

    @GET(VOUCHER_DETAILS)
    fun getVoucherDetails(@Header("Authorization") tokenHeader: String, @Path("voucher_id") voucherId: String): Call<EnvelopedVoucher>

    @POST(REDEEM_VOUCHER)
    fun redeemVoucher(@Header("Authorization") tokenHeader: String, @Path("voucher_id") voucherId: String, @Body body: String): Call<EnvelopedTransaction>

    @GET(TRANSACTIONS_SUMMARY)
    fun getTransactionsSummary(@Header("Authorization") tokenHeader: String, @Query("date_from") dateFrom: String, @Query("date_to") dateTo: String): Call<EnvelopedIncomeBreakdown>

    @GET(TRANSACTIONS_HISTORY)
    fun getTransactionsHistory(@Header("Authorization") tokenHeader: String, @Query("page") page: Int): Call<EnvelopedTransactionHistory>

    @POST(VERIFICATION)
    fun verify(@Body verificationRequest: VerificationRequest): Call<EnvelopedVerification>

    @POST(LOGIN)
    fun login(@Body loginRequest: LoginRequest): Call<EnvelopedUserLogin>

    @POST(VERIFICATION_OTP)
    fun verifyOtp(@Body verificationOtpRequest: VerificationOtpRequest): Call<EnvelopedVerificationOtp>

    @POST(REGISTER)
    fun register(@Header("Authorization") header: String, @Body registerRequest: RegisterRequest): Call<EnvelopedUser>

    @POST(FORGOT_PASSWORD)
    fun forgotPassword(@Body verificationRequest: VerificationRequest): Call<EnvelopedVerification>

    @POST(SET_PASSWORD)
    fun setPassword(@Header("Authorization") header: String, @Body password: Password): Call<EnvelopedStatus>

    @POST(LOGOUT)
    fun logout(@Header("Authorization") header: String): Call<Void>

    @GET(GET_ACTIVE_WITHDRAW_TRANSACTION)
    fun getActiveWithdrawTransaction(@Header("Authorization") header: String): Call<EnvelopedWithdrawTransaction>

    @POST(WITHDRAW)
    fun withdraw(@Header("Authorization") header: String, @Body withdrawRequest: WithdrawRequest): Call<EnvelopedWithdrawTransaction>

    @GET(WITHDRAWALS)
    fun getWithdrawalHistory(@Header("Authorization") header: String, @Query("page") page: Int): Call<EnvelopedWithdrawalHistory>

    companion object {

        const val STORE_DETAILS = "stores/users/detail"
        const val VOUCHER_DETAILS = "vouchers/{voucher_id}/detail"
        const val REDEEM_VOUCHER = "vouchers/{voucher_id}/redeem"
        const val TRANSACTIONS_SUMMARY = "transactions/summary"
        const val TRANSACTIONS_HISTORY = "transactions"
        const val VERIFICATION = "verification"
        const val VERIFICATION_OTP = "verification/otp"
        const val LOGIN = "login"
        const val REGISTER = "register"
        const val FORGOT_PASSWORD = "forgot-password"
        const val SET_PASSWORD = "set-password"
        const val LOGOUT = "logout"
        const val GET_ACTIVE_WITHDRAW_TRANSACTION = "withdraw/active"
        const val WITHDRAW = "withdraw"
        const val WITHDRAWALS = "withdrawals"
    }
}