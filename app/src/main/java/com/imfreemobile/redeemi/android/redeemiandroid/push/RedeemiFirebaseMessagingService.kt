package com.imfreemobile.redeemi.android.redeemiandroid.push

import android.content.Intent
import android.support.v4.content.LocalBroadcastManager
import com.google.firebase.messaging.FirebaseMessagingService
import com.google.firebase.messaging.RemoteMessage
import timber.log.Timber

/**
 * Created by Joseph on 25/10/2017.
 */

class RedeemiFirebaseMessagingService : FirebaseMessagingService() {

    override fun onMessageReceived(remoteMessage: RemoteMessage) {
        if (remoteMessage.data.size > 0) {
            Timber.d(TAG, "data: ${remoteMessage.data.toString()}")
            if (remoteMessage.data.containsKey("type")) {
                val i = Intent(packageName + "profile_updated")
                LocalBroadcastManager.getInstance(this@RedeemiFirebaseMessagingService).sendBroadcast(i)
            }
        }
    }

    companion object {
        private val TAG = RedeemiFirebaseMessagingService::class.java.simpleName
    }
}