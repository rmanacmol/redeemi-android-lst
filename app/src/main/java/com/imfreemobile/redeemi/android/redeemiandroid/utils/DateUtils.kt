package com.imfreemobile.redeemi.android.redeemiandroid.utils

import java.text.DateFormat
import java.text.ParseException
import java.text.SimpleDateFormat
import java.util.Calendar
import java.util.Date
import java.util.Locale
import java.util.concurrent.TimeUnit

import timber.log.Timber

/**
 * Created by jvillarey on 07/11/2017.
 */

class DateUtils {
    companion object {
        private val TAG = DateUtils::class.java.simpleName
        const val SERVER_DATE_FORMAT = "yyyy-MM-dd"
        const val DISPLAY_FULL_DATE_FORMAT = "MMM dd, yyyy"
        const val DISPLAY_SHORT_DISPLAY_FORMAT = "MMM dd"

        @JvmStatic
        fun getApiCallFormattedStringFromCalendar(calendar: Calendar): String {
            val dateFormat = SimpleDateFormat(SERVER_DATE_FORMAT, Locale.US)
            val date = Date(calendar.timeInMillis)
            return dateFormat.format(date)
        }

        @JvmStatic
        fun getCalendarFromYearMonthDay(year: Int, month: Int, day: Int): Calendar {
            val calendar = Calendar.getInstance()
            calendar.set(year, month, day)
            return calendar
        }

        @JvmStatic
        fun getCalendarFromDateString(dateString: String): Calendar {
            val dateFormat = SimpleDateFormat(SERVER_DATE_FORMAT, Locale.US)
            try {
                val date = dateFormat.parse(dateString)
                val calendar = Calendar.getInstance()
                calendar.timeInMillis = date.time
                return calendar
            } catch (e: ParseException) {
                e.printStackTrace()
            }

            return Calendar.getInstance()
        }

        @JvmStatic
        fun getCalendarFromUnixTimestamp(timestamp: Long): Calendar {
            val calendar = Calendar.getInstance()
            calendar.timeInMillis = timestamp
            return calendar
        }

        @JvmStatic
        fun getUnixTimestampFromDateString(dateString: String): Long {
            val dateFormat = SimpleDateFormat(SERVER_DATE_FORMAT, Locale.US)
            try {
                val date = dateFormat.parse(dateString)
                Timber.d(TAG, "returning \$date.getTime")
                return date.time
            } catch (e: ParseException) {
                e.printStackTrace()
            }

            Timber.d(TAG, "returning 0")
            return 0
        }

        @JvmStatic
        fun getyyyyMMddformattedCurrentDateString(): String {
            val toFormat = SimpleDateFormat(SERVER_DATE_FORMAT, Locale.US)
            return toFormat.format(Date())
        }

        @JvmStatic
        fun getyyyyMMddFormattedFirstDayOfMonthDateString(): String {
            val toFormat = SimpleDateFormat(SERVER_DATE_FORMAT, Locale.US)
            val calendar = Calendar.getInstance()
            calendar.set(Calendar.DAY_OF_MONTH, 1)
            return toFormat.format(Date(calendar.timeInMillis))
        }

        @JvmStatic
        fun getMMM_dd_yyyyFormattedStringFromDateString(yyyyMMddformattedString: String): String {
            val fromFormat = SimpleDateFormat(SERVER_DATE_FORMAT, Locale.US)
            val toFormat = SimpleDateFormat(DISPLAY_FULL_DATE_FORMAT, Locale.US)
            var date: Date? = null
            try {
                date = fromFormat.parse(yyyyMMddformattedString)
                return toFormat.format(date)
            } catch (e: ParseException) {
                e.printStackTrace()
            }

            return yyyyMMddformattedString
        }

        @JvmStatic
        fun getDatePickerFormattedTextForDate(calendar: Calendar): String {
            val sdf = SimpleDateFormat(DISPLAY_SHORT_DISPLAY_FORMAT, Locale.US)
            return sdf.format(Date(calendar.timeInMillis)) + " ▼"
        }

        @JvmStatic
        fun convertLongToMmSsFormat(millis: Long): String {
            return String.format(Locale.US, "%02d:%02d",
                    TimeUnit.MILLISECONDS.toMinutes(millis) - TimeUnit.HOURS.toMinutes(TimeUnit.MILLISECONDS.toHours(millis)), // The change is in this line
                    TimeUnit.MILLISECONDS.toSeconds(millis) - TimeUnit.MINUTES.toSeconds(TimeUnit.MILLISECONDS.toMinutes(millis)))
        }
    }
}
