package com.imfreemobile.redeemi.android.redeemiandroid.view

import android.databinding.DataBindingUtil
import android.os.Bundle
import android.support.design.widget.Snackbar
import android.support.v4.app.ActivityCompat
import android.support.v4.view.ViewPager
import android.util.Log
import android.view.ViewGroup
import com.afollestad.materialdialogs.MaterialDialog
import com.imfreemobile.redeemi.android.redeemiandroid.R
import com.imfreemobile.redeemi.android.redeemiandroid.databinding.ActivityRegistrationFlowBinding
import com.imfreemobile.redeemi.android.redeemiandroid.model.entity.RegisterRequest
import com.imfreemobile.redeemi.android.redeemiandroid.model.tables.Token
import com.imfreemobile.redeemi.android.redeemiandroid.model.networkmodel.EnvelopedUser
import com.imfreemobile.redeemi.android.redeemiandroid.utils.AuthUtils
import com.imfreemobile.redeemi.android.redeemiandroid.utils.networkutils.NoConnectivityException
import com.imfreemobile.redeemi.android.redeemiandroid.view.adapter.NonSwipeablePagerAdapter
import com.imfreemobile.redeemi.android.redeemiandroid.view.fragment.registration.OTPFragment
import com.imfreemobile.redeemi.android.redeemiandroid.view.fragment.registration.PinConfirmFragment
import com.imfreemobile.redeemi.android.redeemiandroid.view.fragment.registration.PinEnterFragment
import com.imfreemobile.redeemi.android.redeemiandroid.view.fragment.registration.SetupPasswordFragment
import com.imfreemobile.redeemi.android.redeemiandroid.view.fragment.registration1.ConfirmPasswordFragment
import com.marcinorlowski.fonty.Fonty
import org.jetbrains.anko.intentFor
import retrofit2.Call
import retrofit2.Callback
import retrofit2.Response
import timber.log.Timber

class RegistrationFlowActivity : BaseLocationActivity(), OTPFragment.OnOtpSuccessListener, SetupPasswordFragment.OnSetupPasswordListener, ConfirmPasswordFragment.OnConfirmPasswordListener, PinEnterFragment.OnPinEnterListener, PinConfirmFragment.OnPinConfirmedListener {

    override fun isPinRequiredInActivity(): Boolean {
        return false
    }

    private var loading: MaterialDialog? = null
    override fun onPinConfirmed() {
        loading?.show()
        apiService.register(AuthUtils.getAccessHeaderFormattedToken(accessToken), RegisterRequest(pin, AuthUtils.generateHash(password))).enqueue(object : Callback<EnvelopedUser> {
            override fun onResponse(call: Call<EnvelopedUser>?, response: Response<EnvelopedUser>?) {
                loading?.dismiss()
                if (response?.isSuccessful!!) {
                    var user = response.body()?.data
                    user?.mobileNum = phoneNumber
                    appDatabase.userDao.deleteAll()
                    appDatabase.userDao.insert(user)
                    appDatabase.tokenDao.deleteAll()
                    appDatabase.tokenDao.insert(Token(response.body()?.data?.accessToken))
                    startActivity(intentFor<SuccessRegistrationActivity>())
                    ActivityCompat.finishAffinity(this@RegistrationFlowActivity)
                } else {
                    showError(getString(R.string.error_generic))
                }
            }

            override fun onFailure(call: Call<EnvelopedUser>?, t: Throwable?) {
                loading?.dismiss()
                if (t is NoConnectivityException) showError(getString(R.string.error_connection)) else showError(getString(R.string.error_generic))
            }
        })
    }

    private fun showError(errorString: String) {
        Snackbar.make(mBinding?.root!!, errorString, Snackbar.LENGTH_SHORT).show()
    }

    private var pin: String? = null
    override fun onPinSet(pin: String) {
        Timber.d(TAG, "pin entered");
        this.pin = pin
        mBinding?.viewPager?.setCurrentItem(4, true)
        ((mBinding?.viewPager?.adapter as NonSwipeablePagerAdapter).getItem(4) as PinConfirmFragment).setPin(pin)
    }

    private var password: String? = null
    override fun onPasswordSet(password: String) {
        this.password = password
        mBinding?.viewPager?.setCurrentItem(2, true)
        ((mBinding?.viewPager?.adapter as NonSwipeablePagerAdapter).getItem(2) as ConfirmPasswordFragment).setPassword(password)
    }

    override fun onPasswordConfirmed(password: String) {
        mBinding?.viewPager?.setCurrentItem(3, true)
        ((mBinding?.viewPager?.adapter as NonSwipeablePagerAdapter).getItem(3) as PinEnterFragment).showKeyboard()
    }

    private var accessToken: String? = null
    override fun onOtpSuccess(accessToken: String) {
        this.accessToken = accessToken
        mBinding?.viewPager?.setCurrentItem(1, true)
    }

    companion object {
        val TAG = RegistrationFlowActivity::class.java.simpleName
    }

    private var mBinding: ActivityRegistrationFlowBinding? = null
    private var phoneNumber: String? = null
    private var verificationUuid: String? = null
    private var pageChangeListener: ViewPager.OnPageChangeListener? = null

    override fun onResumeComplete() {
    }

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        mBinding = DataBindingUtil.setContentView(this@RegistrationFlowActivity, R.layout.activity_registration_flow)

        verificationUuid = intent.getStringExtra("verification_uuid")
        phoneNumber = intent.getStringExtra("phone_number")

        val registrationPageAdapter = NonSwipeablePagerAdapter(supportFragmentManager).apply {
            addFragment(OTPFragment.newInstance(verificationUuid, phoneNumber), getString(R.string.verify_number))
            addFragment(SetupPasswordFragment.newInstance(), getString(R.string.toolbar_title_setup_password))
            addFragment(ConfirmPasswordFragment.newInstance(), getString(R.string.toolbar_title_setup_password))
            addFragment(PinEnterFragment(), getString(R.string.toolbar_title_setup_account))
            addFragment(PinConfirmFragment(), getString(R.string.toolbar_title_setup_account))
        }
        pageChangeListener = object : ViewPager.OnPageChangeListener {
            override fun onPageScrollStateChanged(state: Int) {
            }

            override fun onPageScrolled(position: Int, positionOffset: Float, positionOffsetPixels: Int) {
            }

            override fun onPageSelected(position: Int) {
                mBinding?.toolbarTitle?.setText(registrationPageAdapter.getPageTitle(position))
                val progress = ((position + 1).toFloat().div(mBinding?.viewPager?.adapter?.count!!) * 100).toInt()
                mBinding?.progressIndicator?.progress = progress
            }
        }
        mBinding?.apply {
            viewPager.apply {
                adapter = registrationPageAdapter
                addOnPageChangeListener(pageChangeListener)
                post(object : Runnable {
                    override fun run() {
                        mBinding?.viewPager?.currentItem?.let { (pageChangeListener as ViewPager.OnPageChangeListener).onPageSelected(it) }
                        ((mBinding?.viewPager?.adapter as NonSwipeablePagerAdapter).getItem(0) as OTPFragment).startTimer()
                    }
                })
            }
            btnBack.setOnClickListener {
                onBackPressed()
            }
        }
        loading = MaterialDialog.Builder(this@RegistrationFlowActivity).title("Loading").content("Please wait").progress(true, 0).build()
        Fonty.setFonts(mBinding?.getRoot() as ViewGroup)
    }

    override fun onBackPressed() {
        if (mBinding?.viewPager?.currentItem == 0 || mBinding?.viewPager?.currentItem == 1) {
            super.onBackPressed()
        } else {
            mBinding?.viewPager?.setCurrentItem(mBinding?.viewPager!!.currentItem - 1, true)
        }
    }
}
