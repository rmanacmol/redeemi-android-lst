package com.imfreemobile.redeemi.android.redeemiandroid.push

import com.google.firebase.iid.FirebaseInstanceId
import com.google.firebase.iid.FirebaseInstanceIdService
import timber.log.Timber

/**
 * Created by Joseph on 25/10/2017.
 */

class RedeemiFirebaseInstanceIdService : FirebaseInstanceIdService() {

    override fun onTokenRefresh() {
        Timber.d(TAG, "refreshed token: ${FirebaseInstanceId.getInstance().token}")
        super.onTokenRefresh()
        // send new token to server
    }

    companion object {
        private val TAG = RedeemiFirebaseInstanceIdService::class.java.simpleName
    }
}