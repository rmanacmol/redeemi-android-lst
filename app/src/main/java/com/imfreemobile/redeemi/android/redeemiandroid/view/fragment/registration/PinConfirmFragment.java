package com.imfreemobile.redeemi.android.redeemiandroid.view.fragment.registration;

import android.content.Context;
import android.databinding.DataBindingUtil;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.alimuzaffar.lib.pin.PinEntryEditText;
import com.imfreemobile.redeemi.android.redeemiandroid.R;
import com.imfreemobile.redeemi.android.redeemiandroid.databinding.FragmentPinConfirmBinding;
import com.imfreemobile.redeemi.android.redeemiandroid.view.fragment.BaseFragment;
import com.imfreemobile.redeemi.android.redeemiandroid.viewmodel.PinConfirmViewModel;
import com.marcinorlowski.fonty.Fonty;

/**
 * Created by rmanacmol on 13/11/2017.
 */

public class PinConfirmFragment extends BaseFragment {

    private static final String TAG = PinConfirmFragment.class.getSimpleName();

    public interface OnPinConfirmedListener {
        void onPinConfirmed();
    }

    public FragmentPinConfirmBinding mBinding;
    private PinConfirmViewModel mViewModel;
    private String pin;
    private OnPinConfirmedListener mListener;

    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        super.onCreateView(inflater, container, savedInstanceState);
        mBinding = DataBindingUtil.inflate(inflater, R.layout.fragment_pin_confirm, container, false);
        mViewModel = new PinConfirmViewModel(getActivity().getApplication(),
                this.getActivity(), mListener, mBinding);
        mBinding.setViewModel(mViewModel);
        Fonty.setFonts((ViewGroup) mBinding.getRoot());
        return mBinding.getRoot();
    }

    @Override
    public void onViewCreated(View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        mBinding.pinView.setOnPinEnteredListener(new PinEntryEditText.OnPinEnteredListener() {
            @Override
            public void onPinEntered(CharSequence str) {
                mViewModel.onPinEntered(str, getView(), pin);
            }
        });
    }

    public void showKeyboard() {
        mBinding.pinView.focus();
    }

    public void setPin(String pin) {
        this.pin = pin;
        showKeyboard();
    }

    @Override
    public void onAttach(Context context) {
        super.onAttach(context);
        if (context instanceof OnPinConfirmedListener) {
            mListener = (OnPinConfirmedListener) context;
        } else {
            throw new IllegalArgumentException(context.getClass().getSimpleName() + " must implement " + OnPinConfirmedListener.class.getSimpleName());
        }
    }

    @Override
    public void onDetach() {
        super.onDetach();
    }
}
