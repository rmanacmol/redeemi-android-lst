package com.imfreemobile.redeemi.android.redeemiandroid.utils

import org.spongycastle.crypto.PBEParametersGenerator
import org.spongycastle.crypto.digests.SHA256Digest
import org.spongycastle.crypto.generators.PKCS5S2ParametersGenerator
import org.spongycastle.crypto.params.KeyParameter
import org.spongycastle.util.encoders.Hex

import java.io.UnsupportedEncodingException

/**
 * Created by jvillarey on 14/11/2017.
 */

object AuthUtils {

    fun getAccessHeaderFormattedToken(token: String?): String {
        return "JWT " + token
    }

    fun generateHash(input: String?): String {
        try {
            val digest = SHA256Digest()
            val combine = input?.toByteArray(charset("UTF-8"))
            digest.update(combine, 0, combine!!.size)
            val out = ByteArray(32)
            digest.doFinal(out, 0)
            return Hex.toHexString(out)
        } catch (e: UnsupportedEncodingException) {
        }

        throw IllegalArgumentException("Unable to generate hash")
    }

    fun generateSalt(password: String, saltInput: String): String {
        try {
            val generator = PKCS5S2ParametersGenerator(SHA256Digest())
            generator.init(PBEParametersGenerator.PKCS5PasswordToBytes(password.toCharArray()), saltInput.toByteArray(charset("UTF-8")), 1000)
            val key = generator.generateDerivedMacParameters(256) as KeyParameter
            return Hex.toHexString(key.key)
        } catch (e: UnsupportedEncodingException) {
            throw IllegalStateException("Cannot generate salt")
        }

    }
}
