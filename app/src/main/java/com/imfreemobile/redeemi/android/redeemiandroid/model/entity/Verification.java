package com.imfreemobile.redeemi.android.redeemiandroid.model.entity;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

/**
 * Created by jvillarey on 13/11/2017.
 */

public class Verification {

    public static final String NOTIFIED = "notified";
    public static final String VERIFICATION_UUID = "verification_uuid";

    @Expose
    @SerializedName(NOTIFIED)
    private boolean notified;

    @Expose
    @SerializedName(VERIFICATION_UUID)
    private String verificationUuid;

    public Verification() {
    }

    public boolean isNotified() {
        return notified;
    }

    public String getVerificationUuid() {
        return verificationUuid;
    }

    @Override
    public String toString() {
        return "{\"Verification\":{"
                + "\"notified\":\"" + notified + "\""
                + ", \"verificationUuid\":\"" + verificationUuid + "\""
                + "}}";
    }
}
