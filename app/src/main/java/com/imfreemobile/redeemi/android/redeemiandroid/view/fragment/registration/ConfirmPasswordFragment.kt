package com.imfreemobile.redeemi.android.redeemiandroid.view.fragment.registration1

import android.content.Context
import android.databinding.DataBindingUtil
import android.os.Bundle
import android.support.v4.app.Fragment
import android.util.Log
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.view.inputmethod.InputMethod
import android.view.inputmethod.InputMethodManager

import com.imfreemobile.redeemi.android.redeemiandroid.R
import com.imfreemobile.redeemi.android.redeemiandroid.databinding.FragmentConfirmPasswordBinding
import com.imfreemobile.redeemi.android.redeemiandroid.utils.RegistrationValidationutils
import com.imfreemobile.redeemi.android.redeemiandroid.view.RegistrationFlowActivity
import com.imfreemobile.redeemi.android.redeemiandroid.view.fragment.BaseFragment
import com.imfreemobile.redeemi.android.redeemiandroid.viewmodel.ConfirmPasswordViewModel
import com.marcinorlowski.fonty.Fonty
import timber.log.Timber

/**
 * A simple [Fragment] subclass.
 * Activities that contain this fragment must implement the
 * [ConfirmPasswordFragment.OnConfirmPasswordListener] interface
 * to handle interaction events.
 * Use the [ConfirmPasswordFragment.newInstance] factory method to
 * create an instance of this fragment.
 */
class ConfirmPasswordFragment : BaseFragment() {

    var mPassword: String? = null

    var mListener: OnConfirmPasswordListener? = null
    var mBinding: FragmentConfirmPasswordBinding? = null
    var mViewModel: ConfirmPasswordViewModel? = null

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        mViewModel = ConfirmPasswordViewModel(activity.application, mListener, this@ConfirmPasswordFragment)
    }

    override fun onCreateView(inflater: LayoutInflater?, container: ViewGroup?,
                              savedInstanceState: Bundle?): View? {
        super.onCreateView(inflater, container, savedInstanceState)
        mBinding = DataBindingUtil.inflate(inflater, R.layout.fragment_confirm_password, container, false)
        mBinding?.viewModel = mViewModel
        Fonty.setFonts(mBinding?.root as ViewGroup)
        return mBinding?.root
    }

    override fun onViewCreated(view: View?, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)

        mBinding?.etPassword?.addTextChangedListener(RegistrationValidationutils.PasswordStrengthTextWatcher(object : RegistrationValidationutils.PasswordStrengthTextWatcherCallback {
            override fun onWeakReached() {
                mViewModel?.nextEnabled = true
            }

            override fun onGoodReached() {
                mViewModel?.nextEnabled = true
            }

            override fun onStrongReached() {
                mViewModel?.nextEnabled = true
            }

            override fun onInvalidPassword() {
                mViewModel?.nextEnabled = false
            }
        }))
    }

    override fun onAttach(context: Context?) {
        super.onAttach(context)
        if (context is OnConfirmPasswordListener) {
            mListener = context
        } else {
            throw RuntimeException(context!!.toString() + " must implement OnConfirmPasswordListener")
        }
    }

    override fun onDetach() {
        super.onDetach()
        mListener = null
    }

    interface OnConfirmPasswordListener {
        fun onPasswordConfirmed(password: String)
    }

    companion object {

        val TAG = ConfirmPasswordFragment::class.java.simpleName

        private val ARG_PASSWORD = "password"

        fun newInstance(): ConfirmPasswordFragment {
            val fragment = ConfirmPasswordFragment()
            return fragment
        }
    }

    fun setPassword(password: String) {
        Timber.d(TAG, "password: $password")
        this.mPassword = password
    }

    fun showSnackbar(text: String) {
        mViewModel?.showSnackbar(mBinding?.root, text)
    }
}
