package com.imfreemobile.redeemi.android.redeemiandroid.view.fragment.withdraw

import android.content.Context
import android.databinding.DataBindingUtil
import android.net.Uri
import android.os.Bundle
import android.support.v4.app.Fragment
import android.support.v7.widget.LinearLayoutManager
import android.util.Log
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import com.imfreemobile.redeemi.android.redeemiandroid.R
import com.imfreemobile.redeemi.android.redeemiandroid.databinding.FragmentWithdrawHistoryBinding
import com.imfreemobile.redeemi.android.redeemiandroid.view.adapter.WithdrawHistoryAdapter
import com.imfreemobile.redeemi.android.redeemiandroid.view.customviews.EndlessRecyclerOnScrollListener
import com.imfreemobile.redeemi.android.redeemiandroid.viewmodel.WithdrawHistoryViewModel
import com.marcinorlowski.fonty.Fonty

/**
 * A simple [Fragment] subclass.
 * Activities that contain this fragment must implement the
 * [WithdrawHistoryFragment.OnWithdrawHistoryListener] interface
 * to handle interaction events.
 * Use the [WithdrawHistoryFragment.newInstance] factory method to
 * create an instance of this fragment.
 */
class WithdrawHistoryFragment : Fragment() {

    private var mListener: OnWithdrawHistoryListener? = null

    var mBinding: FragmentWithdrawHistoryBinding? = null
    var mViewModel: WithdrawHistoryViewModel? = null
    var adapter: WithdrawHistoryAdapter? = null
    var scrollListener: EndlessRecyclerOnScrollListener? = null

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
    }

    override fun onCreateView(inflater: LayoutInflater?, container: ViewGroup?,
                              savedInstanceState: Bundle?): View? {
        super.onCreateView(inflater, container, savedInstanceState)
        mBinding = DataBindingUtil.inflate(inflater, R.layout.fragment_withdraw_history, container, false)
        mViewModel = WithdrawHistoryViewModel(activity.application, this.activity, parentFragment as WithdrawIncomeFragment, this@WithdrawHistoryFragment)
        mBinding?.viewModel = mViewModel
        Fonty.setFonts(mBinding?.root as ViewGroup)
        return mBinding?.root
    }

    override fun onViewCreated(view: View?, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)

        adapter = WithdrawHistoryAdapter()

        mBinding?.recycler?.adapter = adapter
        mBinding?.recycler?.layoutManager = LinearLayoutManager(activity)
        scrollListener = object : EndlessRecyclerOnScrollListener() {
            override fun onLoadMore() {
                mViewModel?.page = mViewModel?.page?.inc()
                mViewModel?.page?.let { mViewModel?.fetchWithdrawalHistory(it) }
            }
        }
        mBinding?.recycler?.addOnScrollListener(scrollListener)
        mBinding?.swipeRefreshLayout?.setOnRefreshListener {
            mViewModel?.page = 1
            mViewModel?.fetchWithdrawalHistory(1)
        }
        mBinding?.swipeRefreshLayout?.setColorSchemeResources(R.color.textColorHint)
    }

    override fun onAttach(context: Context?) {
        super.onAttach(context)
        if (context is OnWithdrawHistoryListener) {
            mListener = context
        } else {
            throw RuntimeException(context!!.toString() + " must implement OnWithdrawFragmentListener")
        }
    }

    override fun onDetach() {
        super.onDetach()
        mListener = null
    }

    interface OnWithdrawHistoryListener {
        // TODO: Update argument type and name
        fun onFragmentInteraction(uri: Uri)
    }

    companion object {

        val TAG = WithdrawHistoryFragment::class.java.simpleName

        fun newInstance(): WithdrawHistoryFragment {
            val fragment = WithdrawHistoryFragment()
            val args = Bundle()
            fragment.arguments = args
            return fragment
        }
    }
}
