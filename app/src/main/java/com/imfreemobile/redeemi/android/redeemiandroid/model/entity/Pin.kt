package com.imfreemobile.redeemi.android.redeemiandroid.model.entity

import com.google.gson.annotations.Expose
import com.google.gson.annotations.SerializedName

/**
 * Created by jvillarey on 16/11/2017.
 */

class Pin(@field:Expose
          @field:SerializedName(PIN)
          private val pin: String) {

    companion object {
        const val PIN = "pin"
    }
}
