package com.imfreemobile.redeemi.android.redeemiandroid.utils.networkutils;

import java.io.IOException;

public class NoConnectivityException extends IOException {
 
    @Override
    public String getMessage() {
        return "No connectivity exception";
    }
 
}