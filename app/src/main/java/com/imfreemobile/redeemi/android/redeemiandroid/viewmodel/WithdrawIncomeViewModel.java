package com.imfreemobile.redeemi.android.redeemiandroid.viewmodel;

import android.app.Activity;
import android.app.Application;

import com.imfreemobile.redeemi.android.redeemiandroid.R;
import com.imfreemobile.redeemi.android.redeemiandroid.view.BottomNavActivity;
import com.imfreemobile.redeemi.android.redeemiandroid.view.adapter.TabsPageAdapter;
import com.imfreemobile.redeemi.android.redeemiandroid.view.fragment.withdraw.WithdrawIncomeFragment;
import com.imfreemobile.redeemi.android.redeemiandroid.view.fragment.withdraw.WithdrawFragment;
import com.imfreemobile.redeemi.android.redeemiandroid.view.fragment.withdraw.WithdrawHistoryFragment;

/**
 * Created by rmanacmol on 06/11/2017.
 */

public class WithdrawIncomeViewModel extends BaseViewModel {

    private static final String TAG = WithdrawIncomeViewModel.class.getSimpleName();

    private BottomNavActivity mActivity;
    private WithdrawIncomeFragment withdrawIncomeFragment;

    public WithdrawIncomeViewModel(Application application, Activity activity, WithdrawIncomeFragment fragment) {
        super(application, activity);
        this.mActivity = (BottomNavActivity) activity;
        this.withdrawIncomeFragment = fragment;
    }

    public void setupPager() {
        TabsPageAdapter withdrawPageAdapter = new TabsPageAdapter(withdrawIncomeFragment.getChildFragmentManager());
        withdrawPageAdapter.addFragment(WithdrawFragment.Companion.newInstance(), mActivity.getString(R.string.withdraw));
        withdrawPageAdapter.addFragment(WithdrawHistoryFragment.Companion.newInstance(), mActivity.getString(R.string.withdraw_history));
        withdrawIncomeFragment.getMBinding().viewPager.setAdapter(withdrawPageAdapter);
        withdrawIncomeFragment.getMBinding().withdrawTab.setupWithViewPager(withdrawIncomeFragment.getMBinding().viewPager);
    }
}
