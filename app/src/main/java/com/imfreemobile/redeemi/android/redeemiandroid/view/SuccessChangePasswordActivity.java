package com.imfreemobile.redeemi.android.redeemiandroid.view;

import android.os.Bundle;
import android.support.annotation.Nullable;
import android.view.View;
import android.view.ViewGroup;

import com.imfreemobile.redeemi.android.redeemiandroid.R;
import com.marcinorlowski.fonty.Fonty;

/**
 * Created by jvillarey on 15/11/2017.
 */

public class SuccessChangePasswordActivity extends BaseLocationActivity {

    @Override
    protected boolean isPinRequiredInActivity() {
        return false;
    }

    private static final String TAG = SuccessChangePasswordActivity.class.getSimpleName();

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_success_change_password);
        Fonty.setFonts(this);
        findViewById(R.id.btn_done).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                setResult(RESULT_OK);
                finish();
            }
        });
    }

    @Override
    protected void onResumeComplete() {

    }
}
