package com.imfreemobile.redeemi.android.redeemiandroid.receivers

import android.content.BroadcastReceiver
import android.content.Context
import android.content.Intent
import android.os.Bundle
import android.support.v4.content.LocalBroadcastManager

import com.google.android.gms.auth.api.phone.SmsRetriever
import com.google.android.gms.common.api.CommonStatusCodes
import com.google.android.gms.common.api.Status

import timber.log.Timber

/**
 * Created by jvillarey on 06/12/2017.
 */

class SMSReceiver : BroadcastReceiver() {

    override fun onReceive(context: Context, intent: Intent) {
        if (SmsRetriever.SMS_RETRIEVED_ACTION == intent.action) {
            val extras = intent.extras
            val status = extras!!.get(SmsRetriever.EXTRA_STATUS) as Status

            when (status.statusCode) {
                CommonStatusCodes.SUCCESS -> {
                    val message = extras.getString(SmsRetriever.EXTRA_SMS_MESSAGE)
                    Timber.d(TAG, "message: \$message")
                    val code = message!!.replace(com.imfreemobile.redeemi.android.redeemiandroid.BuildConfig.OTP_PREFIX, "").substring(0, 4)
                    val i = Intent("sms-code-received")
                    i.putExtra("code", code)
                    LocalBroadcastManager.getInstance(context).sendBroadcast(i)
                }
                CommonStatusCodes.TIMEOUT -> Timber.d(TAG, "timed out")
            }
        }
    }

    companion object {
        private val TAG = SMSReceiver::class.java.simpleName
    }
}