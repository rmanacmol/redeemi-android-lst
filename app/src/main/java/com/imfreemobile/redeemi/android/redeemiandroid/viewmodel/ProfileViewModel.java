package com.imfreemobile.redeemi.android.redeemiandroid.viewmodel;

import android.app.Application;
import android.databinding.BindingAdapter;
import android.databinding.ObservableField;
import android.text.TextUtils;
import android.widget.ImageView;

import com.imfreemobile.redeemi.android.redeemiandroid.BR;
import com.imfreemobile.redeemi.android.redeemiandroid.R;
import com.imfreemobile.redeemi.android.redeemiandroid.model.tables.User;
import com.imfreemobile.redeemi.android.redeemiandroid.view.ProfileActivity;
import com.squareup.picasso.Picasso;

import jp.wasabeef.picasso.transformations.CropCircleTransformation;

/**
 * Created by jvillarey on 16/11/2017.
 */

public class ProfileViewModel extends BaseViewModel {

    private static final String TAG = ProfileViewModel.class.getSimpleName();

    private ProfileActivity mActivity;
    private ObservableField<User> user = new ObservableField<>();

    public User getUser() {
        return this.user.get();
    }

    public void setUser(User user) {
        this.user.set(user);
        notifyPropertyChanged(BR._all);
    }

    public ProfileViewModel(Application application, ProfileActivity activity) {
        super(application, activity);
        this.mActivity = activity;
        setUser(appDatabase.getUserDao().getUser());
    }

    @BindingAdapter({"bind:imageUrl"})
    public static void loadImage(ImageView imageView, String imageUrl) {
        if (imageUrl != null && !TextUtils.isEmpty(imageUrl)) {
            Picasso.with(imageView.getContext())
                    .load(imageUrl)
                    .error(R.drawable.icon_profile)
                    .placeholder(R.drawable.icon_profile)
                    .transform(new CropCircleTransformation())
                    .into(imageView);
        } else {
            Picasso.with(imageView.getContext()).load(R.drawable.icon_profile).into(imageView);
        }
    }
}
