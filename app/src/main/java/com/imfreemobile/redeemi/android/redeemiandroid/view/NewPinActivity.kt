package com.imfreemobile.redeemi.android.redeemiandroid.view

import android.app.Activity
import android.content.Intent
import android.databinding.DataBindingUtil
import android.os.Bundle
import android.support.v4.app.ActivityCompat
import android.support.v4.view.ViewPager
import android.view.ViewGroup
import com.afollestad.materialdialogs.MaterialDialog
import com.imfreemobile.redeemi.android.redeemiandroid.R
import com.imfreemobile.redeemi.android.redeemiandroid.databinding.ActivityNewPinBinding
import com.imfreemobile.redeemi.android.redeemiandroid.view.adapter.NonSwipeablePagerAdapter
import com.imfreemobile.redeemi.android.redeemiandroid.view.fragment.registration.PinConfirmFragment
import com.imfreemobile.redeemi.android.redeemiandroid.view.fragment.registration.PinEnterFragment
import com.marcinorlowski.fonty.Fonty
import org.jetbrains.anko.intentFor
import timber.log.Timber

class NewPinActivity : BaseLocationActivity(), PinEnterFragment.OnPinEnterListener, PinConfirmFragment.OnPinConfirmedListener {

    override fun onActivityResult(requestCode: Int, resultCode: Int, data: Intent?) {
        super.onActivityResult(requestCode, resultCode, data)
        if (requestCode == 10 && resultCode == Activity.RESULT_OK) {
            startActivity(intentFor<BottomNavActivity>())
            ActivityCompat.finishAffinity(this@NewPinActivity)
        }
    }

    override fun isPinRequiredInActivity(): Boolean {
        return false
    }

    override fun onResumeComplete() {
    }

    companion object {
        val TAG = NewPinActivity::class.java.simpleName
    }

    private var pin: String? = null
    override fun onPinSet(pin: String?) {
        this.pin = pin
        mBinding?.viewPager?.setCurrentItem(1, true)
        ((mBinding?.viewPager?.adapter as NonSwipeablePagerAdapter).getItem(1) as PinConfirmFragment).setPin(pin)
    }

    override fun onPinConfirmed() {
        Timber.d(TAG, "pin confirmed: $pin")
    }

    private var mBinding: ActivityNewPinBinding? = null
    private var pageChangeListener: ViewPager.OnPageChangeListener? = null
    private var loading: MaterialDialog? = null

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        mBinding = DataBindingUtil.setContentView(this@NewPinActivity, R.layout.activity_new_pin)

        val newPinPageAdapter = NonSwipeablePagerAdapter(supportFragmentManager).apply {
            addFragment(PinEnterFragment(), getString(R.string.toolbar_title_new_pin))
            addFragment(PinConfirmFragment(), getString(R.string.toolbar_title_new_pin))
        }
        pageChangeListener = object : ViewPager.OnPageChangeListener {
            override fun onPageScrollStateChanged(state: Int) {
            }

            override fun onPageScrolled(position: Int, positionOffset: Float, positionOffsetPixels: Int) {
            }

            override fun onPageSelected(position: Int) {
                mBinding?.toolbarTitle?.setText(newPinPageAdapter.getPageTitle(position))
            }
        }
        mBinding?.apply {
            viewPager.apply {
                adapter = newPinPageAdapter
                addOnPageChangeListener(pageChangeListener)
                post(object : Runnable {
                    override fun run() {
                        mBinding?.viewPager?.currentItem?.let { (pageChangeListener as ViewPager.OnPageChangeListener).onPageSelected(it) }
                    }
                })
            }
            btnBack.setOnClickListener {
                onBackPressed()
            }
        }

        loading = MaterialDialog.Builder(this@NewPinActivity).title("Loading").content("Please wait").progress(true, 0).build()

        Fonty.setFonts(mBinding?.root as ViewGroup)
    }

    override fun onBackPressed() {
        if (mBinding?.viewPager?.currentItem == 0) {
            super.onBackPressed()
        } else {
            mBinding?.viewPager?.setCurrentItem(mBinding?.viewPager!!.currentItem - 1, true)
        }
    }
}
