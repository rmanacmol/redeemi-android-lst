package com.imfreemobile.redeemi.android.redeemiandroid.utils;

/**
 * Created by jvillarey on 22/11/2017.
 */

public class CurrencyUtils {

    private static final String TAG = CurrencyUtils.class.getSimpleName();

    public static boolean isGreatherThanOrEqualTo(String currencyText, float valueToCompareWith) {
        float currency = Float.valueOf(currencyText);
        return currency >= valueToCompareWith;
    }

    public static String stripCurrencyFromAmountString(String amountString) {
        return amountString.replace("Php ", "").trim();
    }

}
