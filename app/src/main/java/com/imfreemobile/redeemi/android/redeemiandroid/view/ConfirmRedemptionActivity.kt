package com.imfreemobile.redeemi.android.redeemiandroid.view

import android.databinding.DataBindingUtil
import android.os.Bundle
import android.view.View
import android.view.ViewGroup
import com.afollestad.materialdialogs.MaterialDialog
import com.imfreemobile.redeemi.android.redeemiandroid.R
import com.imfreemobile.redeemi.android.redeemiandroid.databinding.ActivityConfirmRedemptionBinding
import com.imfreemobile.redeemi.android.redeemiandroid.model.entity.Voucher
import com.imfreemobile.redeemi.android.redeemiandroid.viewmodel.ConfirmRedemptionViewModel
import com.marcinorlowski.fonty.Fonty
import kotlinx.android.synthetic.main.layout_toolbar.*

/**
 * Created by renzmanacmol on 24/10/2017.
 */

class ConfirmRedemptionActivity : BaseLocationActivity() {

    override fun isPinRequiredInActivity(): Boolean {
        return true
    }

    override fun onResumeComplete() {
    }

    private var mBinding: ActivityConfirmRedemptionBinding? = null
    private var mViewModel: ConfirmRedemptionViewModel? = null
    private var loadingDialog: MaterialDialog? = null

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        mBinding = DataBindingUtil.setContentView(this, R.layout.activity_confirm_redemption)
        mViewModel = ConfirmRedemptionViewModel(application, this, Voucher())
        mBinding?.viewModel = mViewModel
        setSupportActionBar(toolbar)
        supportActionBar?.apply {
            setDisplayHomeAsUpEnabled(true)
            setDisplayShowHomeEnabled(true)
            setDisplayShowTitleEnabled(false)
        }
        mBinding?.toolbarLayout?.toolbar?.setNavigationOnClickListener(object : View.OnClickListener {
            override fun onClick(p0: View?) {
                onBackPressed()
            }
        })
        mBinding?.toolbarLayout?.toolbarTitle?.setText(getString(R.string.confirm_redemption_title))

        mViewModel?.setVoucher(intent.getSerializableExtra("voucher") as Voucher?)

        Fonty.setFonts(mBinding?.root as ViewGroup)
    }

    override fun onPause() {
        super.onPause()
        mViewModel?.cancelNetCall()
    }

    fun showLoadingDialog() {
        if (loadingDialog == null) {
            loadingDialog = MaterialDialog.Builder(this@ConfirmRedemptionActivity).content("Loading").progress(true, 0).build()
        }
        loadingDialog?.show()
    }

    fun hideLoadingDialog() {
        loadingDialog?.dismiss()
    }

}
