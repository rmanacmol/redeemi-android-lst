package com.imfreemobile.redeemi.android.redeemiandroid.view.fragment.transaction;

import android.databinding.DataBindingUtil;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.imfreemobile.redeemi.android.redeemiandroid.R;
import com.imfreemobile.redeemi.android.redeemiandroid.databinding.FragmentTransactionBinding;
import com.imfreemobile.redeemi.android.redeemiandroid.view.fragment.BaseFragment;
import com.imfreemobile.redeemi.android.redeemiandroid.viewmodel.TransactionViewModel;
import com.marcinorlowski.fonty.Fonty;

/**
 * Created by rmanacmol on 06/11/2017.
 */

public class TransactionFragment extends BaseFragment {

    public FragmentTransactionBinding mBinding;
    public TransactionViewModel mViewModel;

    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        super.onCreateView(inflater,container,savedInstanceState);
        mBinding = DataBindingUtil.inflate(inflater, R.layout.fragment_transaction, container, false);
        mViewModel = new TransactionViewModel(getActivity().getApplication(), this.getActivity(), this);
        mBinding.setViewModel(mViewModel);
        Fonty.setFonts((ViewGroup) mBinding.getRoot());
        return mBinding.getRoot();
    }

    public FragmentTransactionBinding getmBinding() {
        return mBinding;
    }

    @Override
    public void onResume() {
        super.onResume();
        mViewModel.setupPager();
    }

    public void showError(String errorString) {
        mViewModel.showError(mBinding.getRoot(), errorString);
    }
}
