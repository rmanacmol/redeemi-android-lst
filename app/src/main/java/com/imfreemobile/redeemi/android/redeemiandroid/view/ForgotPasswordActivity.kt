package com.imfreemobile.redeemi.android.redeemiandroid.view

import android.app.Activity
import android.content.Intent
import android.databinding.DataBindingUtil
import android.os.Bundle
import android.support.design.widget.Snackbar
import android.support.v4.view.ViewPager
import android.view.ViewGroup
import com.afollestad.materialdialogs.MaterialDialog
import com.imfreemobile.redeemi.android.redeemiandroid.R
import com.imfreemobile.redeemi.android.redeemiandroid.databinding.ActivityForgotPasswordBinding
import com.imfreemobile.redeemi.android.redeemiandroid.model.entity.Password
import com.imfreemobile.redeemi.android.redeemiandroid.model.networkmodel.EnvelopedStatus
import com.imfreemobile.redeemi.android.redeemiandroid.utils.AuthUtils
import com.imfreemobile.redeemi.android.redeemiandroid.utils.networkutils.NoConnectivityException
import com.imfreemobile.redeemi.android.redeemiandroid.view.adapter.NonSwipeablePagerAdapter
import com.imfreemobile.redeemi.android.redeemiandroid.view.fragment.forgot.MobileNumberFragment
import com.imfreemobile.redeemi.android.redeemiandroid.view.fragment.registration.OTPFragment
import com.imfreemobile.redeemi.android.redeemiandroid.view.fragment.registration.SetupPasswordFragment
import com.imfreemobile.redeemi.android.redeemiandroid.view.fragment.registration1.ConfirmPasswordFragment
import com.marcinorlowski.fonty.Fonty
import org.jetbrains.anko.intentFor
import retrofit2.Call
import retrofit2.Callback
import retrofit2.Response

class ForgotPasswordActivity : BaseLocationActivity(), MobileNumberFragment.OnMobileNumberEnteredListener, OTPFragment.OnOtpSuccessListener, SetupPasswordFragment.OnSetupPasswordListener, ConfirmPasswordFragment.OnConfirmPasswordListener {

    override fun isPinRequiredInActivity(): Boolean {
        return false
    }

    private var loading: MaterialDialog? = null
    override fun onPasswordConfirmed(password: String) {
        apiService.setPassword(AuthUtils.getAccessHeaderFormattedToken(accessToken), Password(AuthUtils.generateHash(password))).enqueue(object : Callback<EnvelopedStatus> {
            override fun onResponse(call: Call<EnvelopedStatus>?, response: Response<EnvelopedStatus>?) {
                loading?.dismiss()
                if (response?.isSuccessful!!) {
                    startActivityForResult(intentFor<SuccessChangePasswordActivity>(), 10)
                } else {
                    showError(getString(R.string.error_generic))
                }
            }

            override fun onFailure(call: Call<EnvelopedStatus>?, t: Throwable?) {
                loading?.dismiss()
                if (t is NoConnectivityException) showError(getString(R.string.error_connection)) else showError(getString(R.string.error_generic))
            }
        })
    }

    private fun showError(errorString: String) {
        Snackbar.make(mBinding?.root!!, errorString, Snackbar.LENGTH_SHORT).show()
    }

    override fun onActivityResult(requestCode: Int, resultCode: Int, data: Intent?) {
        super.onActivityResult(requestCode, resultCode, data)
        if (resultCode == Activity.RESULT_OK && requestCode == 10) {
            finish()
        }
    }

    private var password: String? = null
    override fun onPasswordSet(password: String) {
        this.password = password
        mBinding?.viewPager?.setCurrentItem(3, true)
        ((mBinding?.viewPager?.adapter as NonSwipeablePagerAdapter).getItem(3) as ConfirmPasswordFragment).setPassword(password)
    }

    private var accessToken: String? = null
    override fun onOtpSuccess(accessToken: String) {
        this.accessToken = accessToken
        mBinding?.viewPager?.setCurrentItem(2, true)
    }

    override fun onMobileNumberEntered(mobileNumber: String, verificationUuid: String) {
        mBinding?.viewPager?.setCurrentItem(1, true)
        ((mBinding?.viewPager?.adapter as NonSwipeablePagerAdapter).getItem(1) as OTPFragment).setVerificationAndMobileNumber(verificationUuid, mobileNumber)
        ((mBinding?.viewPager?.adapter as NonSwipeablePagerAdapter).getItem(1) as OTPFragment).startTimer()
    }

    override fun onResumeComplete() {
    }

    companion object {
        val TAG = ForgotPasswordActivity::class.java.simpleName
    }

    private var mBinding: ActivityForgotPasswordBinding? = null
    private var pageChangeListener: ViewPager.OnPageChangeListener? = null

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        mBinding = DataBindingUtil.setContentView(this@ForgotPasswordActivity, R.layout.activity_forgot_password)

        val forgotPasswordPageAdapter = NonSwipeablePagerAdapter(supportFragmentManager).apply {
            addFragment(MobileNumberFragment.newInstance(), getString(R.string.forgot_password_toolbar_title))
            addFragment(OTPFragment.newInstance(), getString(R.string.forgot_password_toolbar_title))
            addFragment(SetupPasswordFragment.newInstance(), getString(R.string.forgot_password_toolbar_title))
            addFragment(ConfirmPasswordFragment.newInstance(), getString(R.string.forgot_password_toolbar_title))
        }
        pageChangeListener = object : ViewPager.OnPageChangeListener {
            override fun onPageScrollStateChanged(state: Int) {
            }

            override fun onPageScrolled(position: Int, positionOffset: Float, positionOffsetPixels: Int) {
            }

            override fun onPageSelected(position: Int) {
                mBinding?.toolbarTitle?.setText(forgotPasswordPageAdapter.getPageTitle(position))
                val progress = ((position + 1).toFloat().div(mBinding?.viewPager?.adapter?.count!!) * 100).toInt()
                mBinding?.progressIndicator?.progress = progress
            }
        }
        mBinding?.apply {
            viewPager.apply {
                adapter = forgotPasswordPageAdapter
                addOnPageChangeListener(pageChangeListener)
                post(object : Runnable {
                    override fun run() {
                        mBinding?.viewPager?.currentItem?.let { (pageChangeListener as ViewPager.OnPageChangeListener).onPageSelected(it) }
                    }
                })
            }
            btnBack.setOnClickListener {
                onBackPressed()
            }
        }

        loading = MaterialDialog.Builder(this@ForgotPasswordActivity).title("Loading").content("Please wait").progress(true, 0).build()

        Fonty.setFonts(mBinding?.root as ViewGroup)
    }

    override fun onBackPressed() {
        if (mBinding?.viewPager?.currentItem == 0) {
            super.onBackPressed()
        } else if(mBinding?.viewPager?.currentItem == 2) {
            mBinding?.viewPager?.setCurrentItem(0,true)
        } else {
            mBinding?.viewPager?.setCurrentItem(mBinding?.viewPager!!.currentItem - 1, true)
        }
    }
}
