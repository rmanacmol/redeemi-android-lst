package com.imfreemobile.redeemi.android.redeemiandroid.view

import android.app.Activity
import android.content.Intent
import android.os.Bundle
import com.afollestad.materialdialogs.MaterialDialog
import com.google.zxing.ResultPoint
import com.imfreemobile.redeemi.android.redeemiandroid.R
import com.imfreemobile.redeemi.android.redeemiandroid.viewmodel.ScanViewModel
import com.journeyapps.barcodescanner.BarcodeCallback
import com.journeyapps.barcodescanner.BarcodeResult
import com.marcinorlowski.fonty.Fonty
import kotlinx.android.synthetic.main.activity_scan.*
import kotlinx.android.synthetic.main.layout_toolbar.*
import timber.log.Timber

class ScanActivity : BaseLocationActivity() {

    override fun isPinRequiredInActivity(): Boolean {
        return true
    }

    companion object {
        val TAG = ScanActivity::class.java.simpleName
    }

    private var mViewModel: ScanViewModel? = null
    private var loadingDialog: MaterialDialog? = null

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_scan)
        mViewModel = ScanViewModel(application, this@ScanActivity)

        initView()

        Fonty.setFonts(this)
    }

    private fun initView() {
        setSupportActionBar(toolbar)
        supportActionBar?.apply {
            setDisplayHomeAsUpEnabled(true)
            setDisplayShowHomeEnabled(true)
            setDisplayShowTitleEnabled(false)
        }
        toolbar.setNavigationOnClickListener { mViewModel?.onBackPressed() }
        preview_container.setAspectRatio(1.0)
        toolbar_title.setText(R.string.scan_title)
    }

    override fun onResumeComplete() {
        barcodeview.resume()
        initBarcodeScanning()
    }

    fun initBarcodeScanning() {
        barcodeview.decodeSingle(object : BarcodeCallback {
            override fun barcodeResult(result: BarcodeResult?) {
                Timber.d(TAG, "result: ${result.toString()}")
                if (result != null) {
                    mViewModel?.fetchVoucherInfo(result.toString())
                }
            }

            override fun possibleResultPoints(resultPoints: MutableList<ResultPoint>?) {

            }
        })
    }

    override fun onActivityResult(requestCode: Int, resultCode: Int, data: Intent?) {
        if (requestCode == ScanViewModel.REQUEST_CODE_QR_DETECTED) {
            if (data == null) {
                if (resultCode == Activity.RESULT_OK) {
                    showQRInvalid()
                }
            } else {
                showNoInternetConnection()
            }
        }
    }

    fun showQRInvalid() {
        mViewModel?.onQRCodeInvalid(parentPanel)
    }

    fun showNoInternetConnection() {
        mViewModel?.onNoInternetConnection(parentPanel)
    }

    override fun onPause() {
        super.onPause()
        barcodeview.pause()
        mViewModel?.cancelNetCall()
    }

    fun showLoadingDialog() {
        if (loadingDialog == null) {
            loadingDialog = MaterialDialog.Builder(this@ScanActivity).content("Loading").progress(true, 0).build()
        }
        loadingDialog?.show()
    }

    fun hideLoadingDialog() {
        loadingDialog?.dismiss()
    }
}
