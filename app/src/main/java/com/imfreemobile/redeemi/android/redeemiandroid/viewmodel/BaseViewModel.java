package com.imfreemobile.redeemi.android.redeemiandroid.viewmodel;

import android.app.Activity;
import android.app.Application;
import android.databinding.BaseObservable;

import com.google.android.gms.auth.api.phone.SmsRetrieverClient;
import com.google.gson.Gson;
import com.imfreemobile.redeemi.android.redeemiandroid.ApiService;
import com.imfreemobile.redeemi.android.redeemiandroid.RedeemiApplication;
import com.imfreemobile.redeemi.android.redeemiandroid.model.db.AppDatabase;

import javax.inject.Inject;

/**
 * Created by renzmanacmol on 23/10/2017.
 */

public class BaseViewModel extends BaseObservable {

    @Inject ApiService apiService;
    @Inject AppDatabase appDatabase;
    @Inject Gson gson;
    @Inject SmsRetrieverClient client;

    public BaseViewModel() {
    }

    public BaseViewModel(Application application, Activity activity) {
        ((RedeemiApplication) application).getApplicationComponent().inject(this);
    }

}
