package com.imfreemobile.redeemi.android.redeemiandroid.view

import android.annotation.TargetApi
import android.content.Intent
import android.os.Build
import android.os.Bundle
import android.util.Log
import com.imfreemobile.redeemi.android.redeemiandroid.BuildConfig
import com.imfreemobile.redeemi.android.redeemiandroid.utils.AppSignatureHelper
import com.marcinorlowski.fonty.Fonty
import org.jetbrains.anko.intentFor


class SplashActivity : BaseLocationActivity() {

    override fun isPinRequiredInActivity(): Boolean {
        return false
    }

    companion object {
        val TAG = SplashActivity::class.java.simpleName
    }

    override fun onResumeComplete() {
    }

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        if ((intent.flags and Intent.FLAG_ACTIVITY_BROUGHT_TO_FRONT) != 0) {
            finish()
            return
        }
        if (appDatabase.tokenDao.token != null && appDatabase.tokenDao.token.accessToken != null) {
            if (BuildConfig.PIN_REQUIRED) {
                startActivity(intentFor<PinRequireActivity>())
            } else {
                startActivity(intentFor<BottomNavActivity>())
            }
        } else {
            startActivity(intentFor<VerifyActivity>())
        }
        finish()

        Fonty.setFonts(this)
    }
}
