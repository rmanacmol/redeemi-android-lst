package com.imfreemobile.redeemi.android.redeemiandroid.viewmodel;

import android.app.Application;
import android.content.Intent;
import android.databinding.ObservableField;
import android.view.View;

import com.imfreemobile.redeemi.android.redeemiandroid.BR;
import com.imfreemobile.redeemi.android.redeemiandroid.model.entity.Transaction;
import com.imfreemobile.redeemi.android.redeemiandroid.view.BottomNavActivity;
import com.imfreemobile.redeemi.android.redeemiandroid.view.SuccessRedemptionActivity;

/**
 * Created by renzmanacmol on 24/10/2017.
 */

public class SuccessRedemptionViewModel extends BaseViewModel {

    private SuccessRedemptionActivity mActivity;
    public ObservableField<Transaction> transaction = new ObservableField<>();

    public Transaction getTransaction() {
        return transaction.get();
    }

    public void setTransaction(Transaction transaction) {
        this.transaction.set(transaction);
        notifyPropertyChanged(BR._all);
    }

    public SuccessRedemptionViewModel(Application application, SuccessRedemptionActivity activity, Transaction transaction) {
        super(application, activity);
        this.mActivity = activity;
        this.transaction.set(transaction);
    }

    public void onOkayClicked(View view) {
        Intent i = new Intent(mActivity, BottomNavActivity.class);
        i.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP | Intent.FLAG_ACTIVITY_SINGLE_TOP);
        mActivity.startActivity(i);
        mActivity.finish();
    }

}
