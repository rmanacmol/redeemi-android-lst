package com.imfreemobile.redeemi.android.redeemiandroid.view.fragment.transaction

import android.databinding.DataBindingUtil
import android.os.Bundle
import android.support.v7.widget.LinearLayoutManager
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import com.imfreemobile.redeemi.android.redeemiandroid.R
import com.imfreemobile.redeemi.android.redeemiandroid.databinding.FragmentTransactionActivitiesBinding
import com.imfreemobile.redeemi.android.redeemiandroid.view.adapter.TransactionHistoryAdapter
import com.imfreemobile.redeemi.android.redeemiandroid.view.customviews.EndlessRecyclerOnScrollListener
import com.imfreemobile.redeemi.android.redeemiandroid.view.fragment.BaseFragment
import com.imfreemobile.redeemi.android.redeemiandroid.viewmodel.TransactionActivitiesViewModel
import com.marcinorlowski.fonty.Fonty


/**
 * Created by rmanacmol on 06/11/2017.
 */

class TransactionActivitiesFragment : BaseFragment() {

    var mBinding: FragmentTransactionActivitiesBinding? = null
    var mViewModel: TransactionActivitiesViewModel? = null
    var adapter: TransactionHistoryAdapter? = null
    var scrollListener: EndlessRecyclerOnScrollListener? = null

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
    }

    override fun onCreateView(inflater: LayoutInflater?, container: ViewGroup?, savedInstanceState: Bundle?): View? {
        super.onCreateView(inflater, container, savedInstanceState)
        mBinding = DataBindingUtil.inflate(inflater, R.layout.fragment_transaction_activities, container, false)
        mViewModel = TransactionActivitiesViewModel(activity.application, this.activity, parentFragment as TransactionFragment, this@TransactionActivitiesFragment)
        mBinding?.viewModel = mViewModel
        Fonty.setFonts(mBinding?.root as ViewGroup)
        return mBinding?.root
    }

    override fun onPause() {
        super.onPause()
        mViewModel?.cancelNetcall()
    }

    override fun onViewCreated(view: View?, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)

        adapter = TransactionHistoryAdapter()

        mBinding?.recycler?.adapter = adapter
        mBinding?.recycler?.layoutManager = LinearLayoutManager(activity)
        scrollListener = object : EndlessRecyclerOnScrollListener() {
            override fun onLoadMore() {
                mViewModel?.page = mViewModel?.page?.inc()
                mViewModel?.page?.let { mViewModel?.fetchTransactions(it) }
            }
        }
        mBinding?.recycler?.addOnScrollListener(scrollListener)
        mBinding?.swipeRefreshLayout?.setOnRefreshListener {
            mViewModel?.page = 1
            mViewModel?.fetchTransactions(1)
        }
        mBinding?.swipeRefreshLayout?.setColorSchemeResources(R.color.textColorHint)
    }
}
