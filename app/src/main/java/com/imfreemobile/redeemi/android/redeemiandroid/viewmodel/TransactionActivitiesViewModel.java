package com.imfreemobile.redeemi.android.redeemiandroid.viewmodel;

import android.app.Activity;
import android.app.Application;
import android.databinding.ObservableBoolean;
import android.databinding.ObservableField;

import com.imfreemobile.redeemi.android.redeemiandroid.BR;
import com.imfreemobile.redeemi.android.redeemiandroid.model.entity.Transaction;
import com.imfreemobile.redeemi.android.redeemiandroid.model.networkmodel.EnvelopedTransactionHistory;
import com.imfreemobile.redeemi.android.redeemiandroid.utils.AuthUtils;
import com.imfreemobile.redeemi.android.redeemiandroid.utils.networkutils.NoConnectivityException;
import com.imfreemobile.redeemi.android.redeemiandroid.view.BottomNavActivity;
import com.imfreemobile.redeemi.android.redeemiandroid.view.fragment.transaction.TransactionActivitiesFragment;
import com.imfreemobile.redeemi.android.redeemiandroid.view.fragment.transaction.TransactionFragment;

import java.util.List;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

/**
 * Created by rmanacmol on 07/11/2017.
 */

public class TransactionActivitiesViewModel extends BaseViewModel {

    private static final String TAG = TransactionActivitiesViewModel.class.getSimpleName();

    private BottomNavActivity mActivity;
    private TransactionFragment transactionFragment;
    private TransactionActivitiesFragment transactionActivitiesFragment;
    private ObservableField<List<Transaction>> transactions = new ObservableField<>();
    private Call<EnvelopedTransactionHistory> networkCallTransactionHistory;
    private ObservableBoolean loading = new ObservableBoolean();
    public int page = 1;

    public void setLoading(boolean loading) {
        this.loading.set(loading);
        notifyPropertyChanged(BR._all);
        transactionActivitiesFragment.getMBinding().swipeRefreshLayout.setRefreshing(loading);
    }

    public boolean getLoading() {
        return this.loading.get();
    }

    public void setTransactions(List<Transaction> transactions) {
        this.transactions.set(transactions);
        transactionActivitiesFragment.getAdapter().setItems(transactions);
        transactionActivitiesFragment.getMBinding().recycler.getAdapter().notifyDataSetChanged();
        notifyPropertyChanged(BR._all);
    }

    public List<Transaction> getTransactions() {
        return this.transactions.get();
    }

    public TransactionActivitiesViewModel(Application application, Activity activity, TransactionFragment fragment, TransactionActivitiesFragment transactionActivitiesFragment) {
        super(application, activity);
        this.mActivity = (BottomNavActivity) activity;
        this.transactionFragment = fragment;
        this.transactionActivitiesFragment = transactionActivitiesFragment;
        page = 1;

        fetchTransactions(page);
    }

    public void cancelNetcall() {
        if (networkCallTransactionHistory != null) {
            networkCallTransactionHistory.cancel();
            networkCallTransactionHistory = null;
        }
    }

    public void fetchTransactions(final int page) {
        setLoading(true);
        networkCallTransactionHistory = apiService.getTransactionsHistory(AuthUtils.INSTANCE.getAccessHeaderFormattedToken(appDatabase.getTokenDao().getToken().getAccessToken()), page);
        networkCallTransactionHistory.enqueue(new Callback<EnvelopedTransactionHistory>() {
            @Override
            public void onResponse(Call<EnvelopedTransactionHistory> call, Response<EnvelopedTransactionHistory> response) {
                setLoading(false);
                if (response.isSuccessful()) {
                    if (page == 1) {
                        setTransactions(response.body().getData());
                        transactionActivitiesFragment.getScrollListener().setmLoading(false);
                    } else {
                        List<Transaction> transactions = getTransactions();
                        transactions.addAll(response.body().getData());
                        setTransactions(transactions);
                    }
                } else {
                    // handle error
                }
            }

            @Override
            public void onFailure(Call<EnvelopedTransactionHistory> call, Throwable t) {
                if (t instanceof NoConnectivityException) {
                    // show no internet
                } else {
                    // handle error
                }
            }
        });
    }
}
