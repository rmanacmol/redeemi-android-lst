package com.imfreemobile.redeemi.android.redeemiandroid.model.entity

import com.google.gson.annotations.Expose
import com.google.gson.annotations.SerializedName
import com.imfreemobile.redeemi.android.redeemiandroid.utils.DateUtils

/**
 * Created by jvillarey on 07/11/2017.
 */

class IncomeBreakdown(@field:Expose
                      @field:SerializedName(TOTAL_SUM)
                      val totalSum: String = "0.00",
                      @field:Expose
                      @field:SerializedName(TOTAL_REDEMPTIONS)
                      val totalRedemptions: String = "0.00",
                      @field:Expose
                      @field:SerializedName(TOTAL_SERVICE_FEE)
                      val totalServiceFee: String = "0.00",
                      @field:Expose
                      @field:SerializedName(MIN_WITHDRAW_AMT)
                      val minWithdrawAmt: String = "000.00",
                      @field:Expose
                      @field:SerializedName(DATE_FROM)
                      val dateFrom: String = DateUtils.getMMM_dd_yyyyFormattedStringFromDateString(DateUtils.getyyyyMMddFormattedFirstDayOfMonthDateString()),
                      @field:Expose
                      @field:SerializedName(DATE_TO)
                      val dateTo: String = DateUtils.getMMM_dd_yyyyFormattedStringFromDateString(DateUtils.getyyyyMMddformattedCurrentDateString())) {

    companion object {

        const val TOTAL_SUM = "total_sum"
        const val TOTAL_REDEMPTIONS = "total_redemptions"
        const val TOTAL_SERVICE_FEE = "total_service_fee"
        const val MIN_WITHDRAW_AMT = "min_withdraw_amt"
        const val DATE_FROM = "date_from"
        const val DATE_TO = "date_to"
    }
}
