package com.imfreemobile.redeemi.android.redeemiandroid.viewmodel;

import android.app.Application;
import android.content.Intent;
import android.view.View;

import com.imfreemobile.redeemi.android.redeemiandroid.view.BottomNavActivity;
import com.imfreemobile.redeemi.android.redeemiandroid.view.SuccessRegistrationActivity;

/**
 * Created by rmanacmol on 13/11/2017.
 */

public class SuccessRegistrationViewModel extends BaseViewModel {

    private SuccessRegistrationActivity mActivity;

    public SuccessRegistrationViewModel(Application application, SuccessRegistrationActivity activity) {
        super(application, activity);
        this.mActivity = activity;
    }

    public void onOkayButtonClicked(View view) {
        Intent i = new Intent(mActivity, BottomNavActivity.class);
        mActivity.startActivity(i);
        mActivity.finish();
    }

}
