package com.imfreemobile.redeemi.android.redeemiandroid.model.entity

import com.google.gson.annotations.Expose
import com.google.gson.annotations.SerializedName

/**
 * Created by rmanacmol on 14/11/2017.
 */

class LoginRequest(@field:Expose
                   @field:SerializedName(PASSWORD)
                   private val password: String, @field:Expose
                   @field:SerializedName(MOBILE_NUMBER)
                   private val mobileNumber: String, @field:Expose
                   @field:SerializedName(LONGITUDE)
                   private val longitude: Double, @field:Expose
                   @field:SerializedName(LATITUDE)
                   private val latitude: Double, @field:Expose
                   @field:SerializedName(DEVICE_ID)
                   private val deviceId: String) {

    companion object {
        const private val PASSWORD = "password"
        const private val MOBILE_NUMBER = "mobile_number"
        const private val LONGITUDE = "longitude"
        const private val LATITUDE = "latitude"
        const private val DEVICE_ID = "device_id"
    }

}
