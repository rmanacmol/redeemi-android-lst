package com.imfreemobile.redeemi.android.redeemiandroid.viewmodel;

import android.app.Application;
import android.databinding.BindingAdapter;
import android.support.annotation.NonNull;
import android.support.design.widget.BottomNavigationView;
import android.support.design.widget.Snackbar;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentTransaction;
import android.view.MenuItem;
import android.view.View;

import com.imfreemobile.redeemi.android.redeemiandroid.R;
import com.imfreemobile.redeemi.android.redeemiandroid.view.BottomNavActivity;
import com.imfreemobile.redeemi.android.redeemiandroid.view.fragment.HomeFragment;
import com.imfreemobile.redeemi.android.redeemiandroid.view.fragment.transaction.TransactionFragment;
import com.imfreemobile.redeemi.android.redeemiandroid.view.fragment.withdraw.WithdrawIncomeFragment;

/**
 * Created by rmanacmol on 06/11/2017.
 */

public class BottomNavViewModel extends BaseViewModel {

    private BottomNavActivity mActivity;
    private int selectedItemId = -1;

    public BottomNavViewModel(Application application, BottomNavActivity activity) {
        super(application, activity);
        this.mActivity = activity;
    }

    public void initFragment() {
        replaceFragment(new HomeFragment());
    }

    public void frontLinerView() {
        mActivity.mBinding.navigation.getMenu().removeItem(R.id.nav_withdraw_income);
    }

    public boolean onNavigationItemSelected(@NonNull MenuItem item) {
        if (selectedItemId != item.getItemId()) {
            switch (item.getItemId()) {
                case R.id.nav_home:
                    replaceFragment(new HomeFragment());
                    break;
                case R.id.nav_withdraw_income:
                    replaceFragment(new WithdrawIncomeFragment());
                    break;
                case R.id.nav_transaction:
                    replaceFragment(new TransactionFragment());
                    break;
            }
            selectedItemId = item.getItemId();
        }
        return true;
    }

    public static class BottomNavigationViewBindingAdapter {
        @BindingAdapter("onNavigationItemSelected")
        public static void setOnNavigationItemSelectedListener(
                BottomNavigationView view,
                BottomNavigationView.OnNavigationItemSelectedListener listener) {
            view.setOnNavigationItemSelectedListener(listener);
        }
    }

    public void replaceFragment(Fragment fragment) {
        if (!mActivity.isFinishing() && fragment != null) {
            FragmentTransaction fragmentTransaction = mActivity.getSupportFragmentManager().beginTransaction();
            fragmentTransaction.replace(R.id.fragment_container, fragment);
            fragmentTransaction.commitAllowingStateLoss();
        }
    }

    public void onError(View view, String errorString) {
        Snackbar.make(view, errorString, Snackbar.LENGTH_SHORT).show();
    }

}
