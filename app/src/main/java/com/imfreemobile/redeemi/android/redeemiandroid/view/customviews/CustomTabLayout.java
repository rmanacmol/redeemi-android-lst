package com.imfreemobile.redeemi.android.redeemiandroid.view.customviews;

import android.content.Context;
import android.support.design.widget.TabLayout;
import android.util.AttributeSet;
import android.view.ViewGroup;

import com.marcinorlowski.fonty.Fonty;

/**
 * Created by jvillarey on 24/11/2017.
 */

public class CustomTabLayout extends TabLayout {
    public CustomTabLayout(Context context) {
        super(context);
    }

    public CustomTabLayout(Context context, AttributeSet attrs) {
        super(context, attrs);
    }

    @Override
    protected void onLayout(boolean changed, int l, int t, int r, int b) {
        super.onLayout(changed, l, t, r, b);
        Fonty.setFonts((ViewGroup) getChildAt(0));
    }
}
