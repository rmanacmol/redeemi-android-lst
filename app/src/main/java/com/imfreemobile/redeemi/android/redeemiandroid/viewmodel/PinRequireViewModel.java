package com.imfreemobile.redeemi.android.redeemiandroid.viewmodel;

import android.app.Application;
import android.content.Context;
import android.content.Intent;
import android.databinding.ObservableBoolean;
import android.databinding.ObservableField;
import android.support.v4.app.ActivityCompat;
import android.view.View;
import android.view.inputmethod.InputMethodManager;

import com.imfreemobile.redeemi.android.redeemiandroid.R;
import com.imfreemobile.redeemi.android.redeemiandroid.view.BottomNavActivity;
import com.imfreemobile.redeemi.android.redeemiandroid.view.LoginActivity;
import com.imfreemobile.redeemi.android.redeemiandroid.view.PinRequireActivity;

/**
 * Created by rmanacmol on 15/11/2017.
 */

public class PinRequireViewModel extends BaseViewModel {

    public ObservableBoolean pinVisible = new ObservableBoolean();
    public ObservableBoolean maxAttemptVisible = new ObservableBoolean();
    public ObservableField<String> welcome = new ObservableField<>();
    public ObservableField<String> tvUserAttempt = new ObservableField<>("");
    private PinRequireActivity mActivity;
    private int numAttempt;
    private int maxAttempt;

    public PinRequireViewModel(Application application, PinRequireActivity activity) {
        super(application, activity);
        this.mActivity = activity;
        this.numAttempt = 0;
        this.maxAttempt = 4;
        this.pinVisible.set(true);
        this.maxAttemptVisible.set(false);
        if (mActivity.getIntent().getBooleanExtra("isLoginPin", false)) {
            this.welcome.set(mActivity.getString(R.string.welcome));
        } else {
            this.welcome.set(mActivity.getString(R.string.welcome_back));
        }
    }

    public void onPinEntered(CharSequence pin, View view) {
        if (appDatabase.getUserDao().getUser() != null) {
            if (pin.toString().equals(appDatabase.getUserDao().getUser().getPin())) {
                mActivity.startActivity(new Intent(mActivity, BottomNavActivity.class));
                ActivityCompat.finishAffinity(mActivity);
            } else {
                if (numAttempt < 3) {
                    InputMethodManager imm = (InputMethodManager)
                            mActivity.getSystemService(Context.INPUT_METHOD_SERVICE);
                    if (imm != null) {
                        try {
                            imm.hideSoftInputFromWindow(view.getWindowToken(), 0);
                        } catch (Exception e) {
                            e.printStackTrace();
                        }
                    }
                    mActivity.showError("PIN is invalid", false);
                    numAttempt++;

                    if (numAttempt != 0) {
                        tvUserAttempt.set(4 - numAttempt + " attempt/s remaining");
                    }
                } else {
                    InputMethodManager imm = (InputMethodManager)
                            mActivity.getSystemService(Context.INPUT_METHOD_SERVICE);
                    if (imm != null) {
                        try {
                            imm.hideSoftInputFromWindow(view.getWindowToken(), 0);
                        } catch (Exception e) {
                            e.printStackTrace();
                        }
                    }
                    maxAttemptVisible.set(true);
                    pinVisible.set(false);
                }
            }
        }
    }

    public void onClickedBackToLogin(View view) {
        appDatabase.getTokenDao().deleteAll();
        appDatabase.getUserDao().deleteAll();
        mActivity.startActivity(new Intent(mActivity, LoginActivity.class));
        ActivityCompat.finishAffinity(mActivity);
    }

}
