package com.imfreemobile.redeemi.android.redeemiandroid.view;

import android.databinding.DataBindingUtil;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.view.ViewGroup;

import com.imfreemobile.redeemi.android.redeemiandroid.R;
import com.imfreemobile.redeemi.android.redeemiandroid.databinding.ActivityLoginBinding;
import com.imfreemobile.redeemi.android.redeemiandroid.viewmodel.LoginViewModel;
import com.marcinorlowski.fonty.Fonty;

/**
 * Created by rmanacmol on 14/11/2017.
 */

public class LoginActivity extends BaseLocationActivity {

    public ActivityLoginBinding mBinding;
    public LoginViewModel mViewModel;

    @Override
    protected boolean isPinRequiredInActivity() {
        return false;
    }

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        mBinding = DataBindingUtil.setContentView(this, R.layout.activity_login);
        mViewModel = new LoginViewModel(getApplication(), this, mBinding);
        mBinding.setViewModel(mViewModel);

        if (getIntent().hasExtra("mobile-num")) {
            mViewModel.phone.set(getIntent().getStringExtra("mobile-num"));
            mBinding.password.requestFocus();
        }

        Fonty.setFonts((ViewGroup) mBinding.getRoot());
    }

    @Override
    protected void onResumeComplete() {

    }
}
