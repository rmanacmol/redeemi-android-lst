package com.imfreemobile.redeemi.android.redeemiandroid.model.entity

import com.google.gson.annotations.Expose
import com.google.gson.annotations.SerializedName

/**
 * Created by jvillarey on 20/11/2017.
 */

class WithdrawTransaction(@Expose
                          @SerializedName(STATUS)
                          val status: String,
                          @Expose
                          @SerializedName(CURRENCY)
                          val currency: String,
                          @Expose
                          @SerializedName(AMOUNT)
                          val amount: String,
                          @Expose
                          @SerializedName(DATE)
                          val date: String,
                          @Expose
                          @SerializedName(TRANSACTION_CODE)
                          val transactionCode: String) {

    companion object {
        const private val CURRENCY = "currency"
        const private val AMOUNT = "amount"
        const private val DATE = "status_date"
        const private val TRANSACTION_CODE = "transaction_code"
        const private val STATUS = "status"
    }
}
