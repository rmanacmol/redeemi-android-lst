package com.imfreemobile.redeemi.android.redeemiandroid.viewmodel;

import android.app.Application;
import android.content.Context;
import android.content.Intent;
import android.databinding.ObservableBoolean;
import android.databinding.ObservableField;
import android.graphics.Color;
import android.os.CountDownTimer;
import android.support.annotation.NonNull;
import android.support.design.widget.Snackbar;
import android.text.TextUtils;
import android.util.Log;
import android.view.View;
import android.view.inputmethod.InputMethodManager;

import com.imfreemobile.redeemi.android.redeemiandroid.BR;
import com.imfreemobile.redeemi.android.redeemiandroid.BuildConfig;
import com.imfreemobile.redeemi.android.redeemiandroid.R;
import com.imfreemobile.redeemi.android.redeemiandroid.model.entity.VerificationRequest;
import com.imfreemobile.redeemi.android.redeemiandroid.model.networkmodel.EnvelopedVerification;
import com.imfreemobile.redeemi.android.redeemiandroid.model.networkmodel.Error;
import com.imfreemobile.redeemi.android.redeemiandroid.utils.networkutils.NoConnectivityException;
import com.imfreemobile.redeemi.android.redeemiandroid.view.LoginActivity;
import com.imfreemobile.redeemi.android.redeemiandroid.view.RegistrationFlowActivity;
import com.imfreemobile.redeemi.android.redeemiandroid.view.VerifyActivity;

import java.io.IOException;
import java.util.Locale;
import java.util.concurrent.TimeUnit;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

/**
 * Created by jvillarey on 13/11/2017.
 */

public class VerifyViewModel extends BaseViewModel {

    private static final String TAG = VerifyViewModel.class.getSimpleName();

    private VerifyActivity mActivity;
    private ObservableBoolean loading = new ObservableBoolean();
    private ObservableBoolean nextEnabled = new ObservableBoolean();
    private ObservableBoolean errorDisplaying = new ObservableBoolean();
    private ObservableBoolean shouldShowLoginButton = new ObservableBoolean();
    private ObservableBoolean shouldShowCountdown = new ObservableBoolean();
    private ObservableField<String> countdownValue = new ObservableField<>();
    private Call<EnvelopedVerification> networkCallVerify;
    private CountDownTimer timer;

    private Snackbar snackbar;

    public void setShouldShowCountdown(boolean shouldShowCountdown) {
        this.shouldShowCountdown.set(shouldShowCountdown);
        notifyPropertyChanged(BR._all);
    }

    public boolean getShouldShowCountdown() {
        return this.shouldShowCountdown.get();
    }

    public void setCountdownValue(String countdownValue) {
        this.countdownValue.set(countdownValue);
        notifyPropertyChanged(BR._all);
    }

    public String getCountdownValue() {
        return this.countdownValue.get();
    }

    public void setShouldShowLoginButton(boolean shouldShowLoginButton) {
        this.shouldShowLoginButton.set(shouldShowLoginButton);
        notifyPropertyChanged(BR._all);
    }

    public boolean getShouldShowLoginButton() {
        return this.shouldShowLoginButton.get();
    }

    public void setLoading(boolean loading) {
        this.loading.set(loading);
        notifyPropertyChanged(BR._all);
    }

    public boolean getErrorDisplaying() {
        return this.errorDisplaying.get();
    }

    private void setErrorDisplaying(boolean errorDisplaying) {
        this.errorDisplaying.set(errorDisplaying);
        notifyPropertyChanged(BR._all);
    }

    public boolean getLoading() {
        return this.loading.get();
    }

    public void setNextEnabled(boolean enabled) {
        if (enabled) {
            hideKeyboard();
            hideError();
        }
        this.nextEnabled.set(enabled);
        notifyPropertyChanged(BR._all);
    }

    public boolean getNextEnabled() {
        return nextEnabled.get();
    }

    public VerifyViewModel(Application application, VerifyActivity verifyActivity) {
        super(application, verifyActivity);
        this.mActivity = verifyActivity;
    }

    private void fetchVerificationStatus(final String mobileNumber) {
        setLoading(true);
        networkCallVerify = apiService.verify(new VerificationRequest(mobileNumber));
        networkCallVerify.enqueue(new Callback<EnvelopedVerification>() {
            @Override
            public void onResponse(@NonNull Call<EnvelopedVerification> call, @NonNull Response<EnvelopedVerification> response) {
                setLoading(false);
                if (response.isSuccessful()) {
                    onVerificationSuccess(response.body().getData().getVerificationUuid(), mobileNumber);
                } else {
                    try {
                        handleError(response.errorBody().string());
                    } catch (IOException | NullPointerException e) {
                        e.printStackTrace();
                        showGenericError();
                    }
                }
            }

            @Override
            public void onFailure(@NonNull Call<EnvelopedVerification> call, @NonNull Throwable t) {
                setLoading(false);
                if (t instanceof NoConnectivityException) {
                    showNoInternetError();
                } else {
                    handleError(t.getMessage());
                }
            }
        });
    }

    private void onVerificationSuccess(String verificationUuid, String phoneNumber) {
        Intent i = new Intent(mActivity, RegistrationFlowActivity.class);
        i.putExtra("verification_uuid", verificationUuid);
        i.putExtra("phone_number", phoneNumber);
        mActivity.startActivity(i);
    }

    private void handleError(String string) {
        try {
            Error error = gson.fromJson(string, EnvelopedVerification.class).getError();
            switch (error.getErrorCode()) {
                case 1022:
                    showRetryAt(error.getRetryAt());
                    break;
                case 1007:
                    showAccountDoesNotExistError();
                    break;
                case 1008:
                    showAccountAlreadyActivatedError();
                    break;
                case 1009:
                    showAccountAlreadyRegisteredError();
                    break;
                default:
                    showGenericError();
                    break;
            }
        } catch (Exception e) {
            showGenericError();
        }
    }

    private void showGenericError() {
        mActivity.showSnackbar(mActivity.getString(R.string.error_generic));
    }

    private void showAccountAlreadyActivatedError() {
        mActivity.showSnackbar(mActivity.getString(R.string.error_account_already_activated));
    }

    private void showAccountAlreadyRegisteredError() {
        mActivity.showSnackbar(mActivity.getString(R.string.error_account_already_registered));
        setShouldShowLoginButton(true);
    }

    private void showRetryAt(Long retryAt) {
        if (retryAt != null) {
            mActivity.showSnackbar(mActivity.getString(R.string.error_limit_reached));
            setShouldShowCountdown(true);
            startTimer(retryAt);
        }
    }

    private void startTimer(Long retryAt) {
        if (timer != null) {
            timer.cancel();
        }
        retryAt = (long) (retryAt * BuildConfig.BACKEND_TIMER_MULTIPLIER);
        Log.d(TAG, "retryat: " + retryAt);
        timer = new CountDownTimer(retryAt, 1000) {
            @Override
            public void onTick(long millis) {
                setCountdownValue(String.format(Locale.ENGLISH, "%02d:%02d",
                        TimeUnit.MILLISECONDS.toMinutes(millis) -
                                TimeUnit.HOURS.toMinutes(TimeUnit.MILLISECONDS.toHours(millis)),
                        TimeUnit.MILLISECONDS.toSeconds(millis) -
                                TimeUnit.MINUTES.toSeconds(TimeUnit.MILLISECONDS.toMinutes(millis))));
            }

            @Override
            public void onFinish() {
                setShouldShowCountdown(false);
            }
        };
        timer.start();
    }

    private void showAccountDoesNotExistError() {
        mActivity.showSnackbar(mActivity.getString(R.string.error_account_does_not_exist));
    }

    private void showNoInternetError() {
        mActivity.showSnackbar(mActivity.getString(R.string.error_connection));
    }

    public void showSnackbar(View view, String text) {
        if (snackbar == null) {
            snackbar = Snackbar.make(view, text, Snackbar.LENGTH_INDEFINITE);
        } else {
            snackbar.setText(text);
        }
        snackbar.setAction(R.string.okay, new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                mActivity.getMBinding().etPhone.setText("");
                snackbar.dismiss();
            }
        });
        snackbar.setActionTextColor(Color.WHITE);
        snackbar.show();
        setErrorDisplaying(true);
    }

    public void hideError() {
        setErrorDisplaying(false);
        if (snackbar != null) {
            snackbar.dismiss();
        }
    }

    public void onNextButtonClicked(View view) {
        hideError();
        hideKeyboard();
        fetchVerificationStatus(mActivity.getMobileNumber().trim().replace(" ", ""));
    }

    private void hideKeyboard() {
        InputMethodManager imm = (InputMethodManager) mActivity.getSystemService(Context.INPUT_METHOD_SERVICE);
        imm.hideSoftInputFromWindow(mActivity.getMBinding().getRoot().getWindowToken(), 0);
    }

    public void onLoginInsteadClicked(View view) {
        Intent i = new Intent(mActivity, LoginActivity.class);
        if (!TextUtils.isEmpty(mActivity.getMobileNumber().trim())) {
            i.putExtra("mobile-num", mActivity.getMobileNumber().trim());
        }
        mActivity.startActivity(i);
    }
}
