package com.imfreemobile.redeemi.android.redeemiandroid.model.entity

import com.google.gson.annotations.Expose
import com.google.gson.annotations.SerializedName

/**
 * Created by jvillarey on 13/11/2017.
 */

class VerificationOtp(@Expose
                      @SerializedName(ACCESS_TOKEN)
                      val accessToken: String) {

    companion object {
        const val ACCESS_TOKEN = "access_token"
    }
}
