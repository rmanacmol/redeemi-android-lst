package com.imfreemobile.redeemi.android.redeemiandroid;

import android.support.multidex.MultiDexApplication;

import com.crashlytics.android.Crashlytics;
import com.marcinorlowski.fonty.Fonty;

import io.fabric.sdk.android.Fabric;
import timber.log.Timber;

/**
 * Created by renzmanacmol on 23/10/2017.
 */

public class RedeemiApplication extends MultiDexApplication {

    protected ApplicationComponent mApplicationComponent;

    public ApplicationComponent getApplicationComponent() {
        return mApplicationComponent;
    }

    @Override
    public void onCreate() {
        super.onCreate();
        if (BuildConfig.DEBUG) {
            Timber.plant(new Timber.DebugTree());
        } else {
            Fabric.with(this, new Crashlytics());
        }
        Fonty.context(this)
                .regularTypeface("Bariol-Regular.ttf")
                .boldTypeface("Bariol-Bold.ttf")
                .done();
        initAppComponent();
    }


    private void initAppComponent() {
        mApplicationComponent = DaggerApplicationComponent.builder()
                .applicationModule(new ApplicationModule(this))
                .build();
    }

}
