package com.imfreemobile.redeemi.android.redeemiandroid.model.entity

import com.google.gson.annotations.Expose
import com.google.gson.annotations.SerializedName

/**
 * Created by jvillarey on 15/11/2017.
 */

class Password(@field:Expose
               @field:SerializedName(PASSWORD)
               private val password: String) {

    companion object {
        const val PASSWORD = "password"
    }
}
