package com.imfreemobile.redeemi.android.redeemiandroid.model.dao

import android.arch.persistence.room.Dao
import android.arch.persistence.room.Delete
import android.arch.persistence.room.Insert
import android.arch.persistence.room.Query
import android.arch.persistence.room.Update

import com.imfreemobile.redeemi.android.redeemiandroid.model.tables.Token

import android.arch.persistence.room.OnConflictStrategy.REPLACE

/**
 * Created by jvillarey on 15/11/2017.
 */

@Dao
interface TokenDao {

    @get:Query("SELECT * FROM token")
    val allToken: List<Token>

    @get:Query("SELECT * FROM token LIMIT 1")
    val token: Token

    @Insert(onConflict = REPLACE)
    fun insert(token: Token)

    @Delete
    fun delete(token: Token)

    @Query("DELETE FROM token")
    fun deleteAll()

    @Update(onConflict = REPLACE)
    fun updateToken(token: Token)
}
