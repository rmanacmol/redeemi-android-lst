package com.imfreemobile.redeemi.android.redeemiandroid.model.networkmodel

import com.google.gson.annotations.Expose
import com.google.gson.annotations.SerializedName
import com.imfreemobile.redeemi.android.redeemiandroid.model.entity.WithdrawTransaction

/**
 * Created by jvillarey on 22/11/2017.
 */

class EnvelopedWithdrawTransaction {

    @Expose
    @SerializedName(DATA)
    val data: WithdrawTransaction? = null

    @Expose
    @SerializedName(ERROR)
    val error: Error? = null

    companion object {
        const val DATA = "data"
        const val ERROR = "error"
    }
}
