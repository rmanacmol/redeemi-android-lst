package com.imfreemobile.redeemi.android.redeemiandroid.model.entity

import com.google.gson.annotations.Expose
import com.google.gson.annotations.SerializedName

import java.io.Serializable

/**
 * Created by Joseph on 26/10/2017.
 */

class Transaction(@Expose
                  @SerializedName(AMOUNT)
                  val amount: String,
                  @Expose
                  @SerializedName(INCENTIVE_AMOUNT)
                  val incentiveAmount: String,
                  @Expose
                  @SerializedName(DATETIME_REDEMPTION)
                  val datetimeRedemption: String,
                  @Expose
                  @SerializedName(AMOUNT_CURRENCY)
                  val amountCurrency: String,
                  @Expose
                  @SerializedName(TRANSACTION_UUID)
                  val transactionId: String,
                  @Expose
                  @SerializedName(BRAND_NAME)
                  val brandName: String,
                  @Expose
                  @SerializedName(TOTAL)
                  val total: String) : Serializable {

    companion object {

        const val AMOUNT = "amount"
        const val INCENTIVE_AMOUNT = "incentive_amount"
        const val DATETIME_REDEMPTION = "datetime_redemption"
        const val AMOUNT_CURRENCY = "amount_currency"
        const val TRANSACTION_UUID = "transaction_uuid"
        const val BRAND_NAME = "brand_name"
        const val TOTAL = "total"

        private const val serialVersionUID = 6279735524000988632L
    }
}
