package com.imfreemobile.redeemi.android.redeemiandroid.model.entity

import com.google.gson.annotations.Expose
import com.google.gson.annotations.SerializedName

import java.io.Serializable

/**
 * Created by Joseph on 26/10/2017.
 */

data class Voucher(
        @Expose @SerializedName(CODE) val code: String,
        @Expose @SerializedName(BRAND_NAME) val brandName: String,
        @Expose @SerializedName(AMOUNT_CURRENCY) val amountCurrency: String,
        @Expose @SerializedName(AMOUNT) val amount: String,
        @Expose @SerializedName(EXPIRY_DATE) val expiryDate: String,
        @Expose @SerializedName(IMAGE_URL) val imageUrl: String
) : Serializable {

    constructor() : this("", "", "", "", "", "")

    companion object {
        const val CODE = "code"
        const val BRAND_NAME = "brand_name"
        const val AMOUNT_CURRENCY = "amount_currency"
        const val AMOUNT = "amount"
        const val EXPIRY_DATE = "expiry_date"
        const val IMAGE_URL = "image_url"
        private const val serialVersionUID = 739001569299677892L
    }
}
