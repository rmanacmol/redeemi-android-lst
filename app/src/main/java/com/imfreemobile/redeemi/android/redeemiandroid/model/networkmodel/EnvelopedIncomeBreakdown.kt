package com.imfreemobile.redeemi.android.redeemiandroid.model.networkmodel

import com.google.gson.annotations.Expose
import com.google.gson.annotations.SerializedName
import com.imfreemobile.redeemi.android.redeemiandroid.model.entity.IncomeBreakdown

/**
 * Created by jvillarey on 07/11/2017.
 */

class EnvelopedIncomeBreakdown {

    @Expose
    @SerializedName(DATA)
    val data: IncomeBreakdown? = null

    @Expose
    @SerializedName(ERROR)
    val error: Error? = null

    companion object {
        const val DATA = "data"
        const val ERROR = "error"
    }

}
