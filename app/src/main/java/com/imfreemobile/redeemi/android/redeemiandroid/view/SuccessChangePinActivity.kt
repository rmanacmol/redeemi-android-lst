package com.imfreemobile.redeemi.android.redeemiandroid.view

import android.app.Activity
import android.os.Bundle
import android.view.View
import android.view.ViewGroup
import com.imfreemobile.redeemi.android.redeemiandroid.R
import com.marcinorlowski.fonty.Fonty

class SuccessChangePinActivity : BaseLocationActivity() {
    override fun onResumeComplete() {
    }

    override fun isPinRequiredInActivity(): Boolean {
        return false
    }

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_success_change_pin)

        Fonty.setFonts(this)
    }

    fun onDoneClicked(view: View) {
        setResult(Activity.RESULT_OK);
        finish()
    }
}
