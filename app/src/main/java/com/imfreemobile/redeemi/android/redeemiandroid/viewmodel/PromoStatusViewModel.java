package com.imfreemobile.redeemi.android.redeemiandroid.viewmodel;

import android.app.Application;
import android.content.Intent;
import android.databinding.ObservableField;
import android.util.Log;
import android.view.View;

import com.imfreemobile.redeemi.android.redeemiandroid.R;
import com.imfreemobile.redeemi.android.redeemiandroid.view.BottomNavActivity;
import com.imfreemobile.redeemi.android.redeemiandroid.view.PromoStatusActivity;
import com.imfreemobile.redeemi.android.redeemiandroid.view.ScanActivity;

/**
 * Created by renzmanacmol on 24/10/2017.
 */

public class PromoStatusViewModel extends BaseViewModel {

    private static final String TAG = PromoStatusViewModel.class.getSimpleName();

    private PromoStatusActivity mActivity;
    public ObservableField<String> mPromoStatus = new ObservableField<>();

    public PromoStatusViewModel() {

    }

    public PromoStatusViewModel(Application application, PromoStatusActivity activity, int status) {
        super(application, activity);
        this.mActivity = activity;
        switch (status) {
            case 1001:
                updateUI(mActivity.getString(R.string.promo_expired));
                break;
            case 1002:
                updateUI(mActivity.getString(R.string.promo_redeemed));
                break;
        }
    }

    public void updateUI(String status) {
        mPromoStatus.set(status);
    }

    public void onTryAgainClicked(View view) {
        Intent i = new Intent(mActivity, ScanActivity.class);
        mActivity.startActivity(i);
        mActivity.finish();
    }

    public void onCancelClicked(View view) {
        Intent i = new Intent(mActivity, BottomNavActivity.class);
        i.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP|Intent.FLAG_ACTIVITY_SINGLE_TOP);
        mActivity.startActivity(i);
        mActivity.finish();
    }

}
