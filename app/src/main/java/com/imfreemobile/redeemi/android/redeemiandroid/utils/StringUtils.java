package com.imfreemobile.redeemi.android.redeemiandroid.utils;

import android.text.TextUtils;

/**
 * Created by jvillarey on 22/11/2017.
 */

public class StringUtils {

    public static String convertUnderscoresToSpaces(String text) {
        if (!TextUtils.isEmpty(text)) {
            return text.replace("_", " ").trim();
        }
        return "";
    }

}
