package com.imfreemobile.redeemi.android.redeemiandroid.model.tables

import android.arch.persistence.room.ColumnInfo
import android.arch.persistence.room.Entity
import android.arch.persistence.room.Ignore
import android.arch.persistence.room.PrimaryKey

/**
 * Created by jvillarey on 15/11/2017.
 */

@Entity(tableName = "token")
data class Token(@PrimaryKey(autoGenerate = true)
                 @ColumnInfo(name = "id")
                 var id: Int? = null,
                 @ColumnInfo(name = ACCESS_TOKEN)
                 var accessToken: String? = null) {

    @Ignore
    constructor(accessToken: String?) : this() {
        this.accessToken = accessToken
    }

    companion object {
        const val ACCESS_TOKEN = "access_token"
    }
}
