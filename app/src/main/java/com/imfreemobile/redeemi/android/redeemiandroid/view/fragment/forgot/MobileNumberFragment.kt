package com.imfreemobile.redeemi.android.redeemiandroid.view.fragment.forgot

import android.content.Context
import android.databinding.DataBindingUtil
import android.os.Bundle
import android.support.v4.app.Fragment
import android.util.Log
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import com.imfreemobile.redeemi.android.redeemiandroid.R
import com.imfreemobile.redeemi.android.redeemiandroid.databinding.FragmentMobileNumberBinding
import com.imfreemobile.redeemi.android.redeemiandroid.utils.RegistrationValidationutils
import com.imfreemobile.redeemi.android.redeemiandroid.view.ForgotPasswordActivity
import com.imfreemobile.redeemi.android.redeemiandroid.view.fragment.BaseFragment
import com.imfreemobile.redeemi.android.redeemiandroid.viewmodel.MobileNumberFragmentViewModel
import com.marcinorlowski.fonty.Fonty

/**
 * A simple [Fragment] subclass.
 * Activities that contain this fragment must implement the
 * [MobileNumberFragment.OnMobileNumberEnteredListener] interface
 * to handle interaction events.
 * Use the [MobileNumberFragment.newInstance] factory method to
 * create an instance of this fragment.
 */
class MobileNumberFragment : BaseFragment() {

    var mListener: OnMobileNumberEnteredListener? = null
    var mBinding: FragmentMobileNumberBinding? = null
    var mViewModel: MobileNumberFragmentViewModel? = null

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        mViewModel = MobileNumberFragmentViewModel(activity.application, activity as ForgotPasswordActivity?, this@MobileNumberFragment)
    }

    override fun onCreateView(inflater: LayoutInflater?, container: ViewGroup?,
                              savedInstanceState: Bundle?): View? {
        super.onCreateView(inflater, container, savedInstanceState)
        mBinding = DataBindingUtil.inflate(inflater, R.layout.fragment_mobile_number, container, false)
        mBinding?.viewModel = mViewModel
        Fonty.setFonts(mBinding?.root as ViewGroup)
        return mBinding?.root
    }

    override fun onViewCreated(view: View?, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        mBinding?.etPhone?.addTextChangedListener(RegistrationValidationutils.MobileNumberTextWatcher(object : RegistrationValidationutils.DigitsTextWatcherCallback {
            override fun onInput() {
                mViewModel?.hideError()
            }

            override fun onMaxCharsReached() {
                mViewModel?.nextEnabled = true
            }

            override fun onMaxCharsNotReached() {
                mViewModel?.nextEnabled = false
            }

        }))
    }

    fun getMobileNumber(): String {
        return mBinding?.etPhone?.text.toString().trim().replace(" ","")
    }


    override fun onAttach(context: Context?) {
        super.onAttach(context)
        if (context is OnMobileNumberEnteredListener) {
            mListener = context
        } else {
            throw RuntimeException(context!!.toString() + " must implement OnMobileNumberEnteredListener")
        }
    }

    override fun onDetach() {
        super.onDetach()
        mListener = null
    }

    interface OnMobileNumberEnteredListener {
        fun onMobileNumberEntered(mobileNumber: String,verificationUuid: String)
    }

    companion object {

        val TAG = MobileNumberFragment::class.java.simpleName
        fun newInstance(): MobileNumberFragment {
            val fragment = MobileNumberFragment()
            val args = Bundle()
            fragment.arguments = args
            return fragment
        }
    }

    fun showSnackbar(text: String) {
        mViewModel?.showSnackbar(mBinding?.root, text)
    }
}