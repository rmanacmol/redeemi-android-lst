package com.imfreemobile.redeemi.android.redeemiandroid.viewmodel;

import android.app.Application;
import android.content.Intent;
import android.databinding.ObservableBoolean;
import android.databinding.ObservableField;
import android.graphics.Color;
import android.support.design.widget.Snackbar;
import android.text.Editable;
import android.text.TextWatcher;
import android.text.method.HideReturnsTransformationMethod;
import android.text.method.PasswordTransformationMethod;
import android.view.View;

import com.imfreemobile.redeemi.android.redeemiandroid.R;
import com.imfreemobile.redeemi.android.redeemiandroid.databinding.ActivityLoginBinding;
import com.imfreemobile.redeemi.android.redeemiandroid.model.entity.LoginRequest;
import com.imfreemobile.redeemi.android.redeemiandroid.model.tables.Token;
import com.imfreemobile.redeemi.android.redeemiandroid.model.tables.User;
import com.imfreemobile.redeemi.android.redeemiandroid.model.networkmodel.EnvelopedUserLogin;
import com.imfreemobile.redeemi.android.redeemiandroid.model.networkmodel.EnvelopedVerification;
import com.imfreemobile.redeemi.android.redeemiandroid.model.networkmodel.Error;
import com.imfreemobile.redeemi.android.redeemiandroid.utils.AuthUtils;
import com.imfreemobile.redeemi.android.redeemiandroid.utils.RegistrationValidationutils;
import com.imfreemobile.redeemi.android.redeemiandroid.utils.networkutils.NoConnectivityException;
import com.imfreemobile.redeemi.android.redeemiandroid.view.ForgotPasswordActivity;
import com.imfreemobile.redeemi.android.redeemiandroid.view.LoginActivity;
import com.imfreemobile.redeemi.android.redeemiandroid.view.PinRequireActivity;

import java.io.IOException;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

/**
 * Created by rmanacmol on 14/11/2017.
 */

public class LoginViewModel extends BaseViewModel {

    private static final String TAG = LoginViewModel.class.getSimpleName();

    public ObservableField<String> password = new ObservableField<>();
    public ObservableField<String> phone = new ObservableField<>();
    public ObservableBoolean errorDisplay = new ObservableBoolean();
    public ObservableBoolean loading = new ObservableBoolean();
    public ObservableBoolean loginEnabled = new ObservableBoolean();
    public ActivityLoginBinding mBinding;
    private ObservableBoolean passwordToggle = new ObservableBoolean();
    private LoginActivity mActivity;
    private Call<EnvelopedUserLogin> netCallLogin;

    public LoginViewModel(Application application, LoginActivity activity, ActivityLoginBinding binding) {
        super(application, activity);
        this.mActivity = activity;
        this.mBinding = binding;
        errorDisplay.set(false);
        loading.set(false);

        mBinding.phone.addTextChangedListener(new RegistrationValidationutils.MobileNumberTextWatcher(new RegistrationValidationutils.DigitsTextWatcherCallback() {
            @Override
            public void onInput() {
                if (mBinding.phone.length() > 0 && mBinding.password.length() > 0) {
                    loginEnabled.set(true);
                } else {
                    loginEnabled.set(false);
                }
            }

            @Override
            public void onMaxCharsReached() {
            }

            @Override
            public void onMaxCharsNotReached() {
            }
        }));

        mBinding.password.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence s, int start, int count, int after) {

            }

            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count) {
            }

            @Override
            public void afterTextChanged(Editable s) {
                if (mBinding.phone.length() > 0 && mBinding.password.length() > 0) {
                    loginEnabled.set(true);
                } else {
                    loginEnabled.set(false);
                }
            }
        });
    }

    public void onPasswordToggleClicked(View view) {
        passwordToggle.set(!passwordToggle.get());
        int selectionStart = mBinding.password.getSelectionStart();
        int selectionEnd = mBinding.password.getSelectionEnd();
        if (mBinding.btnPasswordToggle.isChecked()) {
            mBinding.password.setTransformationMethod(HideReturnsTransformationMethod.getInstance());
        } else {
            mBinding.password.setTransformationMethod(PasswordTransformationMethod.getInstance());
        }
        if (selectionStart == selectionEnd) {
            mBinding.password.setSelection(selectionStart);
        } else {
            mBinding.password.setSelection(selectionStart, selectionEnd);
        }
    }

    public void onLoginButtonClicked(View view) {
        if (phone.get() != null && password.get() != null) {
            login(phone.get().trim().replace(" ", ""), password.get());
        } else {
            showErrorMessage(mActivity.getString(R.string.please_complete_fields));
        }
    }

    public void onForgotPasswordClicked(View view) {
        mActivity.startActivity(new Intent(mActivity, ForgotPasswordActivity.class));
    }

    private void login(final String mobileNum, String password) {
        loading.set(true);
        netCallLogin = apiService.login(new LoginRequest(
                AuthUtils.INSTANCE.generateHash(password),
                mobileNum,
                100.1,
                10.9, ""));

        netCallLogin.enqueue(new Callback<EnvelopedUserLogin>() {
            @Override
            public void onResponse(Call<EnvelopedUserLogin> call, Response<EnvelopedUserLogin> response) {
                loading.set(false);
                if (response.isSuccessful()) {
                    User user = response.body().getData();
                    user.setMobileNum(mobileNum);
                    appDatabase.getUserDao().deleteAll();
                    appDatabase.getUserDao().insert(user);
                    appDatabase.getTokenDao().insert(new Token(response.body().getData().getAccessToken()));
                    mActivity.finish();
                    Intent i = new Intent(mActivity, PinRequireActivity.class);
                    i.putExtra("isLoginPin", true);
                    mActivity.startActivity(new Intent(i));
                } else {
                    try {
                        handleError(response.errorBody().string());
                    } catch (IOException | NullPointerException e) {
                        e.printStackTrace();
                        showGenericError();
                    }
                    errorDisplay.set(true);
                }
            }

            @Override
            public void onFailure(Call<EnvelopedUserLogin> call, Throwable t) {
                loading.set(false);
                if (t instanceof NoConnectivityException) {
                    showNoInternetConnectionError();
                } else {
                    handleError(call.toString());
                }
            }
        });
    }

    private void handleError(String string) {
        try {
            Error error = gson.fromJson(string, EnvelopedVerification.class).getError();
            switch (error.getErrorCode()) {
                case 1007:
                    showErrorMessage(mActivity.getString(R.string.error_account_does_not_exist));
                    break;
                case 1013:
                    showErrorMessage(mActivity.getString(R.string.invalid_mobile_pass));
                    break;
                default:
                    showGenericError();
                    break;
            }

        } catch (Exception e) {
            showGenericError();
        }

    }

    private void showErrorMessage(String errorMessage) {
        Snackbar.make(mBinding.rootLayout, errorMessage,
                Snackbar.LENGTH_INDEFINITE).setActionTextColor(Color.WHITE)
                .setAction(mActivity.getString(R.string.string_okay),
                        new View.OnClickListener() {
                            @Override
                            public void onClick(View v) {
                                errorDisplay.set(false);
                                password.set("");
                                phone.set("");
                            }
                        }).show();
    }

    private void showNoInternetConnectionError() {
        Snackbar.make(mBinding.rootLayout, mActivity.getString(R.string.error_connection), Snackbar.LENGTH_INDEFINITE).setActionTextColor(Color.WHITE).setAction(R.string.okay, new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                errorDisplay.set(false);
                password.set("");
                phone.set("");
            }
        }).show();
    }

    private void showGenericError() {
        Snackbar.make(mBinding.rootLayout, mActivity.getString(R.string.error_generic),
                Snackbar.LENGTH_INDEFINITE).setActionTextColor(Color.WHITE)
                .setAction(mActivity.getString(R.string.string_okay),
                        new View.OnClickListener() {
                            @Override
                            public void onClick(View v) {
                                errorDisplay.set(false);
                                password.set("");
                                phone.set("");
                            }
                        }).show();
    }

}