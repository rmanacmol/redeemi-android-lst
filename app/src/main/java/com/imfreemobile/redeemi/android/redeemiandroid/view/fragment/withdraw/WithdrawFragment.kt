package com.imfreemobile.redeemi.android.redeemiandroid.view.fragment.withdraw

import android.content.Context
import android.databinding.DataBindingUtil
import android.net.Uri
import android.os.Bundle
import android.support.v4.app.Fragment
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup

import com.imfreemobile.redeemi.android.redeemiandroid.R
import com.imfreemobile.redeemi.android.redeemiandroid.databinding.FragmentWithdrawBinding
import com.imfreemobile.redeemi.android.redeemiandroid.view.fragment.BaseFragment
import com.imfreemobile.redeemi.android.redeemiandroid.viewmodel.WithdrawViewModel
import com.marcinorlowski.fonty.Fonty

/**
 * A simple [Fragment] subclass.
 * Activities that contain this fragment must implement the
 * [WithdrawFragment.OnWithdrawFragmentListener] interface
 * to handle interaction events.
 * Use the [WithdrawFragment.newInstance] factory method to
 * create an instance of this fragment.
 */
class WithdrawFragment : BaseFragment() {

    private var mListener: OnWithdrawFragmentListener? = null

    var mBinding: FragmentWithdrawBinding? = null
    var mViewModel: WithdrawViewModel? = null

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
    }

    override fun onCreateView(inflater: LayoutInflater?, container: ViewGroup?,
                              savedInstanceState: Bundle?): View? {
        super.onCreateView(inflater, container, savedInstanceState)
        mBinding = DataBindingUtil.inflate(inflater, R.layout.fragment_withdraw, container, false)
        mViewModel = WithdrawViewModel(activity.application, this.activity, parentFragment as WithdrawIncomeFragment, this@WithdrawFragment)
        mBinding?.viewModel = mViewModel
        Fonty.setFonts(mBinding?.root as ViewGroup)
        return mBinding?.root
    }

    override fun onViewCreated(view: View?, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
    }

    override fun onPause() {
        super.onPause()
        mViewModel?.cancelNetCall()
    }

    override fun onAttach(context: Context?) {
        super.onAttach(context)
        if (context is OnWithdrawFragmentListener) {
            mListener = context
        } else {
            throw RuntimeException(context!!.toString() + " must implement OnWithdrawFragmentListener")
        }
    }

    override fun onDetach() {
        super.onDetach()
        mListener = null
    }

    interface OnWithdrawFragmentListener {
        fun onFragmentInteraction(uri: Uri)
    }

    companion object {

        val TAG = WithdrawFragment::class.java.simpleName

        fun newInstance(): WithdrawFragment {
            val fragment = WithdrawFragment()
            val args = Bundle()
            fragment.arguments = args
            return fragment
        }
    }

    public fun showSnackbar(errorMessage: String) {
        mViewModel?.showSnackbar(mBinding?.root, errorMessage)
    }
}
