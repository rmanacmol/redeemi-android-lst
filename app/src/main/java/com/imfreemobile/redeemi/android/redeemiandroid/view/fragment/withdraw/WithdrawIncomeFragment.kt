package com.imfreemobile.redeemi.android.redeemiandroid.view.fragment.withdraw

import android.databinding.DataBindingUtil
import android.os.Bundle
import android.support.v4.app.Fragment
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup

import com.imfreemobile.redeemi.android.redeemiandroid.R
import com.imfreemobile.redeemi.android.redeemiandroid.databinding.FragmentWithdrawIncomeBinding
import com.imfreemobile.redeemi.android.redeemiandroid.view.fragment.BaseFragment
import com.imfreemobile.redeemi.android.redeemiandroid.viewmodel.WithdrawIncomeViewModel
import com.marcinorlowski.fonty.Fonty

/**
 * Created by rmanacmol on 06/11/2017.
 */

class WithdrawIncomeFragment : BaseFragment() {

    var mBinding: FragmentWithdrawIncomeBinding? = null
    var mViewModel: WithdrawIncomeViewModel? = null

    override fun onCreateView(inflater: LayoutInflater?, container: ViewGroup?, savedInstanceState: Bundle?): View? {
        super.onCreateView(inflater, container, savedInstanceState)
        mBinding = DataBindingUtil.inflate(inflater!!, R.layout.fragment_withdraw_income, container, false)
        mViewModel = WithdrawIncomeViewModel(activity.application, this.activity, this)
        mBinding?.viewModel = mViewModel
        Fonty.setFonts(mBinding?.root as ViewGroup)
        return mBinding?.root
    }

    override fun onResume() {
        super.onResume()
        mViewModel?.setupPager()
    }

}
