package com.imfreemobile.redeemi.android.redeemiandroid.view.fragment;

import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.databinding.DataBindingUtil;
import android.os.Bundle;
import android.support.v4.content.LocalBroadcastManager;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.imfreemobile.redeemi.android.redeemiandroid.R;
import com.imfreemobile.redeemi.android.redeemiandroid.databinding.FragmentHomeBinding;
import com.imfreemobile.redeemi.android.redeemiandroid.view.BottomNavActivity;
import com.imfreemobile.redeemi.android.redeemiandroid.viewmodel.HomeViewModel;
import com.marcinorlowski.fonty.Fonty;

import timber.log.Timber;

/**
 * Created by rmanacmol on 06/11/2017.
 */

public class HomeFragment extends BaseFragment {

    private static final String TAG = HomeFragment.class.getSimpleName();
    FragmentHomeBinding mBinding;
    HomeViewModel mViewModel;

    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        super.onCreateView(inflater, container, savedInstanceState);
        mBinding = DataBindingUtil.inflate(inflater, R.layout.fragment_home, container, false);
        mViewModel = new HomeViewModel(getActivity().getApplication(), (BottomNavActivity) this.getActivity());
        mBinding.setViewModel(mViewModel);
        Fonty.setFonts((ViewGroup) mBinding.getRoot());
        return mBinding.getRoot();
    }

    @Override
    public void onResume() {
        super.onResume();
        mViewModel.fetchLocalStoreDetails();
        mViewModel.fetchStoreDetails();
        LocalBroadcastManager.getInstance(getActivity()).registerReceiver(profileUpdateReceiver, new IntentFilter(getActivity().getPackageName() + "profile_updated"));
    }

    @Override
    public void onPause() {
        super.onPause();
        mViewModel.cancelNetCall();
        LocalBroadcastManager.getInstance(getActivity()).unregisterReceiver(profileUpdateReceiver);
    }

    private BroadcastReceiver profileUpdateReceiver = new BroadcastReceiver() {
        @Override
        public void onReceive(Context context, Intent intent) {
            Timber.d(TAG, "event received");
            mViewModel.fetchStoreDetails();
        }
    };

}
