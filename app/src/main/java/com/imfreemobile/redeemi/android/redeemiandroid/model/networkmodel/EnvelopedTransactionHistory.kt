package com.imfreemobile.redeemi.android.redeemiandroid.model.networkmodel

import com.google.gson.annotations.Expose
import com.google.gson.annotations.SerializedName
import com.imfreemobile.redeemi.android.redeemiandroid.model.entity.Transaction

/**
 * Created by rmanacmol on 08/11/2017.
 */

class EnvelopedTransactionHistory {

    @Expose
    @SerializedName(DATA)
    val data: List<Transaction>? = null

    @Expose
    @SerializedName(ERROR)
    val error: Error? = null

    companion object {
        const val DATA = "data"
        const val ERROR = "error"
    }

}
