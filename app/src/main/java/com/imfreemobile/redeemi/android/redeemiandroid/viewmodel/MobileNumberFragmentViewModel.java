package com.imfreemobile.redeemi.android.redeemiandroid.viewmodel;

import android.app.Application;
import android.content.Context;
import android.databinding.ObservableBoolean;
import android.databinding.ObservableField;
import android.graphics.Color;
import android.os.CountDownTimer;
import android.support.design.widget.Snackbar;
import android.view.View;
import android.view.inputmethod.InputMethodManager;

import com.imfreemobile.redeemi.android.redeemiandroid.BR;
import com.imfreemobile.redeemi.android.redeemiandroid.R;
import com.imfreemobile.redeemi.android.redeemiandroid.model.entity.VerificationRequest;
import com.imfreemobile.redeemi.android.redeemiandroid.model.networkmodel.EnvelopedVerification;
import com.imfreemobile.redeemi.android.redeemiandroid.model.networkmodel.Error;
import com.imfreemobile.redeemi.android.redeemiandroid.utils.networkutils.NoConnectivityException;
import com.imfreemobile.redeemi.android.redeemiandroid.view.ForgotPasswordActivity;
import com.imfreemobile.redeemi.android.redeemiandroid.view.fragment.forgot.MobileNumberFragment;

import java.io.IOException;
import java.util.Locale;
import java.util.concurrent.TimeUnit;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

/**
 * Created by jvillarey on 15/11/2017.
 */

public class MobileNumberFragmentViewModel extends BaseViewModel {

    private static final String TAG = MobileNumberFragmentViewModel.class.getSimpleName();

    private ForgotPasswordActivity mActivity;
    private MobileNumberFragment mFragment;
    private ObservableBoolean nextEnabled = new ObservableBoolean();
    private ObservableBoolean errorDisplaying = new ObservableBoolean();
    private ObservableBoolean shouldShowCountdown = new ObservableBoolean();
    private ObservableField<String> countdownValue = new ObservableField<>();
    private ObservableBoolean loading = new ObservableBoolean();
    private Call<EnvelopedVerification> networkCallForgotPassword;
    private CountDownTimer timer;

    private Snackbar snackbar;

    public void setShouldShowCountdown(boolean shouldShowCountdown) {
        this.shouldShowCountdown.set(shouldShowCountdown);
        notifyPropertyChanged(BR._all);
    }

    public boolean getShouldShowCountdown() {
        return this.shouldShowCountdown.get();
    }

    public void setCountdownValue(String countdownValue) {
        this.countdownValue.set(countdownValue);
        notifyPropertyChanged(BR._all);
    }

    public String getCountdownValue() {
        return this.countdownValue.get();
    }

    public void setLoading(boolean loading) {
        this.loading.set(loading);
        notifyPropertyChanged(BR._all);
    }

    public boolean getLoading() {
        return this.loading.get();
    }

    public boolean getNextEnabled() {
        return this.nextEnabled.get();
    }

    public void setNextEnabled(boolean nextEnabled) {
        this.nextEnabled.set(nextEnabled);
        notifyPropertyChanged(BR._all);
    }

    public boolean getErrorDisplaying() {
        return this.errorDisplaying.get();
    }

    public void setErrorDisplaying(boolean errorDisplaying) {
        this.errorDisplaying.set(errorDisplaying);
        notifyPropertyChanged(BR._all);
    }

    public MobileNumberFragmentViewModel(Application application, ForgotPasswordActivity activity, MobileNumberFragment fragment) {
        super(application, activity);
        this.mActivity = activity;
        this.mFragment = fragment;
    }

    private void fetchVerificationStatus(final String mobileNumber) {
        setLoading(true);
        networkCallForgotPassword = apiService.forgotPassword(new VerificationRequest(mobileNumber));
        networkCallForgotPassword.enqueue(new Callback<EnvelopedVerification>() {
            @Override
            public void onResponse(Call<EnvelopedVerification> call, Response<EnvelopedVerification> response) {
                setLoading(false);
                if (response.isSuccessful()) {
                    mFragment.getMListener().onMobileNumberEntered(mobileNumber, response.body().getData().getVerificationUuid());
                } else {
                    try {
                        handleError(response.errorBody().string());
                    } catch (IOException | NullPointerException e) {
                        e.printStackTrace();
                        showGenericError();
                    }
                }
            }

            @Override
            public void onFailure(Call<EnvelopedVerification> call, Throwable t) {
                setLoading(false);
                if (t instanceof NoConnectivityException) {
                    showNoInternetError();
                } else {
                    showGenericError();
                }
            }
        });
    }

    private void handleError(String string) {
        try {
            Error error = gson
                    .fromJson(string, EnvelopedVerification.class).getError();
            switch (error.getErrorCode()) {
                case 1009:
                    showAccountNotRegistered();
                    break;
                case 1022:
                    showRetryAt(error.getRetryAt());
                    break;
                case 1007:
                    showAccountDoesNotExistError();
                    break;
                case 1024:
                    showAccountNotActivatedError();
                    break;
                default:
                    showGenericError();
                    break;
            }
        } catch (Exception e) {
            showGenericError();
        }
    }

    private void showAccountNotRegistered() {
        mFragment.showSnackbar(mActivity.getString(R.string.error_account_not_yet_registered));
    }

    private void showAccountNotActivatedError() {
        mFragment.showSnackbar(mActivity.getString(R.string.error_account_not_activated));
    }

    private void showRetryAt(Long retryAt) {
        if (retryAt != null) {
            mFragment.showSnackbar(mActivity.getString(R.string.error_limit_reached));
            setShouldShowCountdown(true);
            startTimer(retryAt);
        }
    }

    private void startTimer(long retryAt) {
        if (timer != null) {
            timer.cancel();
        }
        timer = new CountDownTimer(retryAt, 1000) {
            @Override
            public void onTick(long millis) {
                setCountdownValue(String.format(Locale.ENGLISH, "%02d:%02d",
                        TimeUnit.MILLISECONDS.toMinutes(millis) -
                                TimeUnit.HOURS.toMinutes(TimeUnit.MILLISECONDS.toHours(millis)),
                        TimeUnit.MILLISECONDS.toSeconds(millis) -
                                TimeUnit.MINUTES.toSeconds(TimeUnit.MILLISECONDS.toMinutes(millis))));
            }

            @Override
            public void onFinish() {
                setShouldShowCountdown(false);
            }
        };
        timer.start();
    }

    private void showAccountDoesNotExistError() {
        mFragment.showSnackbar(mActivity.getString(R.string.error_account_does_not_exist));
    }

    private void showNoInternetError() {
        mFragment.showSnackbar(mActivity.getString(R.string.error_connection));
    }

    private void showGenericError() {
        mFragment.showSnackbar(mActivity.getString(R.string.error_generic));
    }

    public void onNextButtonClicked(View view) {
        fetchVerificationStatus(mFragment.getMobileNumber());
    }

    public void hideError() {
        setErrorDisplaying(false);
        if (snackbar != null) {
            snackbar.dismiss();
        }
    }

    private void hideKeyboard() {
        InputMethodManager imm = (InputMethodManager) mActivity.getSystemService(Context.INPUT_METHOD_SERVICE);
        imm.hideSoftInputFromWindow(mFragment.getMBinding().getRoot().getWindowToken(), 0);
    }

    public void showSnackbar(View view, String text) {
        if (snackbar == null) {
            snackbar = Snackbar.make(view, text, Snackbar.LENGTH_INDEFINITE);
        } else {
            snackbar.setText(text);
        }
        snackbar.setAction(R.string.okay, new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                snackbar.dismiss();
            }
        });
        snackbar.setActionTextColor(Color.WHITE);
        snackbar.show();
        setErrorDisplaying(true);
    }
}
