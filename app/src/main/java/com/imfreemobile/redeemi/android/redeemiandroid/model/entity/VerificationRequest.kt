package com.imfreemobile.redeemi.android.redeemiandroid.model.entity

import com.google.gson.annotations.Expose
import com.google.gson.annotations.SerializedName

/**
 * Created by jvillarey on 13/11/2017.
 */

class VerificationRequest(@field:Expose
                          @field:SerializedName(MOBILE_NUMBER)
                          private val mobileNumber: String) {

    companion object {
        const val MOBILE_NUMBER = "mobile_number"
    }
}
