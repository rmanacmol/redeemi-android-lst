package com.imfreemobile.redeemi.android.redeemiandroid.viewmodel;

import android.app.Application;
import android.content.Context;
import android.databinding.ObservableBoolean;
import android.support.design.widget.Snackbar;
import android.text.method.HideReturnsTransformationMethod;
import android.text.method.PasswordTransformationMethod;
import android.util.Log;
import android.view.View;
import android.view.inputmethod.InputMethodManager;

import com.imfreemobile.redeemi.android.redeemiandroid.BR;
import com.imfreemobile.redeemi.android.redeemiandroid.R;
import com.imfreemobile.redeemi.android.redeemiandroid.view.RegistrationFlowActivity;
import com.imfreemobile.redeemi.android.redeemiandroid.view.fragment.registration1.ConfirmPasswordFragment;

/**
 * Created by jvillarey on 14/11/2017.
 */

public class ConfirmPasswordViewModel extends BaseViewModel {

    private static final String TAG = ConfirmPasswordViewModel.class.getSimpleName();

    private ConfirmPasswordFragment.OnConfirmPasswordListener mListener;
    private ConfirmPasswordFragment mFragment;
    private ObservableBoolean nextEnabled = new ObservableBoolean();
    private ObservableBoolean passwordToggle = new ObservableBoolean();

    private Snackbar snackbar;

    public void setNextEnabled(boolean nextEnabled) {
        this.nextEnabled.set(nextEnabled);
        notifyPropertyChanged(BR._all);
    }

    public void setPasswordToggle(boolean passwordToggle) {
        this.passwordToggle.set(passwordToggle);
        notifyPropertyChanged(BR._all);
    }

    public boolean getPasswordToggle() {
        return this.passwordToggle.get();
    }

    public boolean getNextEnabled() {
        return this.nextEnabled.get();
    }

    public ConfirmPasswordViewModel(Application application, ConfirmPasswordFragment.OnConfirmPasswordListener listener, ConfirmPasswordFragment fragment) {
        super(application, fragment.getActivity());
        this.mListener = listener;
        this.mFragment = fragment;
    }

    public void hideKeyboard() {
        InputMethodManager imm = (InputMethodManager) mFragment.getActivity().getSystemService(Context.INPUT_METHOD_SERVICE);
        imm.hideSoftInputFromWindow(mFragment.getMBinding().getRoot().getWindowToken(), 0);
    }

    public void onNextButtonClicked(View view) {
        hideKeyboard();
        hideError();
        if (mFragment.getMPassword().equals(mFragment.getMBinding().etPassword.getText().toString())) {
            mFragment.getMListener().onPasswordConfirmed(mFragment.getMBinding().etPassword.getText().toString());
        } else {
            mFragment.showSnackbar(mFragment.getActivity().getString(R.string.password_not_match));
        }
    }

    public void showSnackbar(View view, String text) {
        if (snackbar == null) {
            snackbar = Snackbar.make(view, text, Snackbar.LENGTH_INDEFINITE);
        } else {
            snackbar.setText(text);
        }
        snackbar.setAction(R.string.try_again, new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                snackbar.dismiss();
            }
        });
        snackbar.show();
    }

    public void hideError() {
        if (snackbar != null) {
            snackbar.dismiss();
        }
    }

    public void onPasswordToggleClicked(View view) {

        setPasswordToggle(!getPasswordToggle());

        int selectionStart = mFragment.getMBinding().etPassword.getSelectionStart();
        int selectionEnd = mFragment.getMBinding().etPassword.getSelectionEnd();
        if (mFragment.getMBinding().btnPasswordToggle.isChecked()) {
            mFragment.getMBinding().etPassword.setTransformationMethod(HideReturnsTransformationMethod.getInstance());
        } else {
            mFragment.getMBinding().etPassword.setTransformationMethod(PasswordTransformationMethod.getInstance());
        }
        if (selectionStart == selectionEnd) {
            mFragment.getMBinding().etPassword.setSelection(selectionStart);
        } else {
            mFragment.getMBinding().etPassword.setSelection(selectionStart, selectionEnd);
        }
    }

}
