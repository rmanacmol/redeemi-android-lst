package com.imfreemobile.redeemi.android.redeemiandroid.view.fragment.registration;

import android.content.Context;
import android.databinding.DataBindingUtil;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.alimuzaffar.lib.pin.PinEntryEditText;
import com.imfreemobile.redeemi.android.redeemiandroid.R;
import com.imfreemobile.redeemi.android.redeemiandroid.databinding.FragmentPinEnterBinding;
import com.imfreemobile.redeemi.android.redeemiandroid.view.RegistrationFlowActivity;
import com.imfreemobile.redeemi.android.redeemiandroid.view.fragment.BaseFragment;
import com.imfreemobile.redeemi.android.redeemiandroid.viewmodel.PinEnterViewModel;
import com.marcinorlowski.fonty.Fonty;

/**
 * Created by rmanacmol on 13/11/2017.
 */

public class PinEnterFragment extends BaseFragment {

    public interface OnPinEnterListener {
        void onPinSet(String pin);
    }

    public FragmentPinEnterBinding mBinding;
    private PinEnterViewModel mViewModel;
    private OnPinEnterListener mListener;

    public OnPinEnterListener getMListener() {
        return this.mListener;
    }

    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        super.onCreateView(inflater, container, savedInstanceState);
        mBinding = DataBindingUtil.inflate(inflater, R.layout.fragment_pin_enter, container, false);
        mViewModel = new PinEnterViewModel(getActivity().getApplication(),
                this.getActivity(), mListener);
        mBinding.setViewModel(mViewModel);
        Fonty.setFonts((ViewGroup) mBinding.getRoot());
        return mBinding.getRoot();
    }

    @Override
    public void onViewCreated(View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);

        mBinding.pinView.setOnPinEnteredListener(new PinEntryEditText.OnPinEnteredListener() {
            @Override
            public void onPinEntered(CharSequence str) {
                mViewModel.onPinEntered(str, getView());
            }
        });
    }

    public void showKeyboard() {
        mBinding.pinView.focus();
    }

    @Override
    public void onAttach(Context context) {
        super.onAttach(context);
        if (context instanceof OnPinEnterListener) {
            mListener = (OnPinEnterListener) context;
        } else {
            throw new IllegalArgumentException(context.getClass().getSimpleName() + " must implement " + OnPinEnterListener.class.getSimpleName());
        }
    }

    @Override
    public void onDetach() {
        super.onDetach();
        mListener = null;
    }
}
