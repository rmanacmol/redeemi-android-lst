package com.imfreemobile.redeemi.android.redeemiandroid.view;

import android.databinding.DataBindingUtil;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.view.ViewGroup;

import com.imfreemobile.redeemi.android.redeemiandroid.R;
import com.imfreemobile.redeemi.android.redeemiandroid.databinding.ActivityPromoStatusBinding;
import com.imfreemobile.redeemi.android.redeemiandroid.viewmodel.PromoStatusViewModel;
import com.marcinorlowski.fonty.Fonty;

/**
 * Created by renzmanacmol on 24/10/2017.
 */

public class PromoStatusActivity extends BaseLocationActivity {

    private ActivityPromoStatusBinding mBinding;
    private PromoStatusViewModel mViewModel;

    @Override
    protected boolean isPinRequiredInActivity() {
        return true;
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        mBinding = DataBindingUtil.setContentView(this, R.layout.activity_promo_status);
        mViewModel = new PromoStatusViewModel(getApplication(), this, getIntent().getIntExtra("error_code", 0));
        mBinding.setViewModel(mViewModel);

        Fonty.setFonts((ViewGroup) mBinding.getRoot());
    }

    @Override
    protected void onResumeComplete() {

    }
}
