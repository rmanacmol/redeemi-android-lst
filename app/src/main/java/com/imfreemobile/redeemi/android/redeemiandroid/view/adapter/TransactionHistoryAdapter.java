package com.imfreemobile.redeemi.android.redeemiandroid.view.adapter;

import android.databinding.DataBindingUtil;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.ViewGroup;

import com.imfreemobile.redeemi.android.redeemiandroid.R;
import com.imfreemobile.redeemi.android.redeemiandroid.databinding.ItemRowTransactionActivitiesBinding;
import com.imfreemobile.redeemi.android.redeemiandroid.model.entity.Transaction;
import com.marcinorlowski.fonty.Fonty;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by rmanacmol on 08/11/2017.
 */

public class TransactionHistoryAdapter extends RecyclerView.Adapter<TransactionHistoryAdapter.THViewHolder> {

    private List<Transaction> transactions;

    public TransactionHistoryAdapter() {
        this.transactions = new ArrayList<>();
    }

    public void setItems(List<Transaction> list) {
        this.transactions.clear();
        this.transactions.addAll(list);
    }

    @Override
    public TransactionHistoryAdapter.THViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {

        ItemRowTransactionActivitiesBinding itemRowTransactionActivitiesBinding = DataBindingUtil.inflate(LayoutInflater.from(parent.getContext()), R.layout.item_row_transaction_activities, parent, false);
        Fonty.setFonts((ViewGroup) itemRowTransactionActivitiesBinding.getRoot());
        return new THViewHolder(itemRowTransactionActivitiesBinding);
    }

    @Override
    public void onBindViewHolder(THViewHolder holder, int position) {
        holder.bindItem(transactions.get(position));
    }

    @Override
    public int getItemCount() {
        return transactions.size();
    }

    public class THViewHolder extends RecyclerView.ViewHolder {

        ItemRowTransactionActivitiesBinding mBinding;

        public THViewHolder(ItemRowTransactionActivitiesBinding itemRowNameBinding) {
            super(itemRowNameBinding.getRoot());
            this.mBinding = itemRowNameBinding;
        }

        void bindItem(Transaction transaction) {
            mBinding.setViewModel(transaction);
            mBinding.executePendingBindings();
        }
    }
}
