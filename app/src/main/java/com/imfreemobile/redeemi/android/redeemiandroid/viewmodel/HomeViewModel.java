package com.imfreemobile.redeemi.android.redeemiandroid.viewmodel;

import android.Manifest;
import android.app.Application;
import android.content.Intent;
import android.databinding.ObservableField;
import android.net.Uri;
import android.provider.Settings;
import android.support.annotation.NonNull;
import android.support.v7.widget.PopupMenu;
import android.util.Log;
import android.view.MenuItem;
import android.view.View;

import com.afollestad.materialdialogs.DialogAction;
import com.afollestad.materialdialogs.MaterialDialog;
import com.imfreemobile.redeemi.android.redeemiandroid.BR;
import com.imfreemobile.redeemi.android.redeemiandroid.R;
import com.imfreemobile.redeemi.android.redeemiandroid.model.networkmodel.EnvelopedUser;
import com.imfreemobile.redeemi.android.redeemiandroid.model.tables.User;
import com.imfreemobile.redeemi.android.redeemiandroid.utils.AuthUtils;
import com.imfreemobile.redeemi.android.redeemiandroid.view.BottomNavActivity;
import com.imfreemobile.redeemi.android.redeemiandroid.view.LoginActivity;
import com.imfreemobile.redeemi.android.redeemiandroid.view.ProfileActivity;
import com.imfreemobile.redeemi.android.redeemiandroid.view.ScanActivity;
import com.karumi.dexter.Dexter;
import com.karumi.dexter.PermissionToken;
import com.karumi.dexter.listener.PermissionDeniedResponse;
import com.karumi.dexter.listener.PermissionGrantedResponse;
import com.karumi.dexter.listener.PermissionRequest;
import com.karumi.dexter.listener.single.PermissionListener;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;
import timber.log.Timber;

/**
 * Created by renzmanacmol on 23/10/2017.
 */

public class HomeViewModel extends BaseViewModel {

    private static final String TAG = HomeViewModel.class.getSimpleName();

    private BottomNavActivity mActivity;
    private ObservableField<User> user = new ObservableField<>();
    private Call<EnvelopedUser> networkCallStoreDetails;
    private Call logoutCall;

    public User getUser() {
        if (user.get() == null && appDatabase.getUserDao().getAllUser() != null) {
            return appDatabase.getUserDao().getUser();
        }
        return user.get();
    }

    public void setUser(User user) {
        this.user.set(user);
        notifyPropertyChanged(BR._all);
        if (!user.getUserType().equals(User.Companion.getTYPE_OWNER())) {
            mActivity.showFrontlinerView();
        }
    }

    public HomeViewModel(Application application, BottomNavActivity activity) {
        super(application, activity);
        this.mActivity = activity;
        User user = appDatabase.getUserDao().getUser();
        if (user == null) {
            user = new User();
        }
        this.user.set(user);
    }

    private void logout() {
        logoutCall = apiService.logout(AuthUtils.INSTANCE.getAccessHeaderFormattedToken(appDatabase.getTokenDao().getToken().getAccessToken()));
        logoutCall.enqueue(new Callback() {
            @Override
            public void onResponse(Call call, Response response) {
                Timber.d(TAG, "logout success");
            }

            @Override
            public void onFailure(Call call, Throwable t) {
                Timber.d(TAG, "logout fail");
            }
        });
        appDatabase.getUserDao().deleteAll();
        appDatabase.getTokenDao().deleteAll();
    }

    public void fetchLocalStoreDetails() {
        User user = appDatabase.getUserDao().getUser();
        if (user != null) {
            setUser(user);
        }
    }

    public void fetchStoreDetails() {
        networkCallStoreDetails = apiService.getStoreDetails(AuthUtils.INSTANCE.getAccessHeaderFormattedToken(appDatabase.getTokenDao().getToken().getAccessToken()));
        networkCallStoreDetails.enqueue(new Callback<EnvelopedUser>() {
            @Override
            public void onResponse(Call<EnvelopedUser> call, Response<EnvelopedUser> response) {
                if (response.isSuccessful()) {
                    User user = response.body().getData();
                    user.setMobileNum(appDatabase.getUserDao().getUser().getMobileNum());
                    appDatabase.getUserDao().deleteAll();
                    appDatabase.getUserDao().insert(user);
                    setUser(user);
                }
            }

            @Override
            public void onFailure(Call<EnvelopedUser> call, Throwable t) {
                Log.e(TAG, "failed getStoreDetails request");
            }
        });
    }

    public void onScanBarCodeClicked(View view) {

        Dexter.withActivity(mActivity).withPermission(Manifest.permission.CAMERA).withListener(new PermissionListener() {
            @Override
            public void onPermissionGranted(PermissionGrantedResponse response) {
                mActivity.startActivity(new Intent(mActivity, ScanActivity.class));
            }

            @Override
            public void onPermissionDenied(PermissionDeniedResponse response) {
                new MaterialDialog.Builder(mActivity)
                        .title(R.string.home_dialog_permission_denied_content).onPositive(new MaterialDialog.SingleButtonCallback() {
                    @Override
                    public void onClick(@NonNull MaterialDialog dialog, @NonNull DialogAction which) {
                        Intent i = new Intent(Settings.ACTION_APPLICATION_DETAILS_SETTINGS);
                        i.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
                        Uri uri = Uri.fromParts("package", mActivity.getPackageName(), null);
                        i.setData(uri);
                        mActivity.startActivity(i);
                        dialog.dismiss();
                    }
                }).cancelable(false).iconRes(R.drawable.icon_camera).positiveText(R.string.home_dialog_settings).show();
            }

            @Override
            public void onPermissionRationaleShouldBeShown(PermissionRequest permission, PermissionToken token) {
                token.continuePermissionRequest();
            }
        }).check();
    }

    public void onViewTransactionDetailsClicked(View view) {
        mActivity.findViewById(R.id.navigation).findViewById(R.id.nav_transaction).performClick();
    }

    public void cancelNetCall() {
        if (networkCallStoreDetails != null) {
            networkCallStoreDetails.cancel();
            networkCallStoreDetails = null;
        }
    }

    public void onProfilePicClicked(View view) {
        PopupMenu popup = new PopupMenu(mActivity, view);
        popup.getMenuInflater().inflate(R.menu.menu_home_popup, popup.getMenu());
        popup.setOnMenuItemClickListener(new PopupMenu.OnMenuItemClickListener() {
            @Override
            public boolean onMenuItemClick(MenuItem item) {
                switch (item.getItemId()) {
                    case R.id.one:
                        mActivity.startActivity(new Intent(mActivity, ProfileActivity.class));
                        break;
                    case R.id.two:
                        logout();
                        mActivity.startActivity(new Intent(mActivity, LoginActivity.class));
                        mActivity.finish();
                        break;
                    default:
                        break;
                }
                return true;
            }
        });
        popup.show();
    }

}
