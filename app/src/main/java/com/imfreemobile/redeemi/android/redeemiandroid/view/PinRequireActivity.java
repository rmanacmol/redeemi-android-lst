package com.imfreemobile.redeemi.android.redeemiandroid.view;

import android.content.Context;
import android.content.Intent;
import android.databinding.DataBindingUtil;
import android.graphics.Color;
import android.os.Bundle;
import android.support.design.widget.Snackbar;
import android.support.v4.app.ActivityCompat;
import android.view.View;
import android.view.ViewGroup;
import android.view.inputmethod.InputMethodManager;

import com.alimuzaffar.lib.pin.PinEntryEditText;
import com.imfreemobile.redeemi.android.redeemiandroid.R;
import com.imfreemobile.redeemi.android.redeemiandroid.databinding.ActivityPinRequireBinding;
import com.imfreemobile.redeemi.android.redeemiandroid.viewmodel.PinRequireViewModel;
import com.marcinorlowski.fonty.Fonty;

/**
 * Created by rmanacmol on 15/11/2017.
 */

public class PinRequireActivity extends BaseLocationActivity {

    private ActivityPinRequireBinding mBinding;
    private PinRequireViewModel mViewModel;

    @Override
    protected boolean isPinRequiredInActivity() {
        return false;
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        mBinding = DataBindingUtil.setContentView(this, R.layout.activity_pin_require);
        mViewModel = new PinRequireViewModel(getApplication(), this);
        mBinding.setViewModel(mViewModel);

        Fonty.setFonts((ViewGroup) mBinding.getRoot());
    }

    @Override
    protected void onResume() {
        super.onResume();
        if (getSystemService(Context.INPUT_METHOD_SERVICE) != null) {
            ((InputMethodManager) getSystemService(Context.INPUT_METHOD_SERVICE))
                    .toggleSoftInput(InputMethodManager.SHOW_FORCED, InputMethodManager.HIDE_IMPLICIT_ONLY);
        }
        mBinding.pinView.focus();
        mBinding.pinView.setOnPinEnteredListener(new PinEntryEditText.OnPinEnteredListener() {
            @Override
            public void onPinEntered(CharSequence str) {
                mViewModel.onPinEntered(str, mBinding.pinView);
            }
        });

    }

    @Override
    protected void onResumeComplete() {

    }

    public void showError(String message, boolean isMaxAttempt) {
        if (isMaxAttempt) {
            Snackbar.make(mBinding.rootLayout, message,
                    Snackbar.LENGTH_INDEFINITE).setActionTextColor(Color.WHITE)
                    .setAction(getString(R.string.string_okay),
                            new View.OnClickListener() {
                                @Override
                                public void onClick(View v) {
                                    ActivityCompat.finishAffinity(PinRequireActivity.this);
                                    startActivity(new Intent(PinRequireActivity.this, LoginActivity.class));
                                }
                            }).show();

        } else {
            Snackbar.make(mBinding.rootLayout, message,
                    Snackbar.LENGTH_INDEFINITE).setActionTextColor(Color.WHITE)
                    .setAction(getString(R.string.string_okay),
                            new View.OnClickListener() {
                                @Override
                                public void onClick(View v) {
                                    mBinding.pinView.setText(null);
                                    mBinding.pinView.focus();
                                }
                            }).show();
        }
    }

}
