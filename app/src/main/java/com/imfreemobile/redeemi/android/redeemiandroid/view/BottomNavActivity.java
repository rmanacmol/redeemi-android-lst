package com.imfreemobile.redeemi.android.redeemiandroid.view;

import android.databinding.DataBindingUtil;
import android.net.Uri;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.v7.app.AppCompatActivity;
import android.view.MenuItem;
import android.view.ViewGroup;

import com.afollestad.materialdialogs.MaterialDialog;
import com.imfreemobile.redeemi.android.redeemiandroid.R;
import com.imfreemobile.redeemi.android.redeemiandroid.databinding.ActivityBaseTabBinding;
import com.imfreemobile.redeemi.android.redeemiandroid.model.db.AppDatabase;
import com.imfreemobile.redeemi.android.redeemiandroid.view.fragment.withdraw.WithdrawFragment;
import com.imfreemobile.redeemi.android.redeemiandroid.view.fragment.withdraw.WithdrawHistoryFragment;
import com.imfreemobile.redeemi.android.redeemiandroid.viewmodel.BottomNavViewModel;
import com.marcinorlowski.fonty.Fonty;

import org.jetbrains.annotations.NotNull;

import javax.inject.Inject;

/**
 * Created by rmanacmol on 06/11/2017.
 */

public class BottomNavActivity extends BaseLocationActivity implements WithdrawFragment.OnWithdrawFragmentListener, WithdrawHistoryFragment.OnWithdrawHistoryListener {

    public ActivityBaseTabBinding mBinding;
    public BottomNavViewModel mViewModel;
    private MaterialDialog loadingDialog;

    @Override
    protected boolean isPinRequiredInActivity() {
        return true;
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        mBinding = DataBindingUtil.setContentView(this, R.layout.activity_base_tab);
        mViewModel = new BottomNavViewModel(getApplication(), this);
        mBinding.setViewModel(mViewModel);
        mViewModel.initFragment();

        Fonty.setFonts((ViewGroup) mBinding.getRoot());
    }

    public void showFrontlinerView() {
        mViewModel.frontLinerView();
    }

    public void showLoadingDialog() {
        if (loadingDialog == null) {
            loadingDialog = new MaterialDialog.Builder(BottomNavActivity.this).content("Loading").progress(true, 0).build();
        }
        loadingDialog.show();
    }

    public void hideLoadingDialog() {
        if (loadingDialog != null) {
            loadingDialog.dismiss();
        }
    }

    public void showError(String errorString) {
        try {
            mViewModel.onError(mBinding.getRoot(), errorString);
        }catch (NullPointerException e){}
    }

    @Override
    protected void onResumeComplete() {

    }

    @Override
    public void onFragmentInteraction(@NotNull Uri uri) {

    }
}
