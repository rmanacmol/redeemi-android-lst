package com.imfreemobile.redeemi.android.redeemiandroid.model.networkmodel

import com.google.gson.annotations.Expose
import com.google.gson.annotations.SerializedName
import com.imfreemobile.redeemi.android.redeemiandroid.model.tables.User

/**
 * Created by rmanacmol on 14/11/2017.
 */

class EnvelopedUserLogin {

    @Expose
    @SerializedName(DATA)
    val data: User? = null

    @Expose
    @SerializedName(ERROR)
    val error: Error? = null

    companion object {
        const val DATA = "data"
        const val ERROR = "error"
    }

}
