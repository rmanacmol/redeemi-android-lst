package com.imfreemobile.redeemi.android.redeemiandroid.model.tables

import android.arch.persistence.room.ColumnInfo
import android.arch.persistence.room.Entity
import android.arch.persistence.room.PrimaryKey

import com.google.gson.annotations.Expose
import com.google.gson.annotations.SerializedName

/**
 * Created by renzmanacmol on 24/10/2017.
 */
@Entity(tableName = "user")
class User {

    @PrimaryKey(autoGenerate = true)
    @ColumnInfo(name = "id")
    var id: Int? = null
    @Expose
    @SerializedName(WALLET_BALANCE_INCENTIVE)
    var walletBalanceIncentive: String? = null
    @Expose
    @SerializedName(FIRST_NAME)
    var firstName: String? = null
    @Expose
    @SerializedName(LAST_NAME)
    var lastName: String? = null
    @Expose
    @SerializedName(WALLET_BALANCE)
    var walletBalance: String? = null
    @Expose
    @SerializedName(UPDATED_AT)
    var updatedAt: String? = null
    @Expose
    @SerializedName(USER_TYPE)
    var userType: String? = null
    @Expose
    @SerializedName(CREATED_AT)
    var createdAt: String? = null
    @Expose
    @SerializedName(TOTAL_SUM)
    var totalSum: String? = null
    @Expose
    @SerializedName(PIN)
    var pin: String? = null
    @Expose
    @SerializedName(ACCESS_TOKEN)
    var accessToken: String? = null
    @Expose
    @SerializedName(PROFILE_PIC_URL)
    var profilePicUrl: String? = null
    @Expose
    @SerializedName(MOBILE_NUM)
    var mobileNum: String? = null
    @Expose
    @SerializedName(EMAIL_ADDRESS)
    var emailAddress: String? = null
    @Expose
    @SerializedName(WITHDRAWABLE_AMOUNT)
    var withdrawableAmount: String? = null

    companion object {
        const val FIRST_NAME = "first_name"
        const val LAST_NAME = "last_name"
        const val WALLET_BALANCE = "wallet_balance_redemption"
        const val WALLET_BALANCE_INCENTIVE = "wallet_balance_incentive"
        const val UPDATED_AT = "updated_at"
        const val USER_TYPE = "user_type"
        const val CREATED_AT = "created_at"
        const val TOTAL_SUM = "total_sum"
        const val ACCESS_TOKEN = "access_token"
        const val PIN = "pin"
        const val PROFILE_PIC_URL = "profile_pic_url"
        const val MOBILE_NUM = "mobile_num"
        const val EMAIL_ADDRESS = "email_address"
        const val WITHDRAWABLE_AMOUNT = "withdrawable_amount"
        val TYPE_OWNER = "store_owner"
        val TYPE_FRONTLINER = "frontliner"
    }
}