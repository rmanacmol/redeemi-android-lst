package com.imfreemobile.redeemi.android.redeemiandroid.model.networkmodel

import com.google.gson.annotations.Expose
import com.google.gson.annotations.SerializedName
import com.imfreemobile.redeemi.android.redeemiandroid.model.entity.Voucher

/**
 * Created by Joseph on 26/10/2017.
 */

class EnvelopedVoucher {

    @Expose
    @SerializedName(DATA)
    val data: Voucher? = null

    @Expose
    @SerializedName(ERROR)
    val error: Error? = null

    companion object {
        const val DATA = "data"
        const val ERROR = "error"
    }
}