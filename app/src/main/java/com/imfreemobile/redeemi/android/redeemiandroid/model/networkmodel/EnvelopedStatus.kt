package com.imfreemobile.redeemi.android.redeemiandroid.model.networkmodel

import com.google.gson.annotations.Expose
import com.google.gson.annotations.SerializedName
import com.imfreemobile.redeemi.android.redeemiandroid.model.entity.Status

/**
 * Created by jvillarey on 15/11/2017.
 */

class EnvelopedStatus {

    @Expose
    @SerializedName(DATA)
    val data: Status? = null
    @Expose
    @SerializedName(ERROR)
    val error: Error? = null

    companion object {
        const val DATA = "data"
        const val ERROR = "error"
    }
}
