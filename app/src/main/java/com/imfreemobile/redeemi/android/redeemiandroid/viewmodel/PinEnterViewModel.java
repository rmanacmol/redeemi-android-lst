package com.imfreemobile.redeemi.android.redeemiandroid.viewmodel;

import android.app.Activity;
import android.app.Application;
import android.view.View;

import com.imfreemobile.redeemi.android.redeemiandroid.databinding.FragmentPinEnterBinding;
import com.imfreemobile.redeemi.android.redeemiandroid.view.RegistrationFlowActivity;
import com.imfreemobile.redeemi.android.redeemiandroid.view.fragment.registration.PinEnterFragment;

/**
 * Created by rmanacmol on 13/11/2017.
 */

public class PinEnterViewModel extends BaseViewModel {

    private Activity mActivity;
    private PinEnterFragment.OnPinEnterListener mListener;

    public PinEnterViewModel(Application application, Activity activity, PinEnterFragment.OnPinEnterListener listener) {
        super(application, activity);
        this.mActivity = activity;
        this.mListener = listener;
    }

    public void onPinEntered(CharSequence str, View view) {
        if (str.length() == 4) {
            this.mListener.onPinSet(str.toString());
        }
    }
}
