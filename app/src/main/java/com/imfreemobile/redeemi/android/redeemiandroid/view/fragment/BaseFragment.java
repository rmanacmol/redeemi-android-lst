package com.imfreemobile.redeemi.android.redeemiandroid.view.fragment;

import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.marcinorlowski.fonty.Fonty;

/**
 * Created by jvillarey on 24/11/2017.
 */

public class BaseFragment extends Fragment {

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        Fonty.setFonts(container);
        return super.onCreateView(inflater, container, savedInstanceState);
    }
}
