package com.imfreemobile.redeemi.android.redeemiandroid.view;

import android.databinding.DataBindingUtil;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.text.Html;
import android.view.ViewGroup;

import com.imfreemobile.redeemi.android.redeemiandroid.R;
import com.imfreemobile.redeemi.android.redeemiandroid.databinding.ActivitySuccessRegistrationBinding;
import com.imfreemobile.redeemi.android.redeemiandroid.viewmodel.SuccessRegistrationViewModel;
import com.marcinorlowski.fonty.Fonty;

/**
 * Created by rmanacmol on 13/11/2017.
 */

public class SuccessRegistrationActivity extends BaseLocationActivity {

    @Override
    protected boolean isPinRequiredInActivity() {
        return false;
    }

    private ActivitySuccessRegistrationBinding mBinding;
    private SuccessRegistrationViewModel mViewModel;

    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        mBinding = DataBindingUtil.setContentView(this, R.layout.activity_success_registration);
        mViewModel = new SuccessRegistrationViewModel(getApplication(), this);
        mBinding.setViewModel(mViewModel);
        mBinding.tvDesc.setText(Html.fromHtml(String.format(getString(R.string.string_success_registration_desc), "25,000")));

        Fonty.setFonts((ViewGroup) mBinding.getRoot());
    }

    @Override
    protected void onResumeComplete() {

    }
}
