package com.imfreemobile.redeemi.android.redeemiandroid.viewmodel;

import android.app.Application;
import android.content.Context;
import android.databinding.BindingAdapter;
import android.databinding.ObservableBoolean;
import android.databinding.ObservableField;
import android.graphics.Color;
import android.os.Build;
import android.os.CountDownTimer;
import android.support.annotation.NonNull;
import android.support.design.widget.Snackbar;
import android.support.v7.widget.AppCompatTextView;
import android.text.Html;
import android.text.TextUtils;
import android.view.View;
import android.view.inputmethod.InputMethodManager;

import com.google.android.gms.tasks.OnFailureListener;
import com.google.android.gms.tasks.OnSuccessListener;
import com.google.android.gms.tasks.Task;
import com.imfreemobile.redeemi.android.redeemiandroid.BR;
import com.imfreemobile.redeemi.android.redeemiandroid.BuildConfig;
import com.imfreemobile.redeemi.android.redeemiandroid.R;
import com.imfreemobile.redeemi.android.redeemiandroid.model.entity.VerificationOtpRequest;
import com.imfreemobile.redeemi.android.redeemiandroid.model.entity.VerificationRequest;
import com.imfreemobile.redeemi.android.redeemiandroid.model.networkmodel.EnvelopedVerification;
import com.imfreemobile.redeemi.android.redeemiandroid.model.networkmodel.EnvelopedVerificationOtp;
import com.imfreemobile.redeemi.android.redeemiandroid.model.networkmodel.Error;
import com.imfreemobile.redeemi.android.redeemiandroid.utils.networkutils.NoConnectivityException;
import com.imfreemobile.redeemi.android.redeemiandroid.view.RegistrationFlowActivity;
import com.imfreemobile.redeemi.android.redeemiandroid.view.fragment.registration.OTPFragment;

import java.io.IOException;
import java.util.Locale;
import java.util.concurrent.TimeUnit;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;
import timber.log.Timber;

/**
 * Created by jvillarey on 13/11/2017.
 */

public class OTPFragmentViewModel extends BaseViewModel {

    private static final String TAG = OTPFragmentViewModel.class.getSimpleName();

    private OTPFragment.OnOtpSuccessListener mListener;
    private OTPFragment mFragment;
    private ObservableField<String> number = new ObservableField<>();
    private ObservableBoolean loading = new ObservableBoolean();
    private ObservableBoolean error = new ObservableBoolean();
    private ObservableBoolean nextEnabled = new ObservableBoolean();
    private ObservableBoolean newCodeSent = new ObservableBoolean();
    private ObservableField<String> timerText = new ObservableField<>();
    private ObservableBoolean lastAttemptMode = new ObservableBoolean();
    private String verificationUuid;
    private Call<EnvelopedVerificationOtp> networkCallVerificationOtp;
    private Call<EnvelopedVerification> networkCallRequestNewCode;

    private Snackbar snackbar;
    private CountDownTimer timer;

    public void setLastAttemptMode(boolean lastAttemptMode) {
        this.lastAttemptMode.set(lastAttemptMode);
        notifyPropertyChanged(BR._all);
    }

    public boolean getLastAttemptMode() {
        return this.lastAttemptMode.get();
    }

    public void setVerificationUuid(String verificationUuid) {
        this.verificationUuid = verificationUuid;
    }

    public void setTimerText(String timerText) {
        this.timerText.set(timerText);
        notifyPropertyChanged(BR._all);
    }

    public String getTimerText() {
        return this.timerText.get();
    }

    public void setNewCodeSent(boolean newCodeSent) {
        this.newCodeSent.set(newCodeSent);
        notifyPropertyChanged(BR._all);
    }

    public boolean getNewCodeSent() {
        return this.newCodeSent.get();
    }

    public void setNextEnabled(boolean nextEnabled) {
        this.nextEnabled.set(nextEnabled);
    }

    public boolean getNextEnabled() {
        return this.nextEnabled.get();
    }

    public void setError(boolean error) {
        this.error.set(error);
        notifyPropertyChanged(BR._all);
    }

    public boolean getError() {
        return this.error.get();
    }

    public void setNumber(String number) {
        this.number.set(number);
        notifyPropertyChanged(BR._all);

        Task<Void> task = client.startSmsRetriever();
        task.addOnSuccessListener(new OnSuccessListener<Void>() {
            @Override
            public void onSuccess(Void aVoid) {
                Timber.d(TAG, "sms onsuccess");
            }
        });
        task.addOnFailureListener(new OnFailureListener() {
            @Override
            public void onFailure(@NonNull Exception e) {
                Timber.d(TAG, "sms onfail");
            }
        });
    }

    public String getNumber() {
        return this.number.get();
    }

    public void setLoading(boolean loading) {
        this.loading.set(loading);
        notifyPropertyChanged(BR._all);
    }

    public boolean getLoading() {
        return this.loading.get();
    }

    public OTPFragmentViewModel(Application application, OTPFragment.OnOtpSuccessListener registrationFlowActivity, OTPFragment fragment, String number, String verificationUuid) {
        super(application, fragment.getActivity());
        this.mListener = registrationFlowActivity;
        this.mFragment = fragment;
        setNumber(number);
        setVerificationUuid(verificationUuid);
    }

    public void hideKeyboard() {
        InputMethodManager imm = (InputMethodManager) mFragment.getActivity().getSystemService(Context.INPUT_METHOD_SERVICE);
        imm.hideSoftInputFromWindow(mFragment.getMBinding().getRoot().getWindowToken(), 0);
        setError(false);
    }

    public void hideError() {
        if (snackbar != null) {
            snackbar.dismiss();
        }
        setError(false);
    }

    private void requestNewCode() {
        setLoading(true);
        if (mFragment.getActivity() instanceof RegistrationFlowActivity) {
            networkCallRequestNewCode = apiService.verify(new VerificationRequest(getNumber()));
        } else {
            networkCallRequestNewCode = apiService.forgotPassword(new VerificationRequest(getNumber()));
        }
        networkCallRequestNewCode.enqueue(new Callback<EnvelopedVerification>() {
            @Override
            public void onResponse(Call<EnvelopedVerification> call, Response<EnvelopedVerification> response) {
                setLoading(false);
                if (response.isSuccessful()) {
                    verificationUuid = response.body().getData().getVerificationUuid();
                    startTimer();
                } else {
                    try {
                        handleError(response.errorBody().string());
                    } catch (IOException e) {
                        e.printStackTrace();
                        showGenericError();
                    }
                }
            }

            @Override
            public void onFailure(Call<EnvelopedVerification> call, Throwable t) {
                if (t instanceof NoConnectivityException) {
                    showNoInternetError();
                } else {
                    handleError(call.toString());
                }
            }
        });
    }

    private void validateOtpCode(final String otpCode) {
        setLoading(true);
        networkCallVerificationOtp = apiService.verifyOtp(new VerificationOtpRequest(otpCode, verificationUuid));
        networkCallVerificationOtp.enqueue(new Callback<EnvelopedVerificationOtp>() {
            @Override
            public void onResponse(@NonNull Call<EnvelopedVerificationOtp> call, @NonNull Response<EnvelopedVerificationOtp> response) {
                setLoading(false);
                if (response.isSuccessful()) {
                    mListener.onOtpSuccess(response.body().getData().getAccessToken());
                } else {
                    // handle errors
                    try {
                        handleError(response.errorBody().string());
                    } catch (IOException | NullPointerException e) {
                        e.printStackTrace();
                        showGenericError();
                    }
                }
            }

            @Override
            public void onFailure(Call<EnvelopedVerificationOtp> call, Throwable t) {
                setLoading(false);
                if (t instanceof NoConnectivityException) {
                    showNoInternetError();
                } else {
                    handleError(call.toString());
                }
            }
        });
    }

    public void showSnackbar(View view, String text) {
        if (snackbar == null) {
            snackbar = Snackbar.make(view, text, Snackbar.LENGTH_INDEFINITE);
        } else {
            snackbar.setText(text);
        }
        snackbar.setAction(R.string.okay, new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                snackbar.dismiss();
            }
        });
        snackbar.setActionTextColor(Color.WHITE);
        snackbar.show();
    }

    private void showNoInternetError() {
        mFragment.showSnackbar(mFragment.getActivity().getString(R.string.error_connection));
        setError(true);
    }

    private void showGenericError() {
        mFragment.showSnackbar(mFragment.getActivity().getString(R.string.error_generic));
        setError(true);
    }

    private void handleError(String string) {
        try {
            Error error = gson.fromJson(string, EnvelopedVerificationOtp.class).getError();
            switch (error.getErrorCode()) {
                case 1010:
                    showUniqueCodeIsInvalid();
                    break;
                case 1022:
                    showRetryAt(error.getRetryAt());
                    break;
                default:
                    showGenericError();
                    break;
            }
        } catch (Exception e) {
            showGenericError();
        }
    }

    private void showUniqueCodeIsInvalid() {
        mFragment.showSnackbar(mFragment.getActivity().getString(R.string.unique_code_invalid));
        setError(true);
    }

    private void showRetryAt(Long retryAt) {
        mFragment.showSnackbar(mFragment.getActivity().getString(R.string.error_limit_reached));
        startTimer(retryAt);
    }

    public void onNextButtonClicked(View view) {
        hideKeyboard();
        validateOtpCode(mFragment.getMBinding().etOtp.getText().toString().trim().replace(" ", ""));
    }

    public void setCodeAndValidate(String code) {
        hideKeyboard();
        mFragment.getMBinding().etOtp.setText(code);
        validateOtpCode(mFragment.getMBinding().etOtp.getText().toString().trim().replace(" ", ""));
    }

    public void onRequestNewCodeClicked(View view) {
        requestNewCode();
        hideError();
        hideKeyboard();
    }

    public void stopTimer() {
        if(timer!=null) {
            timer.cancel();
        }
        setLastAttemptMode(false);
        setNewCodeSent(false);
    }

    public void startTimer(long retryAt) {
        initCountdownTimer(retryAt);
        setLastAttemptMode(true);
    }

    public void startTimer() {
        long retryAt = BuildConfig.OTP_REQUEST_CODE_DURATION;
        initCountdownTimer(retryAt);
        setLastAttemptMode(false);
    }

    private void initCountdownTimer(long retryAt) {
        setNewCodeSent(true);
        if (timer != null) {
            timer.cancel();
        }
        retryAt = (long) (retryAt * BuildConfig.BACKEND_TIMER_MULTIPLIER);
        timer = new CountDownTimer(retryAt, 1000) {
            @Override
            public void onTick(long millis) {
                setTimerText(String.format(Locale.ENGLISH, "%02d:%02d",
                        TimeUnit.MILLISECONDS.toMinutes(millis) -
                                TimeUnit.HOURS.toMinutes(TimeUnit.MILLISECONDS.toHours(millis)),
                        TimeUnit.MILLISECONDS.toSeconds(millis) -
                                TimeUnit.MINUTES.toSeconds(TimeUnit.MILLISECONDS.toMinutes(millis))));
            }

            @Override
            public void onFinish() {
                setNewCodeSent(false);
                setLastAttemptMode(false);
            }
        };
        timer.start();
    }

    @BindingAdapter({"bind:htmlText"})
    public static void loadHtmlText(AppCompatTextView textView, String htmlString) {
        if (htmlString != null && !TextUtils.isEmpty(htmlString)) {
            CharSequence text = null;
            if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.N) {
                text = Html.fromHtml(htmlString, Html.FROM_HTML_MODE_LEGACY);
            } else {
                text = Html.fromHtml(htmlString);
            }
            textView.setText(text);
        }
    }
}