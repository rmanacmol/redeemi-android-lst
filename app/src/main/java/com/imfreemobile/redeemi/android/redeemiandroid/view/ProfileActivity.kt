package com.imfreemobile.redeemi.android.redeemiandroid.view

import android.databinding.DataBindingUtil
import android.os.Bundle
import android.view.ViewGroup
import com.imfreemobile.redeemi.android.redeemiandroid.R
import com.imfreemobile.redeemi.android.redeemiandroid.databinding.ActivityProfileBinding
import com.imfreemobile.redeemi.android.redeemiandroid.viewmodel.ProfileViewModel
import com.marcinorlowski.fonty.Fonty

class ProfileActivity : BaseLocationActivity() {
    override fun onResumeComplete() {
    }

    override fun isPinRequiredInActivity(): Boolean {
        return true
    }

    companion object {
        val TAG = ProfileActivity::class.java.simpleName
    }

    private var mBinding: ActivityProfileBinding? = null
    private var mViewModel: ProfileViewModel? = null

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        mBinding = DataBindingUtil.setContentView(this@ProfileActivity, R.layout.activity_profile)
        mViewModel = ProfileViewModel(application, this@ProfileActivity)
        mBinding?.viewModel = mViewModel

        mBinding?.btnBack?.setOnClickListener { onBackPressed() }

        Fonty.setFonts(mBinding?.getRoot() as ViewGroup)
    }
}
