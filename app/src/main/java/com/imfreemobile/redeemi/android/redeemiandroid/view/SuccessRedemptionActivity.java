package com.imfreemobile.redeemi.android.redeemiandroid.view;

import android.databinding.DataBindingUtil;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.view.ViewGroup;

import com.imfreemobile.redeemi.android.redeemiandroid.R;
import com.imfreemobile.redeemi.android.redeemiandroid.databinding.ActivitySuccessRedemptionBinding;
import com.imfreemobile.redeemi.android.redeemiandroid.model.entity.Transaction;
import com.imfreemobile.redeemi.android.redeemiandroid.viewmodel.SuccessRedemptionViewModel;
import com.marcinorlowski.fonty.Fonty;

/**
 * Created by renzmanacmol on 24/10/2017.
 */

public class SuccessRedemptionActivity extends BaseLocationActivity {

    @Override
    protected boolean isPinRequiredInActivity() {
        return true;
    }

    private ActivitySuccessRedemptionBinding mBinding;
    private SuccessRedemptionViewModel mViewModel;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        mBinding = DataBindingUtil.setContentView(this, R.layout.activity_success_redemption);
        mViewModel = new SuccessRedemptionViewModel(getApplication(), this, (Transaction) getIntent().getSerializableExtra("transaction"));
        mBinding.setViewModel(mViewModel);

        Fonty.setFonts((ViewGroup) mBinding.getRoot());
    }

    @Override
    protected void onResumeComplete() {

    }
}
