package com.imfreemobile.redeemi.android.redeemiandroid.view.adapter;

import android.databinding.DataBindingUtil;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.ViewGroup;

import com.imfreemobile.redeemi.android.redeemiandroid.R;
import com.imfreemobile.redeemi.android.redeemiandroid.databinding.ItemWithdrawHistoryBinding;
import com.imfreemobile.redeemi.android.redeemiandroid.model.entity.WithdrawTransaction;
import com.marcinorlowski.fonty.Fonty;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

/**
 * Created by jvillarey on 20/11/2017.
 */

public class WithdrawHistoryAdapter extends RecyclerView.Adapter<WithdrawHistoryAdapter.WithdrawHistoryViewHolder> {

    private List<WithdrawTransaction> transactionList;

    public WithdrawHistoryAdapter() {
        this.transactionList = new ArrayList<WithdrawTransaction>();
    }

    public void setItems(List<WithdrawTransaction> list) {
        this.transactionList.clear();
        this.transactionList.addAll(list);
    }

    @Override
    public WithdrawHistoryViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {

        ItemWithdrawHistoryBinding binding = DataBindingUtil.inflate(LayoutInflater.from(parent.getContext()), R.layout.item_withdraw_history, parent, false);
        Fonty.setFonts((ViewGroup) binding.getRoot());
        return new WithdrawHistoryViewHolder(binding);
    }

    @Override
    public void onBindViewHolder(WithdrawHistoryViewHolder holder, int position) {
        holder.bindItem(transactionList.get(position));
    }

    @Override
    public int getItemCount() {
        return transactionList.size();
    }

    public class WithdrawHistoryViewHolder extends RecyclerView.ViewHolder {

        private ItemWithdrawHistoryBinding mBinding;

        public WithdrawHistoryViewHolder(ItemWithdrawHistoryBinding binding) {
            super(binding.getRoot());
            this.mBinding = binding;
        }

        void bindItem(WithdrawTransaction transaction) {
            mBinding.setViewModel(transaction);
            mBinding.executePendingBindings();
        }
    }
}
