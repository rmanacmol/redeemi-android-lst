package com.imfreemobile.redeemi.android.redeemiandroid.model.entity

import com.google.gson.annotations.Expose
import com.google.gson.annotations.SerializedName

/**
 * Created by jvillarey on 22/11/2017.
 */

class WithdrawRequest(@field:Expose
                      @field:SerializedName(AMOUNT)
                      private val amount: String) {

    companion object {
        const val AMOUNT = "amount"
    }
}
