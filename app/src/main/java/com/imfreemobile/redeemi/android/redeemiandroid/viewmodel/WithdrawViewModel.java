package com.imfreemobile.redeemi.android.redeemiandroid.viewmodel;

import android.app.Activity;
import android.app.Application;
import android.databinding.ObservableBoolean;
import android.databinding.ObservableField;
import android.graphics.Color;
import android.support.annotation.NonNull;
import android.support.design.widget.Snackbar;
import android.view.View;

import com.afollestad.materialdialogs.DialogAction;
import com.afollestad.materialdialogs.MaterialDialog;
import com.imfreemobile.redeemi.android.redeemiandroid.BR;
import com.imfreemobile.redeemi.android.redeemiandroid.R;
import com.imfreemobile.redeemi.android.redeemiandroid.model.entity.WithdrawRequest;
import com.imfreemobile.redeemi.android.redeemiandroid.model.entity.WithdrawTransaction;
import com.imfreemobile.redeemi.android.redeemiandroid.model.networkmodel.EnvelopedWithdrawTransaction;
import com.imfreemobile.redeemi.android.redeemiandroid.model.networkmodel.Error;
import com.imfreemobile.redeemi.android.redeemiandroid.utils.AuthUtils;
import com.imfreemobile.redeemi.android.redeemiandroid.utils.CurrencyUtils;
import com.imfreemobile.redeemi.android.redeemiandroid.utils.networkutils.NoConnectivityException;
import com.imfreemobile.redeemi.android.redeemiandroid.view.BottomNavActivity;
import com.imfreemobile.redeemi.android.redeemiandroid.view.fragment.withdraw.WithdrawFragment;
import com.imfreemobile.redeemi.android.redeemiandroid.view.fragment.withdraw.WithdrawIncomeFragment;

import java.io.IOException;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

/**
 * Created by jvillarey on 20/11/2017.
 */

public class WithdrawViewModel extends BaseViewModel {

    private static final String TAG = WithdrawViewModel.class.getSimpleName();

    private BottomNavActivity mActivity;
    private WithdrawIncomeFragment withdrawIncomeFragment;
    private WithdrawFragment withdrawFragment;
    private ObservableField<WithdrawTransaction> pendingWithdrawTransaction = new ObservableField<>();
    private ObservableField<String> withdrawableAmount = new ObservableField<>();
    private ObservableBoolean loading = new ObservableBoolean();
    private Call<EnvelopedWithdrawTransaction> networkCallGetPending;
    private Call<EnvelopedWithdrawTransaction> networkCallWithdraw;

    private Snackbar snackbar;

    public boolean getLoading() {
        return this.loading.get();
    }

    public void setLoading(boolean loading) {
        this.loading.set(loading);
        notifyPropertyChanged(BR._all);
    }

    public void setWithdrawableAmount(String withdrawableAmount) {
        this.withdrawableAmount.set(withdrawableAmount);
        notifyPropertyChanged(BR._all);
    }

    public void cancelNetCall() {
        if (networkCallGetPending != null) {
            networkCallGetPending.cancel();
            networkCallGetPending = null;
        }
        if (networkCallWithdraw != null) {
            networkCallWithdraw.cancel();
            networkCallWithdraw = null;
        }
    }

    public void showSnackbar(View view, String text) {
        try {
            if (snackbar == null && view != null) {
                snackbar = Snackbar.make(view, text, Snackbar.LENGTH_INDEFINITE);
            } else {
                snackbar.setText(text);
            }
            snackbar.setAction(R.string.okay, new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    snackbar.dismiss();
                }
            });
            snackbar.setActionTextColor(Color.WHITE);
            snackbar.show();
        } catch (NullPointerException e) {
        }
    }

    public String getWithdrawableAmount() {
        return this.withdrawableAmount.get();
    }

    public void setPendingWithdrawTransaction(WithdrawTransaction transaction) {
        this.pendingWithdrawTransaction.set(transaction);
        notifyPropertyChanged(BR._all);
    }

    public WithdrawTransaction getPendingWithdrawTransaction() {
        return this.pendingWithdrawTransaction.get();
    }

    public WithdrawViewModel(Application application, Activity activity, WithdrawIncomeFragment fragment, WithdrawFragment withdrawFragment) {
        super(application, activity);
        this.mActivity = (BottomNavActivity) activity;
        this.withdrawIncomeFragment = fragment;
        this.withdrawFragment = withdrawFragment;

        setWithdrawableAmount(appDatabase.getUserDao().getUser().getWithdrawableAmount());
        fetchPendingTransaction();
    }

    private void handleError(String string) {
        try {
            Error error = gson.fromJson(string, EnvelopedWithdrawTransaction.class).getError();
            switch (error.getErrorCode()) {
                case 1021:
                    showTooManyWithdrawalAttemptsError();
                    break;
                case 1020:
                    showActiveWithdrawalFound();
                    break;
                case 1018:
                    showMinimumWithdrawAmountError();
                    break;
                case 1019:
                    showMaximumWithdrawAmountError();
                    break;
                default:
                    showGenericError();
                    break;
            }
        } catch (Exception e) {
            showGenericError();
        }
    }

    private void showMinimumWithdrawAmountError() {
        withdrawFragment.showSnackbar(mActivity.getString(R.string.error_withdrawal_less_than_minimum));
    }

    private void showMaximumWithdrawAmountError() {
        withdrawFragment.showSnackbar(mActivity.getString(R.string.error_withdrawal_more_than_balance));
    }

    private void showActiveWithdrawalFound() {
        withdrawFragment.showSnackbar(mActivity.getString(R.string.error_active_withdrawal_found));
    }

    private void showTooManyWithdrawalAttemptsError() {
        withdrawFragment.showSnackbar(mActivity.getString(R.string.error_too_many_withdrawals));
    }

    private void showNoInternetError() {
        withdrawFragment.showSnackbar(mActivity.getString(R.string.error_connection));
    }

    private void showGenericError() {
        withdrawFragment.showSnackbar(mActivity.getString(R.string.error_generic));
    }

    public void withdraw() {
        setLoading(true);
        networkCallWithdraw = apiService.withdraw(AuthUtils.INSTANCE.getAccessHeaderFormattedToken(appDatabase.getTokenDao().getToken().getAccessToken()), new WithdrawRequest(CurrencyUtils.stripCurrencyFromAmountString(withdrawFragment.getMBinding().etAmt.getText().toString())));
        networkCallWithdraw.enqueue(new Callback<EnvelopedWithdrawTransaction>() {
            @Override
            public void onResponse(Call<EnvelopedWithdrawTransaction> call, Response<EnvelopedWithdrawTransaction> response) {
                setLoading(false);
                if (response.isSuccessful() && response.body().getData().getAmount() != null) {
                    setPendingWithdrawTransaction(response.body().getData());
                    setWithdrawableAmount("0.00");
                    showWithdawRequestSentDialog();
                } else {
                    try {
                        handleError(response.errorBody().string());
                    } catch (IOException | NullPointerException e) {
                        e.printStackTrace();
                        showGenericError();
                    }
                }
            }

            @Override
            public void onFailure(Call<EnvelopedWithdrawTransaction> call, Throwable t) {
                setLoading(false);
                if (t instanceof NoConnectivityException) {
                    showNoInternetError();
                } else {
                    handleError(t.getMessage());
//                    showRequestFailedDialog();
                }
            }
        });
    }

    public void fetchPendingTransaction() {
        setLoading(true);
        networkCallGetPending = apiService.getActiveWithdrawTransaction(AuthUtils.INSTANCE.getAccessHeaderFormattedToken(appDatabase.getTokenDao().getToken().getAccessToken()));
        networkCallGetPending.enqueue(new Callback<EnvelopedWithdrawTransaction>() {
            @Override
            public void onResponse(Call<EnvelopedWithdrawTransaction> call, Response<EnvelopedWithdrawTransaction> response) {
                setLoading(false);
                if (response.isSuccessful() && response.body().getData().getAmount() != null) {
                    setPendingWithdrawTransaction(response.body().getData());
                } else {
                    pendingWithdrawTransaction.set(null);
                }
            }

            @Override
            public void onFailure(Call<EnvelopedWithdrawTransaction> call, Throwable t) {
                setLoading(false);
                if (t instanceof NoConnectivityException) {
                    showNoInternetError();
                } else {
                    handleError(t.getMessage());
                }
            }
        });
    }

    public void onWithdrawClicked(View view) {

        new MaterialDialog.Builder(mActivity).content(R.string.amount_to_withdraw_dialog, withdrawFragment.getMBinding().etAmt.getText().toString()).positiveText(R.string.okay).negativeText(R.string.string_cancel).onPositive(new MaterialDialog.SingleButtonCallback() {
            @Override
            public void onClick(@NonNull MaterialDialog dialog, @NonNull DialogAction which) {
                withdraw();
                dialog.dismiss();
            }
        }).onNegative(new MaterialDialog.SingleButtonCallback() {
            @Override
            public void onClick(@NonNull MaterialDialog dialog, @NonNull DialogAction which) {
                dialog.dismiss();
            }
        }).show();
    }

    private void showWithdawRequestSentDialog() {
        new MaterialDialog.Builder(mActivity).title(R.string.title_request_sent).content(R.string.content_request_sent).positiveText(R.string.okay).onPositive(new MaterialDialog.SingleButtonCallback() {
            @Override
            public void onClick(@NonNull MaterialDialog dialog, @NonNull DialogAction which) {
                dialog.dismiss();
            }
        }).show();
    }

    private void showRequestFailedDialog() {
        new MaterialDialog.Builder(mActivity).title(R.string.title_request_failed).content(R.string.content_request_failed).positiveText(R.string.try_again).negativeText(R.string.string_cancel).onPositive(new MaterialDialog.SingleButtonCallback() {
            @Override
            public void onClick(@NonNull MaterialDialog dialog, @NonNull DialogAction which) {
                withdraw();
                dialog.dismiss();
            }
        }).onNegative(new MaterialDialog.SingleButtonCallback() {
            @Override
            public void onClick(@NonNull MaterialDialog dialog, @NonNull DialogAction which) {
                dialog.dismiss();
            }
        }).show();
    }
}
