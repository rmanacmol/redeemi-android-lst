package com.imfreemobile.redeemi.android.redeemiandroid.viewmodel;

import android.app.Activity;
import android.app.Application;
import android.content.Context;
import android.graphics.Color;
import android.support.design.widget.Snackbar;
import android.view.View;
import android.view.inputmethod.InputMethodManager;
import android.widget.Toast;

import com.imfreemobile.redeemi.android.redeemiandroid.R;
import com.imfreemobile.redeemi.android.redeemiandroid.databinding.FragmentPinConfirmBinding;
import com.imfreemobile.redeemi.android.redeemiandroid.view.BottomNavActivity;
import com.imfreemobile.redeemi.android.redeemiandroid.view.RegistrationFlowActivity;
import com.imfreemobile.redeemi.android.redeemiandroid.view.fragment.registration.PinConfirmFragment;

/**
 * Created by rmanacmol on 13/11/2017.
 */

public class PinConfirmViewModel extends BaseViewModel {

    private Activity mActivity;
    private PinConfirmFragment.OnPinConfirmedListener mListener;
    private FragmentPinConfirmBinding mBinding;

    public PinConfirmViewModel(Application application,
                               Activity activity,
                               PinConfirmFragment.OnPinConfirmedListener mListener, FragmentPinConfirmBinding binding) {

        super(application, activity);
        this.mActivity = activity;
        this.mListener = mListener;
        this.mBinding = binding;
    }

    public void onPinEntered(CharSequence str, View view, String pinToConfirm) {
        if (str.toString().equals(pinToConfirm)) {
            this.mListener.onPinConfirmed();
        } else {
            mBinding.pinView.setError(true);

            final InputMethodManager imm = (InputMethodManager) mActivity.getSystemService(Context.INPUT_METHOD_SERVICE);
            if (imm != null) {
                try {
                    imm.hideSoftInputFromWindow(view.getWindowToken(), 0);
                } catch (Exception e) {
                    e.printStackTrace();
                }
            }

            Snackbar.make(mBinding.rootLayout, mActivity.getString(R.string.pin_error_message),
                    Snackbar.LENGTH_INDEFINITE).setActionTextColor(Color.WHITE).setAction(mActivity.getString(R.string.pin_try_again_message),
                    new View.OnClickListener() {
                        @Override
                        public void onClick(View v) {
                            mBinding.pinView.setText(null);
                            mBinding.pinView.focus();
                        }
                    }).show();
        }
    }


}
