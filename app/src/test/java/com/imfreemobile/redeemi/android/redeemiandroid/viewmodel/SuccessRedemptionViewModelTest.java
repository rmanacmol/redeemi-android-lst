package com.imfreemobile.redeemi.android.redeemiandroid.viewmodel;

import com.imfreemobile.redeemi.android.redeemiandroid.BuildConfig;

import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.MockitoAnnotations;
import org.robolectric.RobolectricTestRunner;
import org.robolectric.annotation.Config;

/**
 * Created by renzmanacmol on 25/10/2017.
 */
@RunWith(RobolectricTestRunner.class)
@Config(constants = BuildConfig.class, sdk = 21, manifest = Config.NONE)
public class SuccessRedemptionViewModelTest {

    private SuccessRedemptionViewModel mViewModel;

    @Before
    public void setUpMainViewModelTest() {
        MockitoAnnotations.initMocks(this);

    }

    @Test
    public void updateUI() throws Exception {

    }
}