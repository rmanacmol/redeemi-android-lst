package com.imfreemobile.redeemi.android.redeemiandroid.viewmodel;

import android.content.Intent;

import com.imfreemobile.redeemi.android.redeemiandroid.BuildConfig;
import com.imfreemobile.redeemi.android.redeemiandroid.view.ConfirmRedemptionActivity;
import com.imfreemobile.redeemi.android.redeemiandroid.view.HomeActivity;

import org.junit.Before;
import org.junit.Ignore;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.Mock;
import org.mockito.MockitoAnnotations;
import org.robolectric.RobolectricTestRunner;
import org.robolectric.Shadows;
import org.robolectric.annotation.Config;
import org.robolectric.shadows.ShadowIntent;

import static org.junit.Assert.assertEquals;

/**
 * Created by Joseph on 25/10/2017.
 */
@RunWith(RobolectricTestRunner.class)
@Config(constants = BuildConfig.class, sdk = 21, manifest = Config.NONE)
public class ScanViewModelTest {

    private String testValidUnredeemedBarcodeString = "valid_unredeemed";
    private String testValidRedeemedBarcodeString = "valid_redeemed";
    private String testValidExpiredBarcodeString = "expired";
    private String testInvalidBarcodeString = "invalid";

    @Mock
    private ScanViewModel mViewModel;

    @Before
    public void setUpMainViewModelTest() {
        MockitoAnnotations.initMocks(this);
        mViewModel = new ScanViewModel();
    }

    @Test
    public void onBackPressed() {
        mViewModel.onBackPressed();
        Intent startedIntent = Shadows.shadowOf(mViewModel.getmActivity()).getNextStartedActivity();
        ShadowIntent shadowIntent = Shadows.shadowOf(startedIntent);
        assertEquals(HomeActivity.class, shadowIntent.getIntentClass());
    }

    @Test
    public void onScanValidUnredeemedBarcodeDetected() {
        mViewModel.onBarcodeDetected(testValidUnredeemedBarcodeString);
        Intent startedIntent = Shadows.shadowOf(mViewModel.getmActivity()).getNextStartedActivity();
        ShadowIntent shadowIntent = Shadows.shadowOf(startedIntent);
        assertEquals(ConfirmRedemptionActivity.class, shadowIntent.getIntentClass());
        // also assert confirmactivity's viewmodel
    }

    @Test
    public void onScanValidRedeemedBarcodeString() {
        mViewModel.onBarcodeDetected(testValidRedeemedBarcodeString);
        Intent startedIntent = Shadows.shadowOf(mViewModel.getmActivity()).getNextStartedActivity();
        ShadowIntent shadowIntent = Shadows.shadowOf(startedIntent);
        assertEquals(ConfirmRedemptionActivity.class, shadowIntent.getIntentClass());
        // also assert confirmactivity's viewmodel
    }

    @Ignore
    public void onScanValidExpiredBarcodeString() {

    }

    @Ignore
    public void onScanInvalidBarcodeString() {

    }

}