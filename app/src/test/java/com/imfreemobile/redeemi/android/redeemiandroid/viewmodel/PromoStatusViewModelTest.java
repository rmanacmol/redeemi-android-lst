package com.imfreemobile.redeemi.android.redeemiandroid.viewmodel;

import com.imfreemobile.redeemi.android.redeemiandroid.BuildConfig;

import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.MockitoAnnotations;
import org.robolectric.RobolectricTestRunner;
import org.robolectric.annotation.Config;

import static org.junit.Assert.assertNotNull;

/**
 * Created by renzmanacmol on 25/10/2017.
 */

@RunWith(RobolectricTestRunner.class)
@Config(constants = BuildConfig.class, sdk = 21, manifest = Config.NONE)
public class PromoStatusViewModelTest {

    private PromoStatusViewModel mViewModel;

    @Before
    public void setUpMainViewModelTest() {
        MockitoAnnotations.initMocks(this);
        mViewModel = new PromoStatusViewModel();
    }

    @Test
    public void updateUI() throws Exception {
        mViewModel.updateUI("Promo has expired");
        assertNotNull(mViewModel.mPromoStatus.get());
    }

}